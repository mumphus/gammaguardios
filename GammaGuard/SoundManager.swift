//
//  SoundManager.swift
//  GammaGuard
//
//  Created by EIC on 2018-05-15.
//  Copyright © 2018 EIC. All rights reserved.
//

import UIKit
import AVFoundation

class SoundManager: AVAudioPlayer {
    var player: AVAudioPlayer?
    func playSound() {
        guard let url = Bundle.main.url(forResource: "geiger", withExtension: "mp3") else { return }
        do {
            try AVAudioSession.sharedInstance().setCategory(AVAudioSessionCategoryPlayback)
            try AVAudioSession.sharedInstance().setActive(true)

            /* The following line is required for the player to work on iOS 11. Change the file type accordingly*/
            player = try AVAudioPlayer(contentsOf: url, fileTypeHint: AVFileType.mp3.rawValue)
            guard let aplayer = player else { return }
            print("Before Sound")
            aplayer.play()
            print(aplayer.isPlaying)
            print("Played Sound")
        } catch let error {
            print(error.localizedDescription)
        }
    }
}
