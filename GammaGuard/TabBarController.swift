//
//  TabBarController.swift
//  GammaGuard
//
//  Created by EIC on 2018-06-14.
//  Copyright © 2018 EIC. All rights reserved.
//

import UIKit

class TabBarController: UITabBarController {

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        if !isLiveGraphEnabled{
            if self.viewControllers?.count == 4{
                self.viewControllers?.remove(at: 3)
            }
        }else{
            if self.viewControllers?.count == 3{
                self.viewControllers?.append(self.storyboard!.instantiateViewController(withIdentifier: "liveGraph"))
            }
        }
    }

}
