//
//  GraphArchiveVC.swift
//  GammaGuard
//
//  Created by EIC on 2018-06-06.
//  Copyright © 2018 EIC. All rights reserved.
//

import UIKit
import CoreData

struct DisplayGraph{
    var date: String?
    var device: String?
    var name: String?
    var graphForRow: NSManagedObject?
}

protocol CustomCellUpdater: class {
    func renameAlert(graph: NSManagedObject)
    func deleteAlert(graph: NSManagedObject)
    func openGraph(graph: NSManagedObject)
}

class GraphArchiveVC: UITableViewController, CustomCellUpdater {
    @IBOutlet var EntryTableView: UITableView!
    
    var graphSetArray: [NSManagedObject] = []
    let dateFormatter = DateFormatter()
    var sv = UIView()
    var dataLoaded: Bool = false
    var fromGraph: Bool = false
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.tabBarController?.tabBar.isHidden = true
        self.navigationItem.hidesBackButton = true
        let newBackButton = UIBarButtonItem(title: "< Detector", style: UIBarButtonItem.Style.plain, target: self, action: #selector(GraphArchiveVC.GoBacktoExternalView))
        self.navigationItem.leftBarButtonItem = newBackButton
        dateFormatter.dateFormat = "yyyy-MM-dd 'at' HH:mm"
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        
        archiveGraphBaseTime = 0
        if !fromGraph{
            // Load in all the saved graphs
            sv = UIViewController.displaySpinner(onView: self.view)
            dataLoaded = false
            guard let appDelegate = UIApplication.shared.delegate as? AppDelegate else {return}
            let managedContext = appDelegate.managedObjectContext
            let privateContext = appDelegate.generatePrivateContext()
            let fetchRequest = NSFetchRequest<NSManagedObject>(entityName: "GraphDataSet")
            fetchRequest.sortDescriptors = [NSSortDescriptor(key: "dateSaved", ascending: false)]
            graphSetArray.removeAll()
            privateContext.perform{
                do {
                    self.graphSetArray = try privateContext.fetch(fetchRequest)
                    if Connected{
                        if self.graphSetArray.count > 0{
                            self.graphSetArray.removeFirst()
                        }
                    }
                    var startTime = NSDate().timeIntervalSince1970
                    for graphSet in self.graphSetArray{
                        let currentTime = NSDate().timeIntervalSince1970
                        var pointArrayCount: Int = 0
                        if graphSet.value(forKey: "values") != nil{
                            pointArrayCount = graphSet.value(forKey: "values") as! Int
                        }
                        if pointArrayCount == 0{
                            //for old datamodels
                            pointArrayCount = 100
                        }
                        print("time Dif:", currentTime - startTime, "Points:",pointArrayCount)
                        startTime = currentTime
                        if pointArrayCount < 100{
                            privateContext.delete(graphSet)
                            if privateContext.hasChanges{
                                do {
                                    try privateContext.save()
                                    managedContext.performAndWait {
                                        do {try managedContext.save()}
                                        catch {fatalError("Failure to save context: \(error)")}
                                    }
                                } catch {fatalError("Failure to save context: \(error)")}
                            }
                        }
                    }
                    DispatchQueue.main.async {
                        print("Reloading Data")
                        self.dataLoaded = true
                        self.tableView.reloadData()
                        UIViewController.removeSpinner(spinner: self.sv)
                    }
                } catch let error as NSError {
                    print("Could not fetch. \(error), \(error.userInfo)")
                }
            }
        }
        fromGraph = false
    }
    
    @objc func GoBacktoExternalView(){
        if !fromHome {
            performSegue(withIdentifier: "Etoctsegue", sender: Any?.self)
        } else {
            self.navigationController!.popViewController(animated: true)
        }
    }
    
    // MARK: - Table view data source
    override func numberOfSections(in tableView: UITableView) -> Int {return 1}
    
    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat{return 100.0}

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if !dataLoaded{return 0}
        guard let appDelegate = UIApplication.shared.delegate as? AppDelegate else {return 0}
        let managedContext = appDelegate.managedObjectContext
        let fetchRequest = NSFetchRequest<NSManagedObject>(entityName: "GraphDataSet")
        fetchRequest.sortDescriptors = [NSSortDescriptor(key: "dateSaved", ascending: false)]
        do {
            graphSetArray.removeAll()
            graphSetArray = try managedContext.fetch(fetchRequest)
            if Connected{
                if graphSetArray.count > 0{
                    graphSetArray.removeFirst()
                }
            }
        } catch let error as NSError {
            print("Could not fetch. \(error), \(error.userInfo)")
        }
        return graphSetArray.count
    }

    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = self.EntryTableView.dequeueReusableCell(withIdentifier: "cell")! as! GraphTableViewCell
        let set = graphSetArray[indexPath.row]
        let name = set.value(forKeyPath: "name") as? String
        let device = set.value(forKeyPath: "deviceUsed") as? String
        let date = set.value(forKeyPath: "dateSaved") as? Int64
        let stringDate = dateFormatter.string(from: Date(timeIntervalSince1970: TimeInterval(date!)))
        let info = DisplayGraph(date: stringDate, device: device, name: name, graphForRow: set)
        cell.displayGraph = info
        cell.delegate = self
        return cell
    }
    
    //---------------------------
    // User interactive functions
    //---------------------------
    func renameAlert(graph: NSManagedObject){
        guard let appDelegate = UIApplication.shared.delegate as? AppDelegate else {return}
        let managedContext = appDelegate.managedObjectContext
        let alert = UIAlertController(title: "Change Name",message: "Enter the new name",preferredStyle: .alert)
        let saveAction = UIAlertAction(title: "Save",style: .default) {
            [unowned self] action in
            guard let textField = alert.textFields?.first,
                let nameToSave = textField.text else {return}
            graph.setValue(nameToSave, forKeyPath: "name")
            self.EntryTableView.reloadData()
            
            if managedContext.hasChanges{
                DispatchQueue.global(qos: .utility).async { //(flags: .barrier)
                    DispatchQueue.main.async {
                        do {
                            try managedContext.save()
                        } catch let error as NSError {
                            print("Could not save. \(error), \(error.userInfo)")
                        }
                    }
                }
            }
        }
        let cancelAction = UIAlertAction(title: "Cancel",style: .default)
        alert.addTextField()
        alert.addAction(saveAction)
        alert.addAction(cancelAction)
        present(alert, animated: true)
    }
    func deleteAlert(graph: NSManagedObject){
        guard let appDelegate = UIApplication.shared.delegate as? AppDelegate else {return}
        let managedContext = appDelegate.managedObjectContext
        let alert = UIAlertController(title: "Delete Graph",message: "Are you sure you want to delete this graph?",preferredStyle: .alert)
        let saveAction = UIAlertAction(title: "Delete",style: .default) {
            [unowned self] action in
            managedContext.delete(graph)
            self.EntryTableView.reloadData()
            if managedContext.hasChanges{
                DispatchQueue.global(qos: .utility).async { //(flags: .barrier)
                    DispatchQueue.main.async {
                        do {
                            try managedContext.save()
                        } catch let error as NSError {
                            print("Could not save. \(error), \(error.userInfo)")
                        }
                    }
                }
            }
        }
        let cancelAction = UIAlertAction(title: "Cancel",style: .default)
        alert.addAction(saveAction)
        alert.addAction(cancelAction)
        present(alert, animated: true)
    }
    func openGraph(graph: NSManagedObject){
        let vc = storyboard?.instantiateViewController(withIdentifier: "GraphArchiveSelected") as? GraphArchiveSelectedVC
        vc?.selectedGraph = graph as! GraphDataSet
        navigationController?.pushViewController(vc!, animated: true)
        fromGraph = true
    }
    //----------------------------------
    // End of User interactive functions
    //----------------------------------
}

// Class for table cells
class GraphTableViewCell: UITableViewCell {
    
    @IBOutlet weak var DateLabel: UILabel!
    @IBOutlet weak var DeviceLabel: UILabel!
    @IBOutlet weak var NameLabel: UIButton!
    var graphForRow: NSManagedObject!
    weak var delegate: CustomCellUpdater?
    
    
    var displayGraph: DisplayGraph? {
        didSet {
            if let date = displayGraph!.date{
                DateLabel.text = date
            }
            if let deviceName = displayGraph!.device {
                DeviceLabel.text = deviceName //"Device: "+deviceName
            }
            if let graphName = displayGraph!.name {
                NameLabel.setTitle(graphName, for: .normal)
            }else{
                NameLabel.setTitle("Graph Name", for: .normal)
            }
            if let graph = displayGraph!.graphForRow{
                graphForRow = graph
            }
        }
    }
    
    @IBAction func RenameEntry(_ sender: Any) {
        self.delegate?.renameAlert(graph: graphForRow)
    }
    @IBAction func OpenGraph(_ sender: Any) {
        self.delegate?.openGraph(graph: graphForRow)
    }
    @IBAction func DeleteGraph(_ sender: Any) {
        self.delegate?.deleteAlert(graph: graphForRow)
    }
}

