//
//  GraphViewController.swift
//  GammaGuard
//
//  Created by EIC on 2018-05-14.
//  Copyright © 2018 EIC. All rights reserved.
//

import UIKit
import Charts
import CoreData


class GraphArchiveSelectedVC: BaseViewController, ChartViewDelegate{
    
    //------------------------
    // IBOutlets and Variables
    //------------------------
    @IBOutlet weak var Graph: LineChartView!
    @IBOutlet weak var TimeLabel: UILabel!
    @IBOutlet weak var CountRateLabel: UILabel!
    @IBOutlet weak var LatitudeLabel: UILabel!
    @IBOutlet weak var LongitudeLabel: UILabel!
    @IBOutlet weak var MapButton: UIButton!
    @IBOutlet weak var ResponseSlider: UISlider!
    @IBOutlet weak var MinimumCountsSlider: UISlider!
    @IBOutlet weak var BackButton: UIButton!
    @IBOutlet weak var HistoryButton: UIButton!
    @IBOutlet weak var ResponseLabel: UIButton!
    @IBOutlet weak var MinimumCountsLabel: UIButton!
    @IBOutlet weak var RateLabel: UILabel!
    @IBOutlet weak var UnitsLabel: UILabel!
    @IBOutlet weak var DateLabel: UILabel!
    
    
    let CTData = CT007Data()
    let dateFormatter = DateFormatter()
    var highlightedLatitude: Double = 0
    var highlightedLongitude: Double = 0
    var responseTimeInt: Int = 0
    var minimumCounts: Int = 100
    var minChartEntry = [ChartDataEntry]()
    var maxChartEntry = [ChartDataEntry]()
    var intervalChartEntry = [ChartDataEntry]()
    var historyChartNeedsRefresh = true
    var intervalChartNeedsRefresh = false
    var graphType = "history"
    var recentInterval: Int = 20 * 60 // Stored in seconds (Min*(60sec/min))
    var selectedHistoryInterval: Int = 20 * 60 // Stored in seconds (Min*(60sec/min))
    var highlightCounter: Int = 0
    var minMaxIndexArray = [[Int]]()
    var intervalSize: Int = 0
    let valuesOnHist: Int = 50
    var indexOnGraphCountArray: Int = 0
    let gradientColors1 = [ChartColorTemplates.colorFromString("#00ff0000").cgColor,
                           ChartColorTemplates.colorFromString("#ffff0000").cgColor]
    let gradientColors2 = [ChartColorTemplates.colorFromString("#00ff0000").cgColor,
                           ChartColorTemplates.colorFromString("#ff0000ff").cgColor]
    weak var graphTimer: Timer?
    var selectedGraph: NSManagedObject!
    var points = NSMutableOrderedSet()
    var sv = UIView()
    var stopLoading = false
    var currentlyLoading = false
    var CPStouSvh: Double = 1.0
    var pointFrequency: Int = 1
    //-------------------------------
    // End of IBOutlets and Variables
    //-------------------------------
    
    
    //--------------------------
    // View Controller Functions
    //--------------------------
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Load settings
        addModeButton()
        loadGraphConstants()
        loadSavedSettings()
        Graph.delegate = self
        self.navigationItem.titleView = historyGraphLabel()
        dateFormatter.dateFormat = "HH:mm:ss"
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(true)
        
        // Stop timer
        if graphTimer != nil {
            graphTimer?.invalidate()
            graphTimer = nil
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        
        // Load saved conversion ratio for detector that was used to record and starting time of graph
        CPStouSvh = selectedGraph.value(forKey: "cpsTouSvh") as! Double
        pointFrequency = selectedGraph.value(forKey: "pointFrequency") as! Int
        if pointFrequency == 0{
            pointFrequency = 1
        }
        archiveGraphBaseTime = selectedGraph.value(forKey: "dateSaved") as! Int
        // Checking mode
        if CT007Mode == "Contamination"{
            RateLabel.text = "Count Rate"
            UnitsLabel.text = CT007_ContamUnits
        }else{
            RateLabel.text = "Dose Rate"
            UnitsLabel.text = CT007_GammaUnits
        }
        // Display loading indicator if not already
        if currentlyLoading == false{
            sv = UIViewController.displaySpinner(onView: self.view)
        }
        // Hide graph view until done loading and load in points
        currentlyLoading = true
        Graph.isHidden = true
        points = selectedGraph.mutableOrderedSetValue(forKey: "dataPoints")
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(true)
        
        // Update graphview and restart timer
        updateGraph()
        if graphTimer == nil{
            graphTimer = Timer.scheduledTimer(timeInterval: 1, target: self,  selector: #selector(updateGraph), userInfo: nil, repeats: true)
        }
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(true)
        
        // Reset zoom and unhighlight selected point if there is
        Graph.fitScreen()
        Graph.highlightValue(x: 0, dataSetIndex: -1)
    }
    //---------------------------------
    // End of View Controller Functions
    //---------------------------------
    
    
    //----------------------
    // Mode Button Functions
    //----------------------
    func addModeButton(){
        let btnMode = UIButton(type: UIButton.ButtonType.system)
        btnMode.setImage(self.defaultModeImage(), for: UIControl.State())
        btnMode.frame = CGRect(x: 0, y: 0, width: 60, height: 30)
        btnMode.addTarget(self, action: #selector(ModeButtonPressed), for: UIControl.Event.touchUpInside)
        let customBarItem = UIBarButtonItem(customView: btnMode)
        self.navigationItem.rightBarButtonItem = customBarItem;
    }
    @objc func ModeButtonPressed(){
        if CT007Mode == "Gamma"{
            print("loading contamination settings")
            CT007Mode = "Contamination"
            RateLabel.text = "Count Rate"
            UnitsLabel.text = CT007_ContamUnits
            defaults.set(CT007Mode, forKey: "Saved_Mode")
        }else if CT007Mode == "Contamination"{
            print("loading gamma settings")
            CT007Mode = "Gamma"
            RateLabel.text = "Dose Rate"
            UnitsLabel.text = CT007_GammaUnits
            defaults.set(CT007Mode, forKey: "Saved_Mode")
        }else{
            print("Invalid Mode")
        }
        Graph.fitScreen()
        historyChartNeedsRefresh = true
        intervalChartNeedsRefresh = true
        Graph.highlightValue(x: 0, dataSetIndex: -1)
        updateGraph()
    }
    //-----------------------------
    // End of Mode button functions
    //-----------------------------
    
    //---------------------------
    // Loading settings functions
    //---------------------------
    func loadGraphConstants(){
        let marker = BalloonMarker(color: UIColor.orange,font: .systemFont(ofSize: 12),textColor: .black,insets: UIEdgeInsets(top: 8, left: 8, bottom: 20, right: 8))
        marker.chartView = Graph
        marker.minimumSize = CGSize(width: 80, height: 40)
        Graph.leftAxis.enabled = false
        Graph.xAxis.valueFormatter = TimeFormatter()
        Graph.xAxis.labelCount = 3
        Graph.noDataText = ""
        Graph.xAxis.avoidFirstLastClippingEnabled = true
        Graph.marker = marker
        Graph.maxVisibleCount = 100
        Graph.rightAxis.axisMinimum = 0
    }
    
    func loadSavedSettings(){
        if defaults.object(forKey: "Saved_GraphMinimumCounts") != nil {
            minimumCounts = defaults.integer(forKey: "Saved_GraphMinimumCounts")
            MinimumCountsSlider.value = Float(minimumCounts)
            MinimumCountsLabel.setTitle("Minimum Counts: \(String(minimumCounts))", for: .normal)
            print ("Saved_GraphMinimumCounts not nil")
        }
        if defaults.object(forKey: "Saved_GraphResponseTime") != nil {
            responseTimeInt = defaults.integer(forKey: "Saved_GraphResponseTime")
            ResponseSlider.value = Float(responseTimeInt)
            print ("Saved_GraphResponseTime not nil")
            responseTimeInt = Int(ResponseSlider.value)
            ResponseLabel.setTitle("Response Time: \(String(responseTimeInt))", for: .normal)
            MinimumCountsLabel.setTitle("Disabled when not in Auto Mode", for: .normal)
            MinimumCountsSlider.isEnabled = false
            MinimumCountsLabel.isEnabled = false
            if responseTimeInt == 0{
                MinimumCountsLabel.setTitle("Minimum Counts: \(String(minimumCounts))", for: .normal)
                ResponseLabel.setTitle("Response Time: Auto Mode", for: .normal)
                MinimumCountsSlider.isEnabled = true
                MinimumCountsLabel.isEnabled = true
            }
        }
    }
    //----------------------------------
    // End of Loading settings functions
    //----------------------------------
    
    //--------------------------
    // Point Selection functions
    //--------------------------
    func chartValueSelected(_ chartView: ChartViewBase, entry: ChartDataEntry, highlight: Highlight) {
        indexOnGraphCountArray = Int(Graph.highlighted[0].x)
        highlightCounter = indexOnGraphCountArray + 2
        if graphType == "history"{
            HistoryButton.isEnabled = true
            indexOnGraphCountArray /= intervalSize
            indexOnGraphCountArray = minMaxIndexArray[indexOnGraphCountArray][Graph.highlighted[0].dataSetIndex]
        }else{
            indexOnGraphCountArray /= pointFrequency
        }
        if indexOnGraphCountArray >= points.count{
            indexOnGraphCountArray = points.count - 1
        }
        if graphType == "historyinterval"{
            var pointTime = (points[indexOnGraphCountArray] as! GraphDataPoint).time
            let highlightedTime = Int(Graph.highlighted[0].x)
            while pointTime != highlightedTime{
                if pointTime > highlightedTime{indexOnGraphCountArray -= 1}
                else{indexOnGraphCountArray += 1}
                pointTime = (points[indexOnGraphCountArray] as! GraphDataPoint).time
            }
        }
        let point = points[indexOnGraphCountArray] as! GraphDataPoint
        let selectedTime = Int(point.time) + archiveGraphBaseTime
        TimeLabel.text = secondsToHoursMinutesSeconds(seconds: Double(selectedTime))
        highlightedLatitude = (point.latitude)
        highlightedLongitude = (point.longitude)
        if CT007Mode == "Gamma"{
            CountRateLabel.text = String(format: "%.2f", Graph.highlighted[0].y) + " " + CT007_GammaUnits
        }else{
            CountRateLabel.text = String(format: "%.2f", Graph.highlighted[0].y) + " " + CT007_ContamUnits
        }
        LatitudeLabel.text = "Lat: " + String(format: "%.4f", highlightedLatitude)
        LongitudeLabel.text = "Long: " + String(format: "%.4f", highlightedLongitude)
        MapButton.backgroundColor = UIColor.orange
        MapButton.isEnabled = true
    }
    
    func chartValueNothingSelected(_ chartView: ChartViewBase){
        if graphType == "history"{HistoryButton.isEnabled = false}
        TimeLabel.text = ""
        highlightedLatitude = 0
        highlightedLongitude = 0
        CountRateLabel.text = ""
        LatitudeLabel.text = "Lat: "
        LongitudeLabel.text = "Long: "
        MapButton.backgroundColor = UIColor.gray
        MapButton.isEnabled = false
    }
    //---------------------------------
    // End of Point Selection functions
    //---------------------------------
    
    // Used to get CPM value for graph of certain index of the Count array
    // startIndex should be index on count array and not graph
    func getDoseRate(startIndex: Int) -> Double{
        if startIndex == 0{return 0}
        var doserate: Double = 0.0
        var avgTotal: Double = 0
        if responseTimeInt == 0{
            var values: Int = 0
            while Int(avgTotal) <= minimumCounts && values < 30{
                let index = startIndex - values
                if index >= 0{
                    let point = points[index] as! GraphDataPoint
                    avgTotal += Double(point.count)
                    values += 1
                }else{
                    break
                }
            }
            doserate = Double(avgTotal) / Double(values)
            if (doserate < 0) {doserate = 0}
        }
        else{
            var values: Int = 0
            for i in 0..<responseTimeInt {
                values = i + 1
                let counter = startIndex - i - 1
                if counter >= 0{
                    let point = points[counter] as! GraphDataPoint
                    avgTotal += Double(point.count)
                }else{
                    break
                }
            }
            doserate = Double(avgTotal) / Double(values)
            if (doserate < 0) {doserate = 0}
        }
        if CT007Mode == "Contamination"{
            if CT007_ContamUnits == "CPM"{
                return (doserate * 60)/Double(pointFrequency)
            }
            return doserate/Double(pointFrequency)
        }
        else{
            switch CT007_GammaUnits {
            case "µSv/h":
                break
            case "mSv/h":
                doserate /= 1000
            case "mRem/h":
                doserate /= 10
            default:
                print("invalid units")
            }
            return (doserate * CPStouSvh)/Double(pointFrequency)
        }
    }
    
    
    func secondsToHoursMinutesSeconds (seconds : Double) -> String{return dateFormatter.string(from: Date(timeIntervalSince1970: TimeInterval(seconds)))}
    
    
    //-------------------------
    // Graph Creation functions
    //-------------------------
    func intervalGraphCreation() -> LineChartDataSet{
        if intervalChartNeedsRefresh{
            intervalChartNeedsRefresh = false
            if currentlyLoading == false{
                sv = UIViewController.displaySpinner(onView: self.view)
                currentlyLoading = true
                Graph.isHidden = true
            }
            DispatchQueue.global(qos: .utility).async(flags: .barrier) {
                self.intervalChartEntry.removeAll()
                var startIndex = self.indexOnGraphCountArray - (self.selectedHistoryInterval/2)
                if startIndex < 0 {startIndex = 0}
                if self.selectedHistoryInterval > self.points.count{
                    for i in 0..<self.points.count{
                        let point = self.points[i] as! GraphDataPoint
                        let value = ChartDataEntry(x: Double(point.time), y: self.getDoseRate(startIndex: i))
                        self.intervalChartEntry.append(value)
                    }
                }
                else if startIndex + self.selectedHistoryInterval > self.points.count{
                    startIndex = (self.points.count-1) - self.selectedHistoryInterval
                    for i in 0..<self.selectedHistoryInterval{
                        let point = self.points[startIndex+i] as! GraphDataPoint
                        let value = ChartDataEntry(x: Double(point.time), y: self.getDoseRate(startIndex: startIndex+i))
                        self.intervalChartEntry.append(value)
                    }
                }else{
                    for i in 0..<self.selectedHistoryInterval{
                        let point = self.points[startIndex+i] as! GraphDataPoint
                        let value = ChartDataEntry(x: Double(point.time), y: self.getDoseRate(startIndex: startIndex+i))
                        self.intervalChartEntry.append(value)
                    }
                }
                self.stopLoading = true
            }
        }
        return LineChartDataSet(values: intervalChartEntry, label: "Count Rate")
    }
    
    //Graph for Min and Max
    func MinMaxGraphCreation() -> LineChartDataSet{
        if points.count > valuesOnHist{
            intervalSize = points.count / valuesOnHist
            if historyChartNeedsRefresh == true{
                historyChartNeedsRefresh = false
                if currentlyLoading == false{
                    sv = UIViewController.displaySpinner(onView: self.view)
                    currentlyLoading = true
                    Graph.isHidden = true
                }
                DispatchQueue.global(qos: .utility).async(flags: .barrier) {
                    var minMaxValuesArray = [[Double]]()
                    self.minMaxIndexArray.removeAll()
                    for i in 0..<self.points.count/self.intervalSize{
                        var tempValueArray = [Double]()
                        var tempIndexArray = [Int]()
                        var maxVal: Double = -1
                        var minVal: Double = Double.greatestFiniteMagnitude
                        var maxIndex: Int = 0
                        var minIndex: Int = 0
                        for x in 0..<self.intervalSize{
                            let index = i*self.intervalSize + x
                            let value  = self.getDoseRate(startIndex: index)
                            if value > maxVal {
                                maxVal = value
                                maxIndex = index
                            }
                            if value < minVal {
                                minVal = value
                                minIndex = index
                            }
                        }
                        tempValueArray.append(minVal)
                        tempValueArray.append(maxVal)
                        tempIndexArray.append(maxIndex)
                        tempIndexArray.append(minIndex)
                        minMaxValuesArray.append(tempValueArray)
                        self.minMaxIndexArray.append(tempIndexArray)
                    }
                    //min line creating
                    self.minChartEntry.removeAll()
                    for i in 0..<minMaxValuesArray.count{
                        let point = self.points[0] as! GraphDataPoint
                        let value = ChartDataEntry(x: Double(i*self.intervalSize+Int(point.time)), y: minMaxValuesArray[i][0])
                        self.minChartEntry.append(value)
                    }
                    //max line creating
                    self.maxChartEntry.removeAll()
                    for i in 0..<minMaxValuesArray.count{
                        let point = self.points[0] as! GraphDataPoint
                        let value = ChartDataEntry(x: Double(i*self.intervalSize+Int(point.time)), y: minMaxValuesArray[i][1])
                        self.maxChartEntry.append(value)
                    }
                    self.stopLoading = true
                }
            }
            return LineChartDataSet(values: minChartEntry, label: "Minimum Values")
        }else{
            minChartEntry = [ChartDataEntry]()
            maxChartEntry = [ChartDataEntry]()
            return LineChartDataSet(values: minChartEntry, label: "Minimum Values")
        }
    }
    //--------------------------------
    // End of Graph Creation functions
    //--------------------------------
    
    
    // Called every second
    @objc func updateGraph(){
        let gradient1 = CGGradient(colorsSpace: nil, colors: gradientColors1 as CFArray, locations: nil)!
        let gradient2 = CGGradient(colorsSpace: nil, colors: gradientColors2 as CFArray, locations: nil)!
        var line1 = LineChartDataSet()
        var line2 = LineChartDataSet()
        Graph.xAxis.granularity = 1
        if graphType == "history"{
            line1 = MinMaxGraphCreation()
            line2 = LineChartDataSet(values: maxChartEntry, label: "Max Values")
            line2.drawCirclesEnabled = false
            line2.drawValuesEnabled = false
            line2.lineWidth = 1
            line2.colors = [NSUIColor.red]
            line2.highlightLineWidth = 1
            line2.highlightColor = UIColor.black
            line2.fillAlpha = 1
            line2.fill = Fill(linearGradient: gradient1, angle: 90)
            line2.drawFilledEnabled = true
            line2.axisDependency = .right
            Graph.xAxis.granularity = Double(intervalSize)
        }
        if graphType == "historyinterval"{
            line1 = intervalGraphCreation()
        }
        line1.drawCirclesEnabled = false
        line1.drawValuesEnabled = false
        line1.lineWidth = 1
        line1.colors = [NSUIColor.black]
        line1.highlightLineWidth = 1
        line1.highlightColor = UIColor.black
        line1.fillAlpha = 1
        line1.fill = Fill(linearGradient: gradient1, angle: 90)
        line1.drawFilledEnabled = true
        line1.axisDependency = .right
        let data = LineChartData()
        Graph.legend.enabled = false
        if graphType == "history"{
            line1.fill = Fill(linearGradient: gradient2, angle: 90)
            line1.colors = [NSUIColor.blue]
            line1.fillAlpha = 1
            data.addDataSet(line2)
            Graph.legend.enabled = true
        }
        data.addDataSet(line1)
        Graph.data = data
        Graph.chartDescription?.enabled = false
        if stopLoading{
            UIViewController.removeSpinner(spinner: self.sv)
            stopLoading = false
            currentlyLoading = false
            Graph.isHidden = false
        }
        let dayTimePeriodFormatter = DateFormatter()
        dayTimePeriodFormatter.dateFormat = "MMM d, yyyy"
        DateLabel.text = dayTimePeriodFormatter.string(from: Date(timeIntervalSince1970: TimeInterval(archiveGraphBaseTime)))
    }
    
    //---------------------------
    // User Interaction functions
    //---------------------------
    @IBAction func OpenMapPressed(_ sender: Any) {
        if highlightedLongitude == 0{return}
        let url = "http://maps.apple.com/?q=\(highlightedLatitude),\(highlightedLongitude)"
        UIApplication.shared.openURL(URL(string:url)!)
    }

    @IBAction func BackButtonPressed(_ sender: Any) {
        Graph.fitScreen()
        Graph.highlightValue(x: 0, dataSetIndex: -1)
        graphType = "history"
        HistoryButton.isHidden = false
        BackButton.isHidden = true
        self.navigationItem.titleView = historyGraphLabel()
        updateGraph()
    }
    
    @IBAction func HistoryButtonPressed(_ sender: Any) {
        Graph.fitScreen()
        Graph.highlightValue(x: 0, dataSetIndex: -1)
        graphType = "historyinterval"
        HistoryButton.isHidden = true
        BackButton.isHidden = false
        intervalChartNeedsRefresh = true
        self.navigationItem.titleView = intervalGraphLabel()
        updateGraph()
    }
    
    @IBAction func ResponseSliderChanged(_ sender: Any) {
        Graph.fitScreen()
        historyChartNeedsRefresh = true
        intervalChartNeedsRefresh = true
        Graph.highlightValue(x: 0, dataSetIndex: -1)
        responseTimeInt = Int(ResponseSlider.value)
        ResponseLabel.setTitle("Response Time: \(String(responseTimeInt))", for: .normal)
        MinimumCountsSlider.isEnabled = false
        MinimumCountsLabel.isEnabled = false
        MinimumCountsLabel.setTitle("Disabled when not in Auto Mode", for: .normal)
        if responseTimeInt == 0{
            MinimumCountsLabel.setTitle("Minimum Counts: \(String(minimumCounts))", for: .normal)
            ResponseLabel.setTitle("Response Time: Auto Mode", for: .normal)
            MinimumCountsSlider.isEnabled = true
            MinimumCountsLabel.isEnabled = true
        }
        defaults.set(responseTimeInt, forKey: "Saved_GraphResponseTime")
        updateGraph()
    }
    @IBAction func MinimumCountsSliderChanged(_ sender: Any) {
        Graph.fitScreen()
        historyChartNeedsRefresh = true
        intervalChartNeedsRefresh = true
        Graph.highlightValue(x: 0, dataSetIndex: -1)
        minimumCounts = Int(MinimumCountsSlider.value)
        MinimumCountsLabel.setTitle("Minimum Counts: \(String(minimumCounts))", for: .normal)
        defaults.set(minimumCounts, forKey: "Saved_GraphMinimumCounts")
        updateGraph()
    }
    
    @IBAction func ResponseManualChange(_ sender: Any) {
        let alert = UIAlertController(title: "Response Time",message: "Enter a Response Time between 0-30",preferredStyle: .alert)
        let saveAction = UIAlertAction(title: "Save",style: .default) {
            [unowned self] action in
            guard let textField = alert.textFields?.first,
                let responseTimeText = textField.text else {return}
            if let IntCheck = Int(responseTimeText){
                if IntCheck < 0 || IntCheck > 30{
                    return
                }
            }else{return}
            self.responseTimeInt = Int(responseTimeText)!
            self.ResponseLabel.setTitle("Response Time: \(String(self.responseTimeInt))", for: .normal)
            defaults.set(self.responseTimeInt, forKey: "Saved_GraphResponseTime")
            self.ResponseSlider.value = Float(self.responseTimeInt)
            self.Graph.fitScreen()
            self.historyChartNeedsRefresh = true
            self.intervalChartNeedsRefresh = true
            self.MinimumCountsSlider.isEnabled = false
            self.MinimumCountsLabel.isEnabled = false
            self.MinimumCountsLabel.setTitle("Disabled when not in Auto Mode", for: .normal)
            if self.responseTimeInt == 0{
                self.MinimumCountsLabel.setTitle("Minimum Counts: \(String(self.minimumCounts))", for: .normal)
                self.ResponseLabel.setTitle("Response Time: Auto Mode", for: .normal)
                self.MinimumCountsSlider.isEnabled = true
                self.MinimumCountsLabel.isEnabled = true
            }
            self.Graph.highlightValue(x: 0, dataSetIndex: -1)
            self.updateGraph()
        }
        let cancelAction = UIAlertAction(title: "Cancel",style: .default)
        alert.addTextField()
        alert.addAction(saveAction)
        alert.addAction(cancelAction)
        present(alert, animated: true)
    }
    
    @IBAction func MinCountsManualChange(_ sender: Any) {
        let alert = UIAlertController(title: "Minimum Counts",message: "Enter the minimum counts value between 1-1000",preferredStyle: .alert)
        let saveAction = UIAlertAction(title: "Save",style: .default) {
            [unowned self] action in
            guard let textField = alert.textFields?.first,
                let minCountsText = textField.text else {return}
            if let IntCheck = Int(minCountsText){
                if IntCheck < 1 || IntCheck > 1000{
                    return
                }
            }else{return}
            self.minimumCounts = Int(minCountsText)!
            self.MinimumCountsLabel.setTitle("Minimum Counts: \(String(self.minimumCounts))", for: .normal)
            defaults.set(self.minimumCounts, forKey: "Saved_GraphMinimumCounts")
            self.MinimumCountsSlider.value = Float(self.minimumCounts)
            self.Graph.fitScreen()
            self.historyChartNeedsRefresh = true
            self.intervalChartNeedsRefresh = true
            self.Graph.highlightValue(x: 0, dataSetIndex: -1)
            self.updateGraph()
        }
        let cancelAction = UIAlertAction(title: "Cancel",style: .default)
        alert.addTextField()
        alert.addAction(saveAction)
        alert.addAction(cancelAction)
        present(alert, animated: true)
    }
    //----------------------------------
    // End of User Interaction functions
    //----------------------------------
    
    func recentGraphLabel() -> UILabel{
        let label = UILabel(frame: CGRect(x:0, y:0, width:400, height:50))
        label.backgroundColor = .clear
        label.numberOfLines = 2
        label.font = UIFont.boldSystemFont(ofSize: 16.0)
        label.textAlignment = .center
        label.textColor = .black
        label.text = "Latest \(20*graphPointDuration) Minutes\nDetector: \(selectedGraph.value(forKey: "deviceUsed") as! String)"
        return label
    }
    func historyGraphLabel() -> UILabel{
        let label = UILabel(frame: CGRect(x:0, y:0, width:400, height:50))
        label.backgroundColor = .clear
        label.numberOfLines = 2
        label.font = UIFont.boldSystemFont(ofSize: 16.0)
        label.textAlignment = .center
        label.textColor = .black
        label.text = "Overall History\nDetector: \(selectedGraph.value(forKey: "deviceUsed") as! String)"
        return label
    }
    func intervalGraphLabel() -> UILabel{
        let label = UILabel(frame: CGRect(x:0, y:0, width:400, height:50))
        label.backgroundColor = .clear
        label.numberOfLines = 2
        label.font = UIFont.boldSystemFont(ofSize: 16.0)
        label.textAlignment = .center
        label.textColor = .black
        label.text = "Selected Interval\nDetector: \(selectedGraph.value(forKey: "deviceUsed") as! String)"
        return label
    }
}

// Functions for loading spinner
extension UIViewController {
    class func displaySpinner(onView : UIView) -> UIView {
        let xCenter = onView.bounds.size.width / 2
        let yCenter = onView.bounds.size.height / 2
        let spinnerView = UIView.init(frame: CGRect(x: xCenter - 35, y: yCenter - 35, width: 70, height: 70))
        spinnerView.backgroundColor = UIColor.init(red: 0.5, green: 0.5, blue: 0.5, alpha: 0.3)
        spinnerView.layer.cornerRadius = 20
        let ai = UIActivityIndicatorView.init(style: .whiteLarge)
        ai.color = UIColor.black
        ai.alpha = 0.8
        ai.startAnimating()
        ai.center = CGPoint(x: 35, y: 35)
        
        DispatchQueue.main.async {
            spinnerView.addSubview(ai)
            onView.addSubview(spinnerView)
        }
        
        return spinnerView
    }
    
    class func removeSpinner(spinner :UIView) {
        DispatchQueue.main.async {
            spinner.removeFromSuperview()
        }
    }
}

