////
////  ViewController.swift
////  CT007
////
////  Created by Sepehr Khatir on 15/03/2017.
////  Copyright © 2017 EIC. All rights reserved.
////
//
//import UIKit
//import CoreBluetooth
//
//class CT007ViewController: UIViewController, CBCentralManagerDelegate, CBPeripheralDelegate {
//    
//    var ScanTimer: Timer!
//    
//    // Title labels
//    var titleLabel : UILabel!
//    var statusLabel : UILabel!
//    
//    @IBOutlet weak var batteryLabel: UILabel!
//    @IBOutlet weak var countsLabel: UILabel!
//    @IBOutlet weak var countsLabel2: UILabel!
//    @IBOutlet weak var countsLabel3: UILabel!
//    @IBOutlet weak var connectButton: UIButton!
//    
//    // Status flag
//    var didConnectFlag = false
//    
//    // BLE
//    var centralManager : CBCentralManager!
//    var devicePeripheral : CBPeripheral!
//    
//    
//
//    
//    func back(sender: UIBarButtonItem) {
//        // Perform your custom actions
//        // ...
//        // Go back to the previous ViewController
//        _ = navigationController?.popViewController(animated: true)
//    }
//    
//    
//    override func viewDidLoad() {
//        super.viewDidLoad()
//        
//        self.navigationItem.hidesBackButton = true
//        let newBackButton = UIBarButtonItem(title: "Scan page",
//                                            style: UIBarButtonItemStyle.plain,
//                                            target: self,
//                                            action: #selector(CT007ViewController.back(sender:)))
//        
//        self.navigationItem.leftBarButtonItem = newBackButton
//        
//        // Do any additional setup after loading the view, typically from a nib.
//        batteryLabel!.layer.borderWidth = 1
//        countsLabel!.layer.borderWidth = 1
//        countsLabel2!.layer.borderWidth = 1
//        countsLabel3!.layer.borderWidth = 1
//        
//        // Initialize central manager on load
//        centralManager = CBCentralManager(delegate: self, queue: nil)
//        
//        // Set up title label
//        titleLabel = UILabel()
//        titleLabel.text = "CT007-22"
//        titleLabel.font = UIFont(name: "HelveticaNeue-Bold", size: 20)
//        titleLabel.sizeToFit()
//        titleLabel.center = CGPoint(x: self.view.frame.midX, y: self.titleLabel.bounds.midY+61)
//        self.view.addSubview(titleLabel)
//        
//        // Set up status label
//        statusLabel = UILabel()
//        statusLabel.textAlignment = NSTextAlignment.center
//        statusLabel.text = "Loading..."
//        statusLabel.font = UIFont(name: "HelveticaNeue-Light", size: 12)
//        statusLabel.sizeToFit()
//        statusLabel.frame = CGRect(x: self.view.frame.origin.x, y: self.titleLabel.frame.maxY, width: self.view.frame.width, height: self.statusLabel.bounds.height)
//        self.view.addSubview(statusLabel)
//    }
//    
//    func back(_ sender: AnyObject) {
//        UIView.animate(withDuration: 0.5, animations: {
//                        self.view.alpha = 0.0
//                    }, completion: {_ in
//                        self.dismiss(animated: false, completion: nil)
//                    }) 
//        
//
//        // Perform your custom actions
//        // ...
//        // Go back to the previous ViewController
//        _ = navigationController?.popViewController(animated: true)
//    }
//    
//    
//    @IBAction func disconnectButtonPressed(_ sender: Any) {
//        UIView.animate(withDuration: 0.5, animations: {
//                        self.view.alpha = 0.0
//                    }, completion: {_ in
//                        self.dismiss(animated: false, completion: nil)
//                    })
//          _ = navigationController?.popViewController(animated: true)
//                }
//
//    
//
//    
//    
//    func startScanTimer (Interval: Int) {
//        if ScanTimer == nil {
//            ScanTimer = Timer.scheduledTimer(
//                timeInterval: TimeInterval(Interval),
//                target: self,
//                selector: #selector(ScanTimedCode),
//                userInfo: nil,
//                repeats: true)
//            
//        }
//    }
//    func stopScanTimerTest() {
//        if ScanTimer != nil {
//            ScanTimer.invalidate()
//            ScanTimer = nil
//        }
//    }
//    
//    
//    func ScanTimedCode() {
//
//    }
//
//    
//    override func didReceiveMemoryWarning() {
//        super.didReceiveMemoryWarning()
//        // Dispose of any resources that can be recreated.
//    }
//    
//    /******* CBCentralManagerDelegate *******/
//    
//    // Check status of BLE hardware
//    func centralManagerDidUpdateState(_ central: CBCentralManager) {
//        if central.state == .poweredOn {
//            // Scan for peripherals if BLE is turned on
//            startScanTimer (Interval: 1)
//            
//            self.statusLabel.text = "Searching for BLE Devices"
//            
//
//
//            
//            
//        }
//    }
//   
//    // Wait for user to press connect button to begin communication with BLE device
//    @IBAction func connectToDevice(_ sender : AnyObject) {
//        if (!didConnectFlag) {
//            didConnectFlag = true
//            self.connectButton.setTitle("Disconnect", for: UIControlState())
//        }
//            
//        else if (didConnectFlag) {
//            didConnectFlag = false
//            self.connectButton.setTitle("Connect", for: UIControlState())
//            self.batteryLabel.text = ""
//            self.countsLabel.text = ""
//            self.countsLabel2.text = ""
//            self.countsLabel3.text = ""
//            self.statusLabel.text = "Searching for BLE Devices"
//        }
//    }
//        
//        // Check out the discovered peripherals to find Sensor Tag
////        func centralManager(_ central: CBCentralManager, didDiscover peripheral: CBPeripheral, advertisementData: [String : Any], rssi RSSI: NSNumber) {
////            if (1 == 1) {
////                if CT007.deviceFound(advertisementData) == true {
////                    print ("yeeeeeees ... we found it .......")
////                    
////                    // Update Status Label
////                    self.statusLabel.text = "CT007-22 Found"
////                    
////                    // Stop scanning, set as the peripheral to use and establish connection
////                    self.centralManager.stopScan()
////                    self.devicePeripheral = peripheral
////                    self.devicePeripheral.delegate = self
////                    self.centralManager.connect(peripheral, options: nil)
////                }
////                else {
////                    print ("NOoooooooo ... we didnt find it .......")
////                    self.statusLabel.text = "CT007-22 NOT Found"
////                }
////            }            
////        }
//    
//    // Discover services of the peripheral
//    func centralManager(_ central: CBCentralManager, didConnect peripheral: CBPeripheral) {
//        self.statusLabel.text = "Discovering peripheral services"
//        peripheral.discoverServices(nil)
//    }
//    
//    // If disconnected, start searching again
//    func centralManager(_ central: CBCentralManager, didDisconnectPeripheral peripheral: CBPeripheral, error: Error?) {
//        self.statusLabel.text = "Disconnected"
//        central.scanForPeripherals(withServices: nil, options: nil)
//    }
//    
//    /******* CBCentralPeripheralDelegate *******/
//    
//    // Check if the service discovered is valid i.e.:
//    // Radiation Count or Battery Life
//    func peripheral(_ peripheral: CBPeripheral, didDiscoverServices error: Error?) {
//        self.statusLabel.text = "Looking at peripheral services"
//        for service in peripheral.services! {
//            let thisService = service as CBService
//            
//            if CT007.isBatteryService(thisService) {
//                // Discover characteristics of all valid services
//                self.statusLabel.text = "Discovered battery service"
//                peripheral.discoverCharacteristics(nil, for: thisService)
//            }
//            
//            if CT007.isRadiationService(thisService) {
//                // Discover characteristics of all valid services
//                self.statusLabel.text = "Discovered radiation service"
//                peripheral.discoverCharacteristics(nil, for: thisService)
//            }
//        }
//    }
//    
//    // Enable notification and sensor for each characteristic of valid service
//    func peripheral(_ peripheral: CBPeripheral, didDiscoverCharacteristicsFor service: CBService, error: Error?) {
//        
//        self.statusLabel.text = "Enabling radiation counter"
//        
//        for charateristic in service.characteristics! {
//            let thisCharacteristic = charateristic as CBCharacteristic
//            
//            if CT007.isRadiationCharacteristic(thisCharacteristic) {
//                self.devicePeripheral.setNotifyValue(true, for: thisCharacteristic)
//            }
//            
//            if CT007.isConversionCharacteristic(thisCharacteristic) {
//                self.devicePeripheral.readValue(for: thisCharacteristic)
//            }
//
//            
//            if CT007.isBatteryCharacteristic(thisCharacteristic) {
//                self.devicePeripheral.readValue(for: thisCharacteristic)
//            }
//        }
//    }
//    
//    // Get data values when they are updated
//    func peripheral(_ peripheral: CBPeripheral, didUpdateValueFor characteristic: CBCharacteristic, error: Error?) {
//    print ("we are here getting data from characteristics")
//        self.statusLabel.text = "Connected"
//        //print ("UUID :  ",characteristic.UUID);
//        
//        if characteristic.uuid == batteryLevel {
//            if let batteryLevel = characteristic.value {
//                print ("battery level", batteryLevel);
////                self.batteryLabel.text = CT007.getBatteryLevel(batteryLevel)
//            }
//        }
//        
//        if characteristic.uuid == Conversion_Factor {
//            if let conversionFactor = characteristic.value {
//                print ("Conv_Fact :  ", conversionFactor);
//                //self.batteryLabel.text = CT007.getBatteryLevel(batteryLevel)
//            }
//        }
//        
//        if characteristic.uuid == radiationCount {
//            if let radiationCount = characteristic.value {
////                let CPS = CT007.getRadiationCount(radiationCount)
////                let CPM = CT007.getRadiationCount(radiationCount) * 60
////                let Svhr = CT007.getRadiationCount(radiationCount) / 2
//                
//                //print (radiationCount, "   ", CPM , "   ", Svhr);
//                
//                //self.countsLabel.text = CT007.getRadiationCount(radiationCount)
////                self.countsLabel.text = String(CPM)
////                self.countsLabel2.text = String(Svhr)
////                self.countsLabel3.text = String(CPS)
//            }
//        }
//    }
//    
//    /******* Helper *******/
//    
//    // Show alert
//    func showAlertWithText (_ header : String = "Warning", message : String) {
//        let alert = UIAlertController(title: header, message: message, preferredStyle: UIAlertControllerStyle.alert)
//        alert.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.default, handler: nil))
//        alert.view.tintColor = UIColor.red
//        self.present(alert, animated: true, completion: nil)
//    }
//}
//
//
//
//extension CT007ViewController: DeviceCellDelegate{
//    func connectPressed(_ peripheral: CBPeripheral) {
//        print ("CT007...... 001 \(peripheral)")
//        if peripheral.state != .connected {
//            devicePeripheral = peripheral
//            peripheral.delegate = self
//            print ("CT007...... 002 \(devicePeripheral)")
//            centralManager?.connect(peripheral, options: nil)
//        }
//    }
//}
//
//
