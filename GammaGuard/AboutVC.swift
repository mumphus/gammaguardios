//
//  AboutVC.swift
//  GammaGuard
//
//  Created by sepehr khatir on 2017-04-28.
//  Copyright © 2017 EIC. All rights reserved.
//

import UIKit

class AboutVC: UITableViewController {
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        self.tabBarController?.tabBar.isHidden = true
        self.navigationItem.hidesBackButton = true
        let newBackButton = UIBarButtonItem(title: "< Detector", style: UIBarButtonItem.Style.plain, target: self, action: #selector(AboutVC.GoBacktoExternalView))
        self.navigationItem.leftBarButtonItem = newBackButton
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        switch(indexPath.row){
        case 0:
            UIApplication.shared.openURL(NSURL(string: "http://gammawatch.com")! as URL)
            break
            
        case 1:
            UIApplication.shared.openURL(NSURL(string: "http://gammawatch.com/ExternalDetectors.html")! as URL)
            break
            
        case 2:
            UIApplication.shared.openURL(NSURL(string: "http://gammawatch.com/resources")! as URL)
            break
            
        case 3:
            UIApplication.shared.openURL(NSURL(string: "http://gammawatch.com/contact-us")! as URL)
            break
            
        case 4:
            UIApplication.shared.openURL(NSURL(string: "http://gammawatch.com/Resources%20documents/iOS%20Manual.pdf")! as URL)
            break
            
        default:
            print ("same page view")
        }
        
        if indexPath.row == 1 {
            print ("row 1 about page")
            UIApplication.shared.openURL(NSURL(string: "http://gammawatch.com/UserManuals.html")! as URL)
            //here you can enter the action you want to start when cell 1 is clicked
            
        }
        
        
    }
    
    
    @objc func GoBacktoExternalView(){
        if !fromHome {
            performSegue(withIdentifier: "Ftoctsegue", sender: Any?.self)
        } else {
            self.navigationController!.popViewController(animated: true)
        }
        
    }
    
    override func numberOfSections(in tableView: UITableView) -> Int {
        return 1;
    }
}


