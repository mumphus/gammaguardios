//
//  GraphViewController.swift
//  GammaGuard
//
//  Created by EIC on 2018-05-14.
//  Copyright © 2018 EIC. All rights reserved.
//

import UIKit
import Charts


class GraphViewController: BaseViewController, ChartViewDelegate{
    //------------------------
    // IBOutlets and Variables
    //------------------------
    @IBOutlet weak var Graph: LineChartView! //This is the reference to the actual object on the view controller
    @IBOutlet weak var TimeLabel: UILabel!
    @IBOutlet weak var CountRateLabel: UILabel!
    @IBOutlet weak var LatitudeLabel: UILabel!
    @IBOutlet weak var LongitudeLabel: UILabel!
    @IBOutlet weak var MapButton: UIButton!
    @IBOutlet weak var ResponseSlider: UISlider!
    @IBOutlet weak var MinimumCountsSlider: UISlider!
    @IBOutlet weak var BackButton: UIButton!
    @IBOutlet weak var HistoryButton: UIButton!
    @IBOutlet weak var RefreshButton: UIButton!
    @IBOutlet weak var ResponseLabel: UIButton!
    @IBOutlet weak var MinimumCountsLabel: UIButton!
    @IBOutlet weak var RateLabel: UILabel!
    @IBOutlet weak var UnitsLabel: UILabel!
    @IBOutlet weak var DateLabel: UILabel!
    
    
    let dateFormatter = DateFormatter() //Used to convert seconds to dates
    var highlightedLatitude: Double = 0
    var highlightedLongitude: Double = 0
    var responseTimeInt: Int = 0
    var minimumCounts: Int = 100
    var stopColorChange: Bool = false //Used to stop nav bar from changing if the view is about to change
    var recentChartEntry = [ChartDataEntry]() //The datastructures from the library that stores the graph data
    var minChartEntry = [ChartDataEntry]()
    var maxChartEntry = [ChartDataEntry]()
    var intervalChartEntry = [ChartDataEntry]()
    var recentChartNeedsRefresh = false //represents whether or not the specific graphs need to be refreshed
    var historyChartNeedsRefresh = true //Changing response time, minimum counts, mode, etc. will require a refresh
    var intervalChartNeedsRefresh = false
    var graphType = "recent" //stores what graph type is selected
    var recentInterval: Int = 20 * 60 // Stored in seconds (Min*(60sec/min))
    var selectedHistoryInterval: Int = 20 * 60 // Stored in seconds (Min*(60sec/min))
    var highlightCounter: Int = 0 //Used to check whether the highlighted point is off screen
    var minMaxIndexArray = [[Int]]() // will store indexes of the minimum and maximum points on the history graph, so we know what point it is on the points set when it is selected
    var intervalSize: Int = 0
    let valuesOnHist: Int = 50 //How many max and min values will be shown on the history graph(Seems to only be an approximate)
    var indexOnGraphCountArray: Int = 0 //variable that will store the index of a selected point on the points set
    let gradientColors1 = [ChartColorTemplates.colorFromString("#00ff0000").cgColor,ChartColorTemplates.colorFromString("#ffff0000").cgColor] //gradients to fill
    let gradientColors2 = [ChartColorTemplates.colorFromString("#00ff0000").cgColor,ChartColorTemplates.colorFromString("#ff0000ff").cgColor]
    weak var graphTimer: Timer? // Timer that fires every second
    var sv = UIView() // loading indicator
    var removeLoadingIndicator = false //Once done loading this will be set to true and will remove loading indicator once the graph has been drawn
    var currentlyLoading = false // used to stop loading to the same array multiple times due to background threads
    var recentSyncAfterLoading = false //used to sync back up once loading has finished(loading will for example load indices 1840-3040, but while loading there were maybe 10 new points)
    var step1Loading = false //the first step of loading the recent graph
    var points = NSMutableOrderedSet() //the ordered set of points from the data set
    //-------------------------------
    // End of IBOutlets and Variables
    //-------------------------------
    
    
    //--------------------------
    // View Controller Functions
    //--------------------------
    override func viewDidLoad() {
        super.viewDidLoad()
        if !keyFobConnected{
            addModeButton()
        }
        addSlideMenuButton()
        loadGraphConstants()
        loadSavedSettings()
        Graph.delegate = self
        self.navigationItem.titleView = recentGraphLabel()
        MapButton.backgroundColor = UIColor.gray
        MapButton.titleLabel?.lineBreakMode = .byWordWrapping
        MapButton.titleLabel?.numberOfLines = 2
        MapButton.titleLabel?.textAlignment = .center
        dateFormatter.dateFormat = "HH:mm:ss"
        MinimumCountsSlider.setValue(Float(minimumCounts), animated: false)
    }

    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(true)
        if graphTimer != nil {
            graphTimer?.invalidate()
            graphTimer = nil
        }
        if view.subviews.last!.tag == 23{
            sideMenuOpen = false
            view.subviews.last!.removeFromSuperview()
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        if CT007Mode == "Contamination"{
            RateLabel.text = "Count Rate"
            UnitsLabel.text = CT007_ContamUnits
        }else{
            RateLabel.text = "Dose Rate"
            UnitsLabel.text = CT007_GammaUnits
        }
        stopColorChange = false
        colorChange()
        recentChartNeedsRefresh = true
        if graphType == "recent"{
            Graph.isHidden = true // Recent graph needs to refresh when coming back to the graph screen
        }
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(true)
        //Create and fire timer
        if graphTimer == nil{
            graphTimer = Timer.scheduledTimer(timeInterval: 1, target: self,  selector: #selector(updateGraph), userInfo: nil, repeats: true)
        }
        graphTimer?.fire()
    }
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(true)
        stopColorChange = true
        Graph.fitScreen() //zoom out graph
        Graph.highlightValue(x: 0, dataSetIndex: -1) //Deselect any selected points
    }
    //---------------------------------
    // End of View Controller Functions
    //---------------------------------
    
    func addModeButton(){
        let btnMode = UIButton(type: UIButton.ButtonType.system)
        btnMode.setImage(self.defaultModeImage(), for: UIControl.State())
        btnMode.frame = CGRect(x: 0, y: 0, width: 60, height: 30)
        btnMode.addTarget(self, action: #selector(ModeButtonPressed), for: UIControl.Event.touchUpInside)
        let customBarItem = UIBarButtonItem(customView: btnMode)
        self.navigationItem.rightBarButtonItem = customBarItem;
    }
    @objc func ModeButtonPressed(){
        if CT007Mode == "Gamma"{
            print("loading contamination settings")
            CT007Mode = "Contamination"
            RateLabel.text = "Count Rate"
            UnitsLabel.text = CT007_ContamUnits
            defaults.set(CT007Mode, forKey: "Saved_Mode")
        }else if CT007Mode == "Contamination"{
            print("loading gamma settings")
            CT007Mode = "Gamma"
            RateLabel.text = "Dose Rate"
            UnitsLabel.text = CT007_GammaUnits
            defaults.set(CT007Mode, forKey: "Saved_Mode")
        }else{
            print("Invalid Mode")
        }
        Graph.fitScreen()
        recentChartNeedsRefresh = true
        historyChartNeedsRefresh = true
        intervalChartNeedsRefresh = true
        Graph.highlightValue(x: 0, dataSetIndex: -1)
        updateGraph()
    }
    
    
    func chartValueSelected(_ chartView: ChartViewBase, entry: ChartDataEntry, highlight: Highlight) {
        indexOnGraphCountArray = Int(Graph.highlighted[0].x)
        highlightCounter = indexOnGraphCountArray + 2
        if graphType == "history"{
            HistoryButton.isEnabled = true
            indexOnGraphCountArray /= (intervalSize * graphPointDuration)
            indexOnGraphCountArray = minMaxIndexArray[indexOnGraphCountArray][Graph.highlighted[0].dataSetIndex]
        }else{
            indexOnGraphCountArray /= graphPointDuration
        }
        if indexOnGraphCountArray >= points.count{
            indexOnGraphCountArray = points.count - 1
        }
        if graphType == "historyinterval" || graphType == "recent"{ // Sometimes points x value arent the same as their index due to slowdown, so if it isnt, step back until it is found
            var pointTime = (points[indexOnGraphCountArray] as! GraphDataPoint).time
            let highlightedTime = Int(Graph.highlighted[0].x)
            while pointTime != highlightedTime{
                print("ADJUSTING | point of index:",pointTime,"Highlighted time:",highlightedTime)
                if pointTime > highlightedTime{indexOnGraphCountArray -= 1}
                else{indexOnGraphCountArray += 1}
                pointTime = (points[indexOnGraphCountArray] as! GraphDataPoint).time
            }
//            print("point of index:",pointTime)
//            print("Highlighted time:",highlightedTime)
        }
        print("Index:", indexOnGraphCountArray,"Points.count:", points.count)
        let point = points[indexOnGraphCountArray] as! GraphDataPoint
        let selectedTime = Int(point.time) + liveGraphBaseTime
        TimeLabel.text = secondsToHoursMinutesSeconds(seconds: Double(selectedTime))
        highlightedLatitude = (point.latitude)
        highlightedLongitude = (point.longitude)
        CountRateLabel.text = String(format: "%.2f", Graph.highlighted[0].y) + " " + CT007_ContamUnits
        if CT007Mode == "Gamma"{
            CountRateLabel.text = String(format: "%.2f", Graph.highlighted[0].y) + " " + CT007_GammaUnits
        }
        LatitudeLabel.text = "Lat: " + String(format: "%.4f", highlightedLatitude)
        LongitudeLabel.text = "Long: " + String(format: "%.4f", highlightedLongitude)
        MapButton.backgroundColor = UIColor.orange
        MapButton.isEnabled = true
    }
    
    
    func chartValueNothingSelected(_ chartView: ChartViewBase){ //Called when something is deselected | Used to clear out everything that fills when something is selected
        if graphType == "history"{
            HistoryButton.isEnabled = false
        }
        TimeLabel.text = ""
        highlightedLatitude = 0
        highlightedLongitude = 0
        CountRateLabel.text = ""
        LatitudeLabel.text = "Lat: "
        LongitudeLabel.text = "Long: "
        MapButton.backgroundColor = UIColor.gray
        MapButton.isEnabled = false
    }
    
    func highlightOffScreenCheck(){ //Checks if highlighted point is off screen and if so, deselects it
        if Graph.highlighted.count > 0 && graphType == "recent"{
            if ((points.count - Int(Graph.highlighted[0].x)) > recentChartEntry.count){
                Graph.highlightValue(x: 0, dataSetIndex: -1)
            }
        }
    }
    
    // Nav Bar warning color
    func colorChange(){
        if !stopColorChange{
            if CT007Mode == "Gamma"{
                switch Display_DoseRate_Short_Units_Label {
                case "µSv/h":
                    unitFactor = 1
                case "mSv/h":
                    unitFactor = 1/1000
                case "mRem/h":
                    unitFactor = 1/10
                default:
                    print("invalid units")
                }
            }else{
                unitFactor = 1
            }
            let navBar = navigationController?.navigationBar
            let tabBar = tabBarController?.tabBar
            if (Double(Display_DoseRate_Short_Label)! < CT007CurrentYellowLevel*unitFactor){
                navBar?.barTintColor = UIColor.warningGreenColor()
                tabBar?.barTintColor = UIColor.warningGreenColor()
            }
            else if (Double(Display_DoseRate_Short_Label)! >= CT007CurrentYellowLevel*unitFactor) && (Double(Display_DoseRate_Short_Label)! < CT007CurrentRedLevel*unitFactor) {
                navBar?.barTintColor = UIColor.warningYellowColor()
                tabBar?.barTintColor = UIColor.warningYellowColor()
            }else {
                if navBar?.barTintColor != UIColor.warningRedColor() && CT007DeviceAlarm{
                    playAlarm = true
                }
                navBar?.barTintColor = UIColor.warningRedColor()
                tabBar?.barTintColor = UIColor.warningRedColor()
            }
        }
    }

    // Used to get CPM value for graph of certain index of the Count array
    // startIndex should be index on count array and not graph
    func getDoseRate(startIndex: Int) -> Double{
        var doserate: Double = 0.0
        var avgTotal: Double = 0
        if responseTimeInt == 0{
            var values: Int = 0
            while Int(avgTotal) <= minimumCounts && values < 30{
                let index = startIndex - values
                if index >= 0{
                    let point = points[index] as! GraphDataPoint
                    avgTotal += Double(point.count)
                    values += 1
                }else{
                    break
                }
            }
            doserate = Double(avgTotal) / Double(values)
            if (doserate < 0) {doserate = 0}
        }
        else{
            var values: Int = 0
            for i in 0..<responseTimeInt {
                values = i + 1
                let counter = startIndex - i - 1
                if counter >= 0{
                    let point = points[counter] as! GraphDataPoint
                    avgTotal += Double(point.count)
                }else{
                    break
                }
            }
            doserate = Double(avgTotal) / Double(values)
            if (doserate < 0) {doserate = 0}
        }
        if CT007Mode == "Contamination"{
            if CT007_ContamUnits == "CPM"{
                return (doserate * 60)/Double(graphPointDuration)
            }
            return doserate/Double(graphPointDuration)
        }
        else{
            switch CT007_GammaUnits {
            case "µSv/h":
                break
            case "mSv/h":
                doserate /= 1000
            case "mRem/h":
                doserate /= 10
            default:
                print("invalid units")
            }
            return (doserate * currentConversionFactor)/Double(graphPointDuration)
        }
    }
    
    
    func secondsToHoursMinutesSeconds (seconds : Double) -> String{
        return dateFormatter.string(from: Date(timeIntervalSince1970: TimeInterval(seconds)))
    }
    
    func loadGraphConstants(){
        Graph.leftAxis.enabled = false
        Graph.xAxis.valueFormatter = TimeFormatter()
        Graph.xAxis.labelCount = 3
        Graph.xAxis.avoidFirstLastClippingEnabled = true
        let marker = BalloonMarker(color: UIColor.orange, font: .systemFont(ofSize: 12), textColor: .black, insets: UIEdgeInsets(top: 8, left: 8, bottom: 20, right: 8))
        marker.chartView = Graph
        marker.minimumSize = CGSize(width: 80, height: 40)
        Graph.marker = marker
        Graph.maxVisibleCount = 100
        Graph.rightAxis.axisMinimum = 0
    }
    
    
    
    func loadSavedSettings(){
        if defaults.object(forKey: "Saved_GraphMinimumCounts") != nil {
            minimumCounts = defaults.integer(forKey: "Saved_GraphMinimumCounts")
            MinimumCountsSlider.value = Float(minimumCounts)
            MinimumCountsLabel.setTitle("Minimum Counts: \(String(minimumCounts))", for: .normal)
            print ("Saved_GraphMinimumCounts not nil")
        }
        if defaults.object(forKey: "Saved_GraphResponseTime") != nil {
            responseTimeInt = defaults.integer(forKey: "Saved_GraphResponseTime")
            ResponseSlider.value = Float(responseTimeInt)
            print ("Saved_GraphResponseTime not nil")
            responseTimeInt = Int(ResponseSlider.value)
            ResponseLabel.setTitle("Response Time: \(String(responseTimeInt))", for: .normal)
            MinimumCountsLabel.setTitle("Disabled when not in Auto Mode", for: .normal)
            MinimumCountsSlider.isEnabled = false
            MinimumCountsLabel.isEnabled = false
            if responseTimeInt == 0{
                MinimumCountsLabel.setTitle("Minimum Counts: \(String(minimumCounts))", for: .normal)
                ResponseLabel.setTitle("Response Time: Auto Mode", for: .normal)
                MinimumCountsSlider.isEnabled = true
                MinimumCountsLabel.isEnabled = true
            }
        }
    }
    
    func intervalGraphCreation() -> LineChartDataSet{
        if intervalChartNeedsRefresh{
            intervalChartNeedsRefresh = false
            if currentlyLoading == false{
                sv = UIViewController.displaySpinner(onView: self.view)
            }
            currentlyLoading = true
            Graph.isHidden = true
            DispatchQueue.global(qos: .utility).async(flags: .barrier) {
                self.intervalChartEntry.removeAll()
                var startIndex = self.indexOnGraphCountArray - (self.selectedHistoryInterval/2)
                if startIndex < 0{
                    startIndex = 0
                }
                if self.selectedHistoryInterval > self.points.count{
                    for i in 0..<self.points.count{
                        let point = self.points[i] as! GraphDataPoint
                        let value = ChartDataEntry(x: Double(point.time), y: self.getDoseRate(startIndex: i))
                        self.intervalChartEntry.append(value)
                    }
                }
                else if startIndex + self.selectedHistoryInterval > self.points.count{
                    startIndex = (self.points.count-1) - self.selectedHistoryInterval
                    for i in 0..<self.selectedHistoryInterval{
                        let point = self.points[startIndex+i] as! GraphDataPoint
                        let value = ChartDataEntry(x: Double(point.time), y: self.getDoseRate(startIndex: startIndex+i))
                        self.intervalChartEntry.append(value)
                    }
                }else{
                    for i in 0..<self.selectedHistoryInterval{
                        let point = self.points[startIndex+i] as! GraphDataPoint
                        let value = ChartDataEntry(x: Double(point.time), y: self.getDoseRate(startIndex: startIndex+i))
                        self.intervalChartEntry.append(value)
                    }
                }
                self.removeLoadingIndicator = true
            }
        }
        return LineChartDataSet(values: intervalChartEntry, label: "Count Rate")
    }
    
    //Graph for Min and Max
    func MinMaxGraphCreation() -> LineChartDataSet{
        if points.count > valuesOnHist{
            intervalSize = points.count / valuesOnHist
            if historyChartNeedsRefresh == true{
                historyChartNeedsRefresh = false
                if currentlyLoading == false{
                    sv = UIViewController.displaySpinner(onView: self.view)
                }
                currentlyLoading = true
                Graph.isHidden = true
                DispatchQueue.global(qos: .utility).async(flags: .barrier) {
                    var minMaxValuesArray = [[Double]]()
                    self.minMaxIndexArray.removeAll()
                    for i in 0..<self.points.count/self.intervalSize{
                        var tempValueArray = [Double]()
                        var tempIndexArray = [Int]()
                        var maxVal: Double = -1
                        var minVal: Double = Double.greatestFiniteMagnitude
                        var maxIndex: Int = 0
                        var minIndex: Int = 0
                        for x in 0..<self.intervalSize{
                            let index = i*self.intervalSize + x
                            let value  = self.getDoseRate(startIndex: index)
                            if value > maxVal {
                                maxVal = value
                                maxIndex = index
                            }
                            if value < minVal {
                                minVal = value
                                minIndex = index
                            }
                        }
                        tempValueArray.append(minVal)
                        tempValueArray.append(maxVal)
                        tempIndexArray.append(maxIndex)
                        tempIndexArray.append(minIndex)
                        minMaxValuesArray.append(tempValueArray)
                        self.minMaxIndexArray.append(tempIndexArray)
                    }
                    //min line creating
                    self.minChartEntry.removeAll()
                    for i in 0..<minMaxValuesArray.count{
                        let point = self.points[0] as! GraphDataPoint
                        let value = ChartDataEntry(x: Double(graphPointDuration*i*self.intervalSize+Int(point.time)), y: minMaxValuesArray[i][0])//getDoseRate(startIndex: secondArray[i][0])
                        self.minChartEntry.append(value)
                    }
                    //max line creating
                    self.maxChartEntry.removeAll()
                    for i in 0..<minMaxValuesArray.count{
                        let point = self.points[0] as! GraphDataPoint
                        let value = ChartDataEntry(x: Double(graphPointDuration*i*self.intervalSize+Int(point.time)), y: minMaxValuesArray[i][1])//getDoseRate(startIndex: secondArray[i][1])
                        self.maxChartEntry.append(value)
                    }
                    self.removeLoadingIndicator = true
                }
            }
            return LineChartDataSet(values: minChartEntry, label: "Minimum Values")
        }else{
            minChartEntry = [ChartDataEntry]()
            maxChartEntry = [ChartDataEntry]()
            return LineChartDataSet(values: minChartEntry, label: "Minimum Values")
        }
    }
    
    func recentChart() -> LineChartDataSet{
        if recentChartNeedsRefresh{
            recentChartNeedsRefresh = false
            refreshRecentChart()
        }
        print("points.count:",points.count)
        print("RCE.count:",recentChartEntry.count)
        if points.count > 0 && points.count > recentChartEntry.count && step1Loading == false{
            if recentSyncAfterLoading && recentChartEntry.count > 0{
                recentSyncAfterLoading = false
                let difference = indexDifference()
                for i in 0 ..< difference{
                    while recentChartEntry.count >= recentInterval{recentChartEntry.remove(at: 0)}
                    let index = points.count-(difference-i)
                    print("Index:",index,"difference:",difference)
                    let point = points[index] as! GraphDataPoint
                    let value = ChartDataEntry(x: Double(point.time), y: getDoseRate(startIndex: points.count-1))
                    recentChartEntry.append(value)
                }
            }else{
                while recentChartEntry.count >= recentInterval{recentChartEntry.remove(at: 0)}
                let index = points.count-1
                let point = points[index] as! GraphDataPoint
                let value = ChartDataEntry(x: Double(point.time), y: getDoseRate(startIndex: points.count-1))
                recentChartEntry.append(value)
            }
//            if currentlyLoading{
//                removeLoadingIndicator = true
//            }
        }
        if (points.count == recentChartEntry.count || recentChartEntry.count >= 1200) && currentlyLoading{
            removeLoadingIndicator = true
        }
        return LineChartDataSet(values: recentChartEntry, label: "Count Rate")
    }
    func refreshRecentChart(){
        recentChartEntry.removeAll()
        if recentChartEntry.count != 0{
                print("ERROR")
        }
        if currentlyLoading == false{
            sv = UIViewController.displaySpinner(onView: self.view)
        }
        Graph.isHidden = true
        currentlyLoading = true
        step1Loading = true
        DispatchQueue.global(qos: .utility).async(flags: .barrier) {
            if self.points.count <= self.recentInterval{
                for i in 0..<self.points.count{
                    let point = self.points[i] as! GraphDataPoint
                    let value = ChartDataEntry(x: Double(point.time), y: self.getDoseRate(startIndex: i))
                    self.recentChartEntry.append(value)
                }
            }else{
                for i in 0..<self.recentInterval{
                    let index = self.points.count - self.recentInterval + i
                    let point = self.points[index] as! GraphDataPoint
                    let value = ChartDataEntry(x: Double(point.time), y: self.getDoseRate(startIndex: index))
                    self.recentChartEntry.append(value)
                }
            }
            self.recentSyncAfterLoading = true
            self.step1Loading = false
        }
    }
    
    // Called every second
    @objc func updateGraph(){
        if CTData.currentSet != nil{
            points = (CTData.currentSet?.mutableOrderedSetValue(forKey: "dataPoints"))!
        }else{
            print("CURRENT SET WAS NIL")
            return
        }
        if points.count > valuesOnHist*2 && graphType == "recent"{
            HistoryButton.isEnabled = true
        }
        highlightOffScreenCheck()
        let gradient1 = CGGradient(colorsSpace: nil, colors: gradientColors1 as CFArray, locations: nil)!
        let gradient2 = CGGradient(colorsSpace: nil, colors: gradientColors2 as CFArray, locations: nil)!
        colorChange()
        var line1 = LineChartDataSet()
        var line2 = LineChartDataSet()
        if graphType == "recent"{
            line1 = recentChart()
        }
        Graph.xAxis.granularity = 1
        if graphType == "history"{
            line1 = MinMaxGraphCreation()
            line2 = LineChartDataSet(values: maxChartEntry, label: "Max Values")
            line2.drawCirclesEnabled = false
            line2.drawValuesEnabled = false
            line2.lineWidth = 1
            line2.colors = [NSUIColor.red]
            line2.highlightLineWidth = 1
            line2.highlightColor = UIColor.black
            line2.fillAlpha = 1
            line2.fill = Fill(linearGradient: gradient1, angle: 90)
            line2.drawFilledEnabled = true
            line2.axisDependency = .right
            Graph.xAxis.granularity = Double(intervalSize)
        }
        if graphType == "historyinterval"{
            line1 = intervalGraphCreation()
        }
        line1.drawCirclesEnabled = false
        line1.drawValuesEnabled = false
        line1.lineWidth = 1
        line1.colors = [NSUIColor.black]
        line1.highlightLineWidth = 1
        line1.highlightColor = UIColor.black
        line1.fillAlpha = 1
        line1.fill = Fill(linearGradient: gradient1, angle: 90)
        line1.drawFilledEnabled = true
        line1.axisDependency = .right
        let data = LineChartData()
        Graph.legend.enabled = false
        if graphType == "history"{
            line1.fill = Fill(linearGradient: gradient2, angle: 90)
            line1.colors = [NSUIColor.blue]
            line1.fillAlpha = 1
            data.addDataSet(line2)
            Graph.legend.enabled = true
        }
        data.addDataSet(line1)
        Graph.data = data
        Graph.chartDescription?.enabled = false
        if removeLoadingIndicator{
            UIViewController.removeSpinner(spinner: self.sv)
            removeLoadingIndicator = false
            Graph.isHidden = false
            currentlyLoading = false
        }
        let dayTimePeriodFormatter = DateFormatter()
        dayTimePeriodFormatter.dateFormat = "MMM d, yyyy"
        DateLabel.text = dayTimePeriodFormatter.string(from: Date(timeIntervalSince1970: TimeInterval(liveGraphBaseTime)))
    }
    
    
    
    @IBAction func OpenMapPressed(_ sender: Any) {
        if highlightedLongitude == 0{return}
        let url = "http://maps.apple.com/?q=\(highlightedLatitude),\(highlightedLongitude)"
        UIApplication.shared.openURL(URL(string:url)!)
    }
    
    @IBAction func BackButtonPressed(_ sender: Any) {
        Graph.fitScreen()
        Graph.highlightValue(x: 0, dataSetIndex: -1)
        if graphType == "history"{
            graphType = "recent"
            recentChartNeedsRefresh = true
            BackButton.isHidden = true
            HistoryButton.isEnabled = true
            RefreshButton.isHidden = true
            HistoryButton.setTitle("View History", for: .normal)
            self.navigationItem.titleView = recentGraphLabel()
            // Do other stuff
        }else if graphType == "historyinterval"{
            graphType = "history"
            RefreshButton.isHidden = false
            HistoryButton.isHidden = false
            self.navigationItem.titleView = historyGraphLabel()
            // Do other stuff
            
        }
        updateGraph()
    }
    @IBAction func HistoryButtonPressed(_ sender: Any) {
        Graph.fitScreen()
        Graph.highlightValue(x: 0, dataSetIndex: -1)
        if graphType == "recent"{
            graphType = "history"
            BackButton.isHidden = false
            HistoryButton.setTitle("View Interval", for: .normal)
            HistoryButton.isEnabled = false
            RefreshButton.isHidden = false
            self.navigationItem.titleView = historyGraphLabel()
            // Do other stuff show legend for min max
        }else if graphType == "history"{
            graphType = "historyinterval"
            HistoryButton.isHidden = true
            RefreshButton.isHidden = true
            intervalChartNeedsRefresh = true
            self.navigationItem.titleView = intervalGraphLabel()
            // Do other stuff call function to create interval graph
            
        }
        updateGraph()
    }
    
    @IBAction func ResponseSliderChanged(_ sender: Any) {
        Graph.fitScreen()
        recentChartNeedsRefresh = true
        historyChartNeedsRefresh = true
        intervalChartNeedsRefresh = true
        Graph.highlightValue(x: 0, dataSetIndex: -1)
        responseTimeInt = Int(ResponseSlider.value)
        MinimumCountsSlider.isEnabled = false
        MinimumCountsLabel.isEnabled = false
        ResponseLabel.setTitle("Response Time: \(String(self.responseTimeInt))", for: .normal)
        MinimumCountsLabel.setTitle("Disabled when not in Auto Mode", for: .normal)
        if responseTimeInt == 0{
            MinimumCountsLabel.setTitle("Minimum Counts: \(String(minimumCounts))", for: .normal)
            ResponseLabel.setTitle("Response Time: Auto Mode", for: .normal)
            MinimumCountsSlider.isEnabled = true
            MinimumCountsLabel.isEnabled = true
        }
        defaults.set(responseTimeInt, forKey: "Saved_GraphResponseTime")
        updateGraph()
    }
    
    @IBAction func MinimumCountsSliderChanged(_ sender: Any) {
        Graph.fitScreen()
        Graph.highlightValue(x: 0, dataSetIndex: -1)
        recentChartNeedsRefresh = true
        historyChartNeedsRefresh = true
        intervalChartNeedsRefresh = true
        minimumCounts = Int(MinimumCountsSlider.value)
        MinimumCountsLabel.setTitle("Minimum Counts: \(String(minimumCounts))", for: .normal)
        defaults.set(minimumCounts, forKey: "Saved_GraphMinimumCounts")
        updateGraph()
    }
    @IBAction func RefreshButtonTapped(_ sender: Any) {
        Graph.fitScreen()
        historyChartNeedsRefresh = true
        updateGraph()
    }
    @IBAction func ResponseManualChange(_ sender: Any) {
        let alert = UIAlertController(title: "Response Time",message: "Enter a Response Time between 0-30",preferredStyle: .alert)
        let saveAction = UIAlertAction(title: "Save",style: .default) {
            [unowned self] action in
            guard let textField = alert.textFields?.first,
                let responseTimeText = textField.text else {return}
            if let IntCheck = Int(responseTimeText){
                if IntCheck < 0 || IntCheck > 30{
                    return
                }
            }else{return}
            self.responseTimeInt = Int(responseTimeText)!
            self.ResponseLabel.setTitle("Response Time: \(String(self.responseTimeInt))", for: .normal)
            defaults.set(self.responseTimeInt, forKey: "Saved_GraphResponseTime")
            self.ResponseSlider.value = Float(self.responseTimeInt)
            self.Graph.fitScreen()
            self.recentChartNeedsRefresh = true
            self.historyChartNeedsRefresh = true
            self.intervalChartNeedsRefresh = true
            self.MinimumCountsSlider.isEnabled = false
            self.MinimumCountsLabel.isEnabled = false
            self.MinimumCountsLabel.setTitle("Disabled when not in Auto Mode", for: .normal)
            if self.responseTimeInt == 0{
                self.MinimumCountsLabel.setTitle("Minimum Counts: \(String(self.minimumCounts))", for: .normal)
                self.ResponseLabel.setTitle("Response Time: Auto Mode", for: .normal)
                self.MinimumCountsSlider.isEnabled = true
                self.MinimumCountsLabel.isEnabled = true
            }
            self.Graph.highlightValue(x: 0, dataSetIndex: -1)
            self.updateGraph()
        }
        let cancelAction = UIAlertAction(title: "Cancel",style: .default)
        alert.addTextField()
        alert.addAction(saveAction)
        alert.addAction(cancelAction)
        present(alert, animated: true)
    }
    @IBAction func MinCountsManualChange(_ sender: Any) {
        let alert = UIAlertController(title: "Minimum Counts",message: "Enter the minimum counts value between 1-1000",preferredStyle: .alert)
        let saveAction = UIAlertAction(title: "Save",style: .default) {
            [unowned self] action in
            guard let textField = alert.textFields?.first,
                let minCountsText = textField.text else {return}
            if let IntCheck = Int(minCountsText){
                if IntCheck < 1 || IntCheck > 1000{
                    return
                }
            }else{return}
            self.minimumCounts = Int(minCountsText)!
            self.MinimumCountsLabel.setTitle("Minimum Counts: \(String(self.minimumCounts))", for: .normal)
            defaults.set(self.minimumCounts, forKey: "Saved_GraphMinimumCounts")
            self.MinimumCountsSlider.value = Float(self.minimumCounts)
            self.Graph.fitScreen()
            self.recentChartNeedsRefresh = true
            self.historyChartNeedsRefresh = true
            self.intervalChartNeedsRefresh = true
            self.Graph.highlightValue(x: 0, dataSetIndex: -1)
            self.updateGraph()
        }
        let cancelAction = UIAlertAction(title: "Cancel",style: .default)
        alert.addTextField()
        alert.addAction(saveAction)
        alert.addAction(cancelAction)
        present(alert, animated: true)
    }
    
    func indexDifference() -> Int{
        var difference = 0
        var searchPoint = points.lastObject as! GraphDataPoint
        while searchPoint.time != Int((recentChartEntry.last?.x)!){
            difference += 1
            searchPoint = points[points.count-1-difference] as! GraphDataPoint
        }
        return difference
    }
    
    func recentGraphLabel() -> UILabel{
        let label = UILabel(frame: CGRect(x:0, y:0, width:400, height:50))
        label.backgroundColor = .clear
        label.numberOfLines = 2
        label.font = UIFont.boldSystemFont(ofSize: 16.0)
        label.textAlignment = .center
        label.textColor = .black
        label.text = "Latest \(20*graphPointDuration) Minutes\nDetector: \(Detector_NAME!)"
        return label
    }
    func historyGraphLabel() -> UILabel{
        let label = UILabel(frame: CGRect(x:0, y:0, width:400, height:50))
        label.backgroundColor = .clear
        label.numberOfLines = 2
        label.font = UIFont.boldSystemFont(ofSize: 16.0)
        label.textAlignment = .center
        label.textColor = .black
        label.text = "Overall History\nDetector: \(Detector_NAME!)"
        return label
    }
    func intervalGraphLabel() -> UILabel{
        let label = UILabel(frame: CGRect(x:0, y:0, width:400, height:50))
        label.backgroundColor = .clear
        label.numberOfLines = 2
        label.font = UIFont.boldSystemFont(ofSize: 16.0)
        label.textAlignment = .center
        label.textColor = .black
        label.text = "Selected Interval\nDetector: \(Detector_NAME!)"
        return label
    }
}
