//
//  PeripheralConnectedViewController+Alerts.swift
//  GammaGuard
//
//  Created by EIC on 2018-10-26.
//  Copyright © 2018 EIC. All rights reserved.
//

import Foundation


//
//  DeviceConnectedVC+Alerts.swift
//  AlertTest
//
//  Created by EIC on 2018-10-17.
//  Copyright © 2018 Darby. All rights reserved.
//

import Foundation
import UIKit
import UserNotifications

extension PeripheralConnectedViewController {
    
    func showDisconnectAlert(){
        print("222222")
        timeToReconnect = reconnectionTime
        removeConnectingAlert()
        if disconnectAlertController == nil{
//            disconnectAlertController = UIAlertController(title: "Alert!", message: "The detector was disconnected. GammaGuard will attempt to reconnect every \(reconnectionTime) seconds" + "\nTime to reconnect: \(reconnectionTime)", preferredStyle: .alert)
            disconnectAlertController = UIAlertController(title: "Alert!", message: "The detector was disconnected. GammaGuard will search for the device and attempt to reconnect.", preferredStyle: .alert)
            disconnectAlertController?.addAction(UIAlertAction(title: NSLocalizedString("Cancel", comment: "Default action"), style: .`default`, handler: { _ in
                NSLog("The \"OK\" alert occured.")
                //                self.reconTimer?.invalidate()
                //                self.serviceTimer?.invalidate()
            }))
        }else{
            removeDisconnectAlert()
        }
        if var topController = UIApplication.shared.keyWindow?.rootViewController {
            while let presentedViewController = topController.presentedViewController {
                topController = presentedViewController
            }
            //finding correct view
        }
        if var topController = UIApplication.shared.keyWindow?.rootViewController {
            while let presentedViewController = topController.presentedViewController {
                if !(presentedViewController is UIAlertController){
                    topController = presentedViewController
                }else{
                    break
                }
            }
            topController.present(disconnectAlertController!, animated: true)
        }
    }
    
    func removeDisconnectAlert(){
        if disconnectAlertController != nil{
            disconnectAlertController!.dismiss(animated: true, completion: nil)
            disconnectAlertController = nil
        }
    }
    
    func showConnectingAlert(){
        print("11111")
        removeDisconnectAlert()
        if connectingAlertController == nil{
            connectingAlertController = UIAlertController(title: "Connected", message: "The connection was found, reconnecting to device...\nDiscovering Services", preferredStyle: .alert)
        }else{
//            connectingAlertController?.message = "The connection was found, reconnecting to device...\nDiscovering Services"
            removeConnectingAlert()
        }
        if var topController = UIApplication.shared.keyWindow?.rootViewController {
            while let presentedViewController = topController.presentedViewController {
                topController = presentedViewController
            }
            //finding correct view
        }
        if var topController = UIApplication.shared.keyWindow?.rootViewController {
            while let presentedViewController = topController.presentedViewController {
                if !(presentedViewController is UIAlertController){
                    topController = presentedViewController
                }else{
                    break
                }
            }
            topController.present(connectingAlertController!, animated: true)
        }
    }
    
    func removeConnectingAlert(){
        if connectingAlertController != nil{
            connectingAlertController!.dismiss(animated: true, completion: nil)
            connectingAlertController = nil
        }
    }
    
}
