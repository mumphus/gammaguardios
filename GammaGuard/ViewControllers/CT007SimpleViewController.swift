//
//  CT007SimpleViewController.swift
//  UITableVC
//
//  Created by Sepehr Khatir on 15/03/2017.
//  Copyright © 2017 EIC. All rights reserved.
//
//

import UIKit

//Defaults
var unitFactor: Double = 1
    
    class CT007SimpleViewController: BaseViewController{
        
        //------------------------
        // IBOutlets and Variables
        //------------------------
        @IBOutlet weak var rssiLabel: UILabel!
        @IBOutlet weak var Status: UILabel!
        @IBOutlet weak var BatteryLabel: UILabel!
        @IBOutlet weak var DisplayDoseRateShortLabel: UILabel!
        @IBOutlet weak var DoseRateShortUnitsLabel: UILabel!
        @IBOutlet weak var CT007LogButtonLabel: UIButton!
        @IBOutlet weak var counterView: CounterView!
        @IBOutlet weak var MaxLevelLabel: UILabel!
        @IBOutlet weak var UnitsLabel: UILabel!
        @IBOutlet weak var DeviceNameLabel: UILabel!
        @IBOutlet weak var DisconnectButton: UIButton!
        @IBOutlet weak var DateLabel: UILabel!
        
        var stopColorChange: Bool = false
        //-------------------------------
        // End of IBOutlets and Variables
        //-------------------------------
        
     
        //--------------------------
        // View Controller Functions
        //--------------------------
        override func viewDidLoad() {
            super.viewDidLoad()
            addSlideMenuButton()
            if !keyFobConnected{
                addModeButton()
            }
            DeviceNameLabel.text = Detector_NAME
            if IsCT007LoggingEnabled {
                CT007LogButtonLabel.isEnabled = true
            } else {
                CT007LogButtonLabel.isEnabled = false
                CT007LogButtonLabel.setTitleColor(UIColor.gray, for: .disabled)
            }
        }
        override func viewDidDisappear(_ animated: Bool) {
            super.viewDidDisappear(true)
            print (".......... CT Simple notification observer removed ...........")
            NotificationCenter.default.removeObserver(self)
            if view.subviews.last!.tag == 23{
                sideMenuOpen = false
                view.subviews.last!.removeFromSuperview()
            }
        }
        override func viewDidAppear(_ animated: Bool) {
            super.viewDidAppear(animated)
            NotificationCenter.default.addObserver(self,
                                                   selector: #selector(doSomethingAfterNotifiedCT007),
                                                   name: NSNotification.Name(rawValue: myCT007NotificationKey),
                                                   object: nil)
        }
        
        override func viewWillAppear(_ animated: Bool) {
            super.viewWillAppear(animated)
            stopColorChange = false
            doSomethingAfterNotifiedCT007()
            //Color loading
            if CT007Mode == "Gamma"{
                self.navigationItem.titleView = doseModeLabel()
//                self.navigationItem.title = "Dose Mode"
            }else if CT007Mode == "Contamination"{
                self.navigationItem.titleView = countModeLabel()
//                self.navigationItem.title = "Counts Mode"
            }else{
                self.navigationItem.titleView = invalidModeLabel()
            }
            colorChange()
        }
        
        override func viewWillDisappear(_ animated: Bool) {
            super.viewWillDisappear(true)
            stopColorChange = true
        }
        //---------------------------------
        // End of View Controller Functions
        //---------------------------------
        
        
        //-----------------------
        // Mode Related functions
        //-----------------------
        func loadGammaSettings(){
            self.navigationItem.titleView = doseModeLabel()
            CT007Mode = "Gamma"
            defaults.set(CT007Mode, forKey: "Saved_Mode")
        }
        func loadContaminationSettings(){
            self.navigationItem.titleView = countModeLabel()
            CT007Mode = "Contamination"
            defaults.set(CT007Mode, forKey: "Saved_Mode")
        }
        func addModeButton(){
            let btnMode = UIButton(type: UIButton.ButtonType.system)
            btnMode.setImage(self.defaultModeImage(), for: UIControl.State())
            btnMode.frame = CGRect(x: 0, y: 0, width: 60, height: 30)
            btnMode.addTarget(self, action: #selector(ModeButtonPressed), for: UIControl.Event.touchUpInside)
            let customBarItem = UIBarButtonItem(customView: btnMode)
            self.navigationItem.rightBarButtonItem = customBarItem;
        }
        @objc func ModeButtonPressed(){
            doSomethingAfterNotifiedCT007()
            if CT007Mode == "Gamma"{
                print("loading contamination settings")
                loadContaminationSettings()
            }else if CT007Mode == "Contamination"{
                print("loading gamma settings")
                loadGammaSettings()
            }else{
                print("Invalid Mode")
            }
        }
        //------------------------------
        // End of Mode Related functions
        //------------------------------
        
        //
        
        // Nav bar warning color
        func colorChange(){
            if !stopColorChange{
                let tabBar = tabBarController?.tabBar
                let navBar = navigationController?.navigationBar
                if (Double(Display_DoseRate_Short_Label)! < CT007CurrentYellowLevel*unitFactor){
//                    navBar?.barTintColor = UIColor.green
//                    tabBar?.barTintColor = UIColor.green
                    navBar?.barTintColor = UIColor.warningGreenColor()
                    tabBar?.barTintColor = UIColor.warningGreenColor()
                }
                else if (Double(Display_DoseRate_Short_Label)! >= CT007CurrentYellowLevel*unitFactor) && (Double(Display_DoseRate_Short_Label)! < CT007CurrentRedLevel*unitFactor) {
                    navBar?.barTintColor = UIColor.warningYellowColor()
                    tabBar?.barTintColor = UIColor.warningYellowColor()
                }else if (Double(Display_DoseRate_Short_Label)! >= CT007CurrentRedLevel*unitFactor) {
                    if navBar?.barTintColor != UIColor.warningRedColor() && CT007DeviceAlarm{
                        playAlarm = true
                    }
                    navBar?.barTintColor = UIColor.warningRedColor()
                    tabBar?.barTintColor = UIColor.warningRedColor()
                }
            }
        }
        
        func getUnitFactor(){
            if CT007Mode == "Gamma"{
                switch Display_DoseRate_Short_Units_Label {
                case "µSv/h":
                    unitFactor = 1
                case "mSv/h":
                    unitFactor = 1/1000
                case "mRem/h":
                    unitFactor = 1/10
                default:
                    print("invalid units")
                }
            }else{
                if Display_DoseRate_Short_Units_Label == "CPS"{unitFactor = 1/60}
                else{unitFactor = 1}
            }
        }
        
        
        // Called every second
        @objc func doSomethingAfterNotifiedCT007 (){
            //MARK: connection status .....
                Status.text = ConnectionState
            switch IntRSSI {
            case -90 ... -60:
                rssiLabel.textColor = UIColor.bluetoothOrangeColor()
                break
            case -200 ... -90:
                rssiLabel.textColor = UIColor.bluetoothRedColor()
                break
            default:
                rssiLabel.textColor = UIColor.bluetoothGreenColor()
            }
            getUnitFactor()
            counterView.refreshDisp()
            colorChange()
            rssiLabel.text = RSSIGLOBAL
            BatteryLabel.text = "Battery \(BatteryLevel_Characteristics_Value_Int) %"
            DisplayDoseRateShortLabel.text = Display_DoseRate_Short_Label
            DoseRateShortUnitsLabel.text = Display_DoseRate_Short_Units_Label
            MaxLevelLabel.text = String(format: "%.2f",CT007GaugeMax*unitFactor)
            UnitsLabel.text = Display_DoseRate_Short_Units_Label
            let date = Date()
            let dayTimePeriodFormatter = DateFormatter()
            dayTimePeriodFormatter.dateFormat = "MMM d, yyyy"
            DateLabel.text = dayTimePeriodFormatter.string(from: date)
             print ("noti simple : " , Display_DoseRate_Short_Label)
        }

        @IBAction func disconnectButtonPressed(_ sender: Any) {
                        PeripheralConnectedViewController().disconnectButtonPressed()
        }
        
        @IBAction func LogCT007DataPressed(_ sender: Any) {
            PeripheralConnectedViewController().LogExternalData()
        }
}



// Class for gauge
class CounterView: UIView {
    private struct Constants {
        static let arcWidth: CGFloat = 20
    }
    
    //Called every second to refresh gauge
    func refreshDisp(){
        if (Double(Display_DoseRate_Short_Label)! < CT007CurrentYellowLevel*unitFactor){
            CT007GaugeMax = CT007CurrentYellowLevel
//            self.liveDataColor = UIColor.green
            self.liveDataColor = UIColor.warningGreenColor()
        }
        else if (Double(Display_DoseRate_Short_Label)! >= CT007CurrentYellowLevel*unitFactor) && (Double(Display_DoseRate_Short_Label)! < CT007CurrentRedLevel*unitFactor) {
            CT007GaugeMax = CT007CurrentRedLevel
            self.liveDataColor = UIColor.warningYellowColor()
        }else {
            CT007GaugeMax = CT007CurrentMaxLevel
            self.liveDataColor = UIColor.warningRedColor()
        }
        //refreshes to display
        setNeedsDisplay()
    }
    
    // variables for drawing
    var liveData: Double = Double(Display_DoseRate_Short_Label)!
    var liveDataColor: UIColor = UIColor.gray
    var baseColor: UIColor = UIColor.gray
    let endAngle: CGFloat = 0
    
    
    override func draw(_ rect: CGRect) {
        // Data for drawing
        let center = CGPoint(x: bounds.width / 2, y: (bounds.height / 2) + 80)
        let radius: CGFloat = min(bounds.width, bounds.height) + 80
        let startAngle: CGFloat = .pi
        var endAngleLive: CGFloat = .pi*(CGFloat(Double(Display_DoseRate_Short_Label)!)/CGFloat(CT007GaugeMax*unitFactor)) - .pi
        // If live angle is out of range then set to "empty" gauge
        if endAngleLive < -.pi || endAngleLive > 0{
            endAngleLive = -.pi
        }
        // If doserate is greater than max just show "full gauge"
        if (Double(Display_DoseRate_Short_Label)! > CT007CurrentMaxLevel){
            endAngleLive = 0
        }
        
        // Draw the base gauge
        let path = UIBezierPath(arcCenter: center,
                                radius: radius/2 - Constants.arcWidth/2,
                                startAngle: startAngle,
                                endAngle: 0,
                                clockwise: true)
        path.lineWidth = Constants.arcWidth
        path.lineCapStyle = .round
        baseColor.setStroke()
        path.stroke()
        // Draw the live gauge
        let livePath = UIBezierPath(arcCenter: center,
                                radius: radius/2 - Constants.arcWidth/2,
                                startAngle: .pi,
                                endAngle: endAngleLive,
                                clockwise: true)
        livePath.lineWidth = Constants.arcWidth
        livePath.lineCapStyle = .round
        liveDataColor.setStroke()
        
        livePath.stroke()
    }
}

