//
//  PeripheralConnectedViewController.swift
//  BLEScanner
//
//  Created by Sepehr Khatir on 15/03/2017.
//  Copyright © 2017 EIC. All rights reserved.
//

import UIKit
import CoreBluetooth
import AVFoundation
import UserNotifications

var CTData = CT007Data()
var Connected: Bool = false
var Detector_NAME: String? = ""
let myCT007NotificationKey = "CT007NotificationKey"
let send_msg_to_console_NotificationKey = "Console_NotificationKey"
var ConnectionState = ""
var Radiation_Characteristics_Value_Int = 0
var BatteryLevel_Characteristics_Value_Int = 0
var RSSIGLOBAL = ""
var Display_DoseRate_Short_Label = ""
var Display_DoseRate_Long_Label = ""
var Display_DoseRate_Short_Units_Label = ""
var Display_TotalTime_Label = ""
var Display_TotalDose_Label = ""
var Display_TotalDose_Units_Label = ""
var Display_OverallDoseRate_Label = ""
var periphGlob: CBPeripheral?
var IntRSSI = 0


class PeripheralConnectedViewController: BaseViewController{
    
    var MyOutPutChar:CBCharacteristic?
    var CT_Device: Bool = false
    var OneSecTimer = 0
    var MillisecondsTimer = 0
    var inputCharacteristic: CBCharacteristic?
    var pending = ""
    var writepending = ""
    var DeviceRespond = ""
    var command_STOP_TIMER0 = "TIMER 0 STOP"
    var command_STOP_TIMER1 = "TIMER 1 STOP"
    var command_STOP_TIMER2 = "TIMER 2 STOP"
    var command_RUN_TIMER_0 = "TIMER 0, 200 REPEAT GOSUB 2100"
    var command_RUN_TIMER_1 = "TIMER 0 STOP"
    var command_RUN_TIMER_2 = "TIMER 2, 1000 REPEAT GOSUB 2110"
    var command_BEFORE_TIMERS = "T(34)=15"
    var command_LIST = "list 3630"
    var command_1 = "1"
    var command_PRINT_V = "print V"
    var command_PRINT_VERSION = "print T(31),\".\",T(32)"
    var command_PRINT_DEVICE_ID = "print T(0)"
    var command_PRINT_Beep = "print T(2)"
    var command_PRINT_Alarm = "print T(12)"
    var command_PRINT_Alarm_Level = "print F"
    var command_PRINT_RTime = "print T(37)"
    var command_PRINT_DTime = "PRINT W"
    var command_PRINT_SCREENS = "PRINT X(1),X(2),X(3),X(4)"
    var command_PRINT_UNITS = "PRINT T(36)"
    var command_test = "2800 DELAY 1"
    var command_CLEAR_COUNTER = "counter 0, clear"
    var command_RESPOND_TEST = "PRINT 'TEST'"
    var SentMsg = ""
    var GOT_CONVERSION = ""
    var GOT_DEVICE_ID = ""
    var test = ""
    var GOT_BASIC_VERSION = ""
    var GOT_DEVICE_NAME = ""
    var GOT_BEEP_STATE = ""
    var INT_GOT_BEEP_STATE = 0
    var GOT_ALARM_STATE = ""
    var INT_GOT_ALARM_STATE = 0
    var GOT_ALARM_LEVEL = ""
    var INT_GOT_ALARM_LEVEL = 0
    var GOT_RTime = ""
    var GOT_cFactor = ""
    var INT_GOT_cFactor: Double = 0
    var GOT_DTime = ""
    var GOT_Screens = ""
    var GOT_Units = ""
    var INT_GOT_RTime = 0
    var soundInterval: Double = 0
    weak var serviceTimer: Timer?
    weak var reconTimer: Timer?
    weak var connectionQueueTimer: Timer?
    var disconnectAlertController : UIAlertController?
    var connectingAlertController : UIAlertController?
    var timeToReconnect: Int = 30
    let reconnectionTime: Int = 30
    let reconnectAttemptTime: Int = 10
    var ids = [CBUUID]()
    var consistentConnection = false
    var connectionQueueAdded = false
    var workItem: DispatchWorkItem?
    
    var packetCount: Int = 0
    
    //var centralManager: CBCentralManager?
    var ConversionFactor_Characteristics_Value: UInt32 = 1
    var BatteryLevel_Characteristics_Value: UInt32 = 0
    var Radiation_Characteristics_Value: UInt32 = 0
    var ConversionFactor_Characteristics_Value_Int: Int = 1
    var INPUT_Characteristics_Value: UInt32 = 0
    var INPUT_Characteristics_Value_Int = 0
    var LogCT007Timer = 0
    
    //Sound variables
    var time: Int = 200
    var fractionalTones: Double = 0
    weak var toneTimer: Timer?
    var toneCount = 0
    var totalTones = 0
    
    
    //Device details dictionary in format [ID, Name, BasicFile, Units, Linear max dose rate, Max dose rate, response time, hotzone level, contamination capable]
    var detailsDict: [Int: [Any]] = [
        0:[0,"S","basicFile Placeholder", "Units Placeholder", 5000, 10000, 30, 500, false],
        1:[1,"P","basicFile Placeholder", "Units Placeholder", 300, 700, 30, 500, true],
        2:[2,"AM","basicFile Placeholder", "Units Placeholder", 10, 25, 30, 500, false],
        3:[3,"EC","basicFile Placeholder", "Units Placeholder", 10000, 20000, 30, 500, false],
        4:[4,"N","basicFile Placeholder", "Units Placeholder", 800, 2500, 30, 500, false],
        6:[6,"F","basicFile Placeholder", "Units Placeholder", 10000, 20000, 30, 100, true],
        8:[8,"M","basicFile Placeholder", "Units Placeholder", 500, 1000, 30, 500, false]]
    
    
    
    @IBOutlet weak var ConnectionStatusLabel: UILabel!
    @IBOutlet weak var BatteryLabel: UILabel!
    @IBOutlet weak var GettingInfoLabel: UILabel!
    @IBOutlet weak var CTdeviceLabel: UILabel!
    @IBOutlet weak var TimersStopLabel: UILabel!
    @IBOutlet weak var ConversionFactorLabel: UILabel!
    @IBOutlet weak var BasicFWversionLabel: UILabel!
    @IBOutlet weak var UnitIDLabel: UILabel!
    @IBOutlet weak var DetectorNameLabel: UILabel!
    @IBOutlet weak var TimersRunningLabel: UILabel!
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var peripheralName: UILabel!
    @IBOutlet weak var blurView: UIVisualEffectView!
    @IBOutlet weak var rssiLabel: UILabel!
    
    var peripheral: CBPeripheral?
//    var rssiReloadTimer: Timer?
    var consoleTimer: Timer?
    var services: [CBService] = []
    
    override func viewDidAppear(_ animated: Bool) {
        NotificationCenter.default.addObserver(self,
                                               selector: #selector(doSomethingAfterNotified_Console),
                                               name: NSNotification.Name(rawValue: send_msg_to_console_NotificationKey),
                                               object: nil)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        addSlideMenuButton()
        CTdeviceLabel.text = ""
        TimersStopLabel.text = ""
        ConversionFactorLabel.text = ""
        BasicFWversionLabel.text = ""
        UnitIDLabel.text = ""
        DetectorNameLabel.text = ""
        TimersRunningLabel.text = ""
        print ("connected view viewDidLoad 001")
        peripheral?.delegate = self
        //peripheralName.text = peripheral?.name
        Detector_NAME = peripheral?.name
        self.navigationItem.title = Detector_NAME
        print ("radiation : ", Detector_NAME!)
        if Detector_NAME != nil {
            if ((Detector_NAME?.hasPrefix("CT-")))!{
                CT_Device = true
                GOT_DEVICE_NAME = Detector_NAME!
                print ("writepending yes this is a CT device")
            } else {
                CT_Device = false
                //GOT_DEVICE_NAME = ""
                print ("writepending NO this is not a CT device")
            }
        }
        Singleton.sharedInstance.centralManager?.delegate = self
        
        let blurEffect = UIBlurEffect(style: .light)
        blurView.effect = blurEffect
        NotificationCenter.default.addObserver(self,
                                               selector: #selector(doThisWhenNotifyCT007),
                                               name: NSNotification.Name(rawValue: myCT007NotificationKey),
                                               object: nil)
        //main timer that refreshes data
        rssiReloadTimer = Timer.scheduledTimer(timeInterval: 1.0, target: self, selector: #selector(PeripheralConnectedViewController.refreshRSSI), userInfo: nil, repeats: true)
        RunLoop.current.add(rssiReloadTimer!, forMode: RunLoop.Mode.common)
    }
    
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.destination is CT007SimpleViewController{
            print ("nitification...002 - segue prepare")
            //destinationViewController.peripheral = selectedPeripheral
        }
    }
    
    
    @objc func doThisWhenNotifyCT007() {
        print("nitification...001")
    }
    
    
    func secondsToHoursMinutesSeconds (seconds : Int) -> (Int, Int, Int) {return (seconds / 3600, (seconds % 3600) / 60, (seconds % 3600) % 60)}
    
    
    @objc func doSomethingAfterNotified_Console(){
        print("nitification...001 _ console")
        let text_to_console = Console_Setting_Text
        SendConsole(msg: text_to_console)
    }
    
    //This tells detector to stop, send info and restart timer
    @objc func GetConsoleData(){
        if Connected && SentMsg == ""{
            MillisecondsTimer = MillisecondsTimer + 1
        }
        if keyFobConnected{
            MillisecondsTimer = 15
        }
        print ("getting timer 300 milis ", MillisecondsTimer )
        connectingAlertController?.message = "The connection was found, reconnecting to device...\n(\(MillisecondsTimer)/15)"
        if MillisecondsTimer == 1 , Connected {
            //Check if device is ready
            SendConsole(msg: command_RESPOND_TEST)
            print ("checking if device is ready")
        }
        if MillisecondsTimer == 2 , Connected {
            SendConsole(msg: command_STOP_TIMER0)
            SendConsole(msg: command_STOP_TIMER2)
            TimersStopLabel.text = "STOPPING TIMERS"
            print ("getting stopping timers")
        }
        if MillisecondsTimer == 3 , Connected {
            SendConsole(msg: command_PRINT_V)
            ConversionFactorLabel.text = "getting Conversion Factor \(GOT_CONVERSION)"
            print ("getting Conversion Factor \(GOT_CONVERSION)")
        }
        if MillisecondsTimer == 4 , Connected {
            SendConsole(msg: command_PRINT_VERSION)
            BasicFWversionLabel.text = "Getting Basic Version \(GOT_BASIC_VERSION)"
            print ("Getting Basic Version \(GOT_BASIC_VERSION)")
        }
        if MillisecondsTimer == 5 , Connected {
            SendConsole(msg: command_PRINT_DEVICE_ID)
            UnitIDLabel.text = "getting Device Unit ID  \(GOT_DEVICE_ID)"
            print ("getting Detector ID  \(GOT_DEVICE_ID)")
        }
        if !CT_Device {
            if MillisecondsTimer == 6 , Connected {
                SendConsole(msg: command_LIST)
                DetectorNameLabel.text = "getting Detector Name  \(GOT_DEVICE_NAME)"
                print ("getting Detector Name  \(GOT_DEVICE_NAME)")
                Detector_NAME = GOT_DEVICE_NAME
            }
        }else{
            DetectorNameLabel.text = "This is a CT-detector : " +  Detector_NAME!
        }
        
        if MillisecondsTimer == 7 , Connected {
            SendConsole(msg: command_PRINT_Beep)
            print ("getting beeping state  \(GOT_DEVICE_ID)")
        }
        if MillisecondsTimer == 8 , Connected {
            SendConsole(msg: command_PRINT_RTime)
            print ("getting Response Time  \(GOT_DEVICE_ID)")
        }
        if MillisecondsTimer == 9 , Connected {
            SendConsole(msg: command_PRINT_DTime)
            print ("getting Dead Time  \(GOT_DEVICE_ID)")
        }
        if MillisecondsTimer == 10 , Connected {
            SendConsole(msg: command_PRINT_Alarm)
            print ("getting alarm state  \(GOT_DEVICE_ID)")
        }
        if MillisecondsTimer == 11 , Connected {
            SendConsole(msg: command_PRINT_SCREENS)
            print ("getting screens state  \(GOT_DEVICE_ID)")
        }
        if MillisecondsTimer == 12 , Connected {
            SendConsole(msg: command_PRINT_UNITS)
            print ("getting units state  \(GOT_DEVICE_ID)")
        }
        if MillisecondsTimer == 13 , Connected {
            SendConsole(msg: command_PRINT_Alarm_Level)
            print ("getting units state  \(GOT_DEVICE_ID)")
        }
        if MillisecondsTimer == 14 , Connected {
            SendConsole(msg: command_CLEAR_COUNTER)
            SendConsole(msg: command_BEFORE_TIMERS)
            SendConsole(msg: command_RUN_TIMER_0)
            SendConsole(msg: command_RUN_TIMER_2)
            print ("getting running timers again")
            TimersRunningLabel.text = "Timers running again..."
        }
        if MillisecondsTimer == 15, Connected {
            performSegue(withIdentifier: "ConnectedViewSegue", sender: self)
            if consoleTimer != nil {
                consoleTimer?.invalidate()
                consoleTimer = nil
            }
            MillisecondsTimer = 0
            print("getting timer 300 mi \(MillisecondsTimer)")
            doneLoadingDevice = true
            removeConnectingAlert()
//            if connectingAlertVisible{
//                print("333 dismissed")
//                print("987 connecting dismissed")
//                connectingAlertController?.dismiss(animated: true, completion: nil)
//                connectingAlertVisible = false
//            }else{
//                print("333 should have dismissed")
//            }
        }
    }
    
    func disconnectFunc(){
        AudioSingleton.sharedInstance.playDiscAlert()
//        if !disconnectAlertVisible{
//            disconnectAlertVisible = true
//            disconnectAlertController = UIAlertController(title: "Alert!", message: "The detector was disconnected. GammaGuard will attempt to reconnect every \(reconnectionTime) seconds" + "\nTime to reconnect: 30", preferredStyle: .alert)
//            disconnectAlertController?.addAction(UIAlertAction(title: NSLocalizedString("Cancel", comment: "Default action"), style: .`default`, handler: { _ in
//                NSLog("The \"OK\" alert occured.")
//                if var topController = UIApplication.shared.keyWindow?.rootViewController {
//                    while let presentedViewController = topController.presentedViewController {
//                        topController = presentedViewController
//                    }
//                    if topController is TabBarController{
//                        let tabBar = topController as! TabBarController
//                        if tabBar.selectedViewController is UINavigationController{
//                            let navControl = tabBar.selectedViewController as! UINavigationController
//                            if navControl.topViewController != nil{
//                                let curVC = navControl.topViewController
//                                if curVC is CT007SimpleViewController{
//                                    let vc = curVC as! CT007SimpleViewController
//                                    vc.DisconnectButton.setTitle("Back to Scan", for: .normal)
//                                }
//                                else if curVC is CT007detailVC{
//                                    let vc = curVC as! CT007detailVC
//                                    vc.DisconnectButton.setTitle("Back to Scan", for: .normal)
//                                }
//                                else if curVC is CT007InterpretVC{
//                                    let vc = curVC as! CT007InterpretVC
//                                    vc.DisconnectButton.setTitle("Back to Scan", for: .normal)
//                                }else{
//                                    print("test 1 could not find ")
//                                }
//                            }
//                        }
//                    }
//                }
////                self.disconnectButtonPressed()
//                self.reconTimer?.invalidate()
//                self.serviceTimer?.invalidate()
//                disconnectAlertVisible = false
//            }))
//            if var topController = UIApplication.shared.keyWindow?.rootViewController {
//                //testing this
//                if connectingAlertVisible{
//                    print("333 dismissed")
//                    print("987 connecting dismissed")
////                    print("98 \(connectingAlertController)")
//                    connectingAlertController?.dismiss(animated: true){
//                        print("987 ACTUALLY DISMISSED")
//                    }
//                    connectingAlertVisible = false
//                }
//                //
//                while let presentedViewController = topController.presentedViewController {
//                    if !(presentedViewController is UIAlertController){
//                        topController = presentedViewController
//                    }else{
//                        break
//                    }
//                }
//                print("987 disconnecting alert presented")
//                print("98 \(topController)")
////                print("98 \(disconnectAlertController)")
//                topController.present(disconnectAlertController!, animated: true) {
//                    print("987 ACTUALLY PRESENTED")
//                }
////                topController.present(disconnectAlertController!, animated: true, completion: nil)
//                print("test9123 presenting disconnect alert")
//            }
//        }
    }
    
    func SendConsole (msg: String){
        SentMsg = msg
        print ("sent to console : ", msg)
        let command = msg + "\n"
        if let command = command.data(using: String.Encoding.ascii, allowLossyConversion: false) {
            write(command, characteristic: MyOutPutChar!, type: .withResponse)
        }
        if msg == command_STOP_TIMER2 || msg == command_RUN_TIMER_2{
            SentMsg = ""
        }
    }
    
    
    @objc func refreshRSSI(){
        CT007DisplayOnScreen()
        peripheral?.readRSSI()
        //notify the variables to other camera view controllers
        NotificationCenter.default.post(name: Notification.Name(rawValue: myCT007NotificationKey), object: self)
        Radiation_Characteristics_Value_Int = 0
    }
    
    
    func CT007DisplayOnScreen (){
//        let timeBefore = NSDate().timeIntervalSince1970
        OneSecTimer = OneSecTimer + 1
        periphGlob = self.peripheral
        //MARK: connection status .....
        if self.peripheral?.state == CBPeripheralState.connected {
            Connected = true
            ConnectionState = "Connected"
            ConnectionStatusLabel.text = ConnectionState
        }else if self.peripheral?.state == CBPeripheralState.disconnected {
            Connected = false
//            rssiReloadTimer?.invalidate()
            ConnectionState = "Disconnected"
            ConnectionStatusLabel.text = ConnectionState
        }else if self.peripheral?.state == CBPeripheralState.connecting {
            Connected = false
            ConnectionState = "Scanning"
            ConnectionStatusLabel.text = ConnectionState
        }else if self.peripheral?.state == CBPeripheralState.disconnecting {
            Connected = false
            ConnectionState = "Disconnecting"
            ConnectionStatusLabel.text = ConnectionState
        }
        
        if OneSecTimer == 2 , Connected {
            CTData.graphCounts = 0
            CTData.graphDurationCounter = 0
            consoleTimer = Timer.scheduledTimer(timeInterval: 0.5,
                                                target: self,
                                                selector: #selector(PeripheralConnectedViewController.GetConsoleData),
                                                userInfo: nil,
                                                repeats: true)
        }
        print("Radiation_Characteristics_Value:   " , Radiation_Characteristics_Value_Int)
        //Time correction
        if keyFobConnected{
            CTData.addCT007Counts(counts: Double(Radiation_Characteristics_Value_Int))
        }else{
            CTData.addCT007Counts(counts: deadTimeCorrection(counts: timeCountCorrection(Counts: Radiation_Characteristics_Value_Int)))
        }
        //gets time in Hours:Minutes:Seconds
        let time = secondsToHoursMinutesSeconds(seconds: CTData.totalTime)
        var seconds = String(time.2)
        var minutes = String(time.1)
        var hours = String(time.0)
        if time.0 < 10{
            hours = "0"+String(time.0)
        }
        if time.1 < 10{
            minutes = "0"+String(time.1)
        }
        if time.2 < 10{
            seconds = "0"+String(time.2)
        }
        Display_TotalTime_Label = "\(hours):\(minutes):\(seconds)"
        
        // Displays correct doserate for mode and units and sets current levels
        if CT007Mode == "Contamination"{
            CT007CurrentYellowLevel = CT007YellowLevelContam
            CT007CurrentRedLevel = CT007RedLevelContam
            CT007CurrentMaxLevel = CT007MaxLevelContam
            if CT007_ContamUnits == "CPM"{
                Display_DoseRate_Short_Label = String(Int(CTData.getCT007_uSv_h_DoseRate(array: "CPM")))
                Display_DoseRate_Long_Label = String(Int(CTData.getLongCT007_uSv_h_DoseRate(array: "CPM")))
                Display_TotalDose_Label = String(format: "%.0f",CTData.totalCT007Counts)
                Display_DoseRate_Short_Units_Label = "CPM"
                Display_TotalDose_Units_Label = "C"
                Display_OverallDoseRate_Label = String(format: "%.2f", 60*(Double(CTData.totalCT007Counts)/Double(CTData.totalTime)))
            }else{
                Display_DoseRate_Short_Label = String(CTData.getCT007_uSv_h_DoseRate(array: "CPM")/60)
                Display_DoseRate_Long_Label = String(CTData.getLongCT007_uSv_h_DoseRate(array: "CPM")/60)
                Display_TotalDose_Label = String(format: "%.0f",CTData.totalCT007Counts)
                Display_DoseRate_Short_Units_Label = "CPS"
                Display_TotalDose_Units_Label = "C"
                Display_OverallDoseRate_Label = String(format: "%.2f", (Double(CTData.totalCT007Counts)/Double(CTData.totalTime)))
            }
        }else{
            CT007CurrentYellowLevel = CT007YellowLevelGamma
            CT007CurrentRedLevel = CT007RedLevelGamma
            CT007CurrentMaxLevel = CT007MaxLevelGamma
            switch CT007_GammaUnits {
            case "µSv/h":
                Display_DoseRate_Short_Label = String(CTData.getCT007_uSv_h_DoseRate(array: "µSv_h"))
                Display_DoseRate_Long_Label = String(CTData.getLongCT007_uSv_h_DoseRate(array: "µSv_h"))
                Display_TotalDose_Label = String(format: "%.2f", CTData.totalCT007Dose)
                Display_DoseRate_Short_Units_Label = "µSv/h"
                Display_TotalDose_Units_Label = "µSv"
                Display_OverallDoseRate_Label = String(format: "%.2f", 3600*(CTData.totalCT007Dose/Double(CTData.totalTime)))
            case "mSv/h":
                Display_DoseRate_Short_Label = String(CTData.getCT007_uSv_h_DoseRate(array: "µSv_h")/1000)
                Display_DoseRate_Long_Label = String(CTData.getLongCT007_uSv_h_DoseRate(array: "µSv_h")/1000)
                Display_TotalDose_Label = String(format: "%.2f", CTData.totalCT007Dose/1000)
                Display_DoseRate_Short_Units_Label = "mSv/h"
                Display_TotalDose_Units_Label = "mSv"
                Display_OverallDoseRate_Label = String(format: "%.2f", 3600*(CTData.totalCT007Dose/1000)/Double(CTData.totalTime))
            case "mRem/h":
                Display_DoseRate_Short_Label = String(CTData.getCT007_uSv_h_DoseRate(array: "µSv_h")/10)
                Display_DoseRate_Long_Label = String(CTData.getLongCT007_uSv_h_DoseRate(array: "µSv_h")/10)
                Display_TotalDose_Label = String(format: "%.2f", CTData.totalCT007Dose/10)
                Display_DoseRate_Short_Units_Label = "mRem/h"
                Display_TotalDose_Units_Label = "mRem"
                Display_OverallDoseRate_Label = String(format: "%.2f", 3600*(CTData.totalCT007Dose/10)/Double(CTData.totalTime))
            default:
                Display_DoseRate_Short_Label = String(CTData.getCT007_uSv_h_DoseRate(array: "CPM"))
                Display_DoseRate_Long_Label = String(CTData.getLongCT007_uSv_h_DoseRate(array: "CPM"))
                Display_TotalDose_Label = String(CTData.totalCT007Counts)
                Display_DoseRate_Short_Units_Label = "CPM"
                Display_TotalDose_Units_Label = "C"
                Display_OverallDoseRate_Label = String(format: "%.2f", 60*(CTData.totalCT007Counts/Double(CTData.totalTime)))
            }
        }
        if CT007Mode != "Contamination" || CT007_ContamUnits != "CPM"{
            switch Double(Display_DoseRate_Short_Label)!{
            case 0 ... 1:
                Display_DoseRate_Short_Label = String(format: "%.3f", Double(Display_DoseRate_Short_Label)!)
            case 1 ... 10:
                Display_DoseRate_Short_Label = String(format: "%.2f", Double(Display_DoseRate_Short_Label)!)
            case 10 ... 100:
                Display_DoseRate_Short_Label = String(format: "%.1f", Double(Display_DoseRate_Short_Label)!)
            default:
                Display_DoseRate_Short_Label = String(format: "%.0f", Double(Display_DoseRate_Short_Label)!)
            }
        }
        if IsCT007LoggingEnabled{//} && Connected {
            LogCT007Timer += 1
            if LogCT007Timer >= CT007LogInterval {
                LogExternalData()
                LogCT007Timer = 0
            }
        }
        if playAlarm{
            print("Played alarm")
            notificationWarning()
            AudioSingleton.sharedInstance.playAlarm()
            playAlarm = false
        }
        // Add back in if going back to timed scan
//        if timeToReconnect >= 0{
//            disconnectAlertController?.message = "The detector was disconnected. GammaGuard will attempt to reconnect every \(reconnectionTime) seconds" + "\nTime to reconnect: " + String(timeToReconnect)
//            //            alertController?.textFields?.last?.text = "Time to reconnect: " + String(timeToReconnect)
//            timeToReconnect -= 1
//        }
    }
    
    func notificationWarning (){
        if #available(iOS 10.0, *) {
            let content = UNMutableNotificationContent()
            content.title = NSString.localizedUserNotificationString(forKey: "WARNING!!!", arguments: nil)
            if CT007Mode == "Gamma"{
                content.body = NSString.localizedUserNotificationString(forKey: "Dose rate of "+Display_DoseRate_Short_Label+" "+CT007_GammaUnits+" exceeds warning level", arguments: nil)
            }else{
                content.body = NSString.localizedUserNotificationString(forKey: "Count rate of "+Display_DoseRate_Short_Label+" "+CT007_GammaUnits+" exceeds warning level", arguments: nil)
            }
            //will need to look into how to play custom sounds
            // searches only "Library/Sounds" and bundle
            content.sound = UNNotificationSound(named: convertToUNNotificationSoundName("Audio/Alarms/radiation_alert.caf"))
            let trigger = UNTimeIntervalNotificationTrigger(timeInterval: 1, repeats: false)
            let request = UNNotificationRequest(identifier: "Now", content: content, trigger: trigger) // Schedule the notification.
            let center = UNUserNotificationCenter.current()
            center.add(request) { (error : Error?) in
                if error != nil {
                    // Handle any errors
                }
            }
        } else {
            // Fallback on earlier versions
            let notification = UILocalNotification()
            notification.fireDate = NSDate(timeIntervalSinceNow: 1) as Date
            if CT007Mode == "Gamma"{
                notification.alertBody = "Dose rate of "+Display_DoseRate_Short_Label+" "+CT007_GammaUnits+" exceeds warning level"
            }else{
                notification.alertBody = "Count rate of "+Display_DoseRate_Short_Label+" "+CT007_ContamUnits+" exceeds warning level"
            }
            notification.alertAction = "WARNING!!!"
            notification.soundName = "radiation_alert.caf"
            UIApplication.shared.scheduleLocalNotification(notification)
        }
    }
    
    func timeCountCorrection(Counts: Int) -> Double{
        if packetCount == 0{
            return 0
        }
        let duration: Double = 1
        let correctedValue = (Double(Counts)/duration) * ((duration/0.2)/Double(packetCount))
        return correctedValue
    }
    
    func deadTimeCorrection(counts: Double) -> Double{
        let duration: Double = 1
        if (CT007DeadTime == 0) {
            packetCount = 0
            return counts
        }
        else {
            if (((counts / Double(CT007DeadTime*(packetCount/5))) / duration) < 1) {
                let correctedVal1 = (counts / (1 - ((counts / Double(CT007DeadTime*(packetCount/5))) / duration)))
//                let correctedVal2 = (counts / (1 - ((counts / Double(CT007DeadTime*(packetCount/5))) / (Double(packetCount)*0.2))))
                packetCount = 0
                return correctedVal1
            }
            else {
                packetCount = 0
                return counts
            }
        }
    }
    
    func disconnectButtonPressed() {
        //this will call didDisconnectPeripheral, but if any other apps are using the device it will not immediately disconnect
        //Singleton.sharedInstance.centralManager?.cancelPeripheralConnection(peripheral!)
        //Singleton.centralManager?.cancelPeripheralConnection(peripheral!)
        rssiReloadTimer?.invalidate()
        Connected = false
        fromHome = true
        Singleton.sharedInstance.centralManager?.cancelPeripheralConnection(periphGlob!)
        //test
        peripheral = nil
        periphGlob = nil
        //test
        print ("disconnecting....")
    }
    
    func LogExternalData() {
        if IsCT007LoggingEnabled {
            let date = Date()
            let formatter = DateFormatter()
            formatter.dateFormat = "yyyy/MM/dd' 'hh:mm:ssa"
            var datedata = formatter.string(from: date)
            datedata = datedata + " > "
            print ("datedata=  \(datedata)")
            let array = [datedata , Detector_NAME!,
                         "Battery:" ,String(BatteryLevel_Characteristics_Value_Int) ,
                         "Conversion Factor" , String(CTData.C_to_uSv_Conversion) ,
                         "Selected Response Average [\(Display_DoseRate_Short_Units_Label)]" , Display_DoseRate_Short_Label,
                         "Max Response Average [\(Display_DoseRate_Short_Units_Label)]" , Display_DoseRate_Long_Label,
                         "Total Dose [\(Display_TotalDose_Units_Label)]" , Display_TotalDose_Label]
            
            let stringRepresentation = array.joined(separator: "\t") // "1-2-3"
            let whatTowrite = stringRepresentation + "\n"
            
            //EXTERNALwriteStringHeaders.append(whatTowrite + "\n")
            print ("whatTowrite from main CT007View: " ,  whatTowrite)
            let data = whatTowrite.data(using: String.Encoding.utf8, allowLossyConversion: false)!
            
            if FileManager.default.fileExists(atPath: ExternalfileURL.path) {
                print ("External LOGLOG 01")
                do {
                    let fileHandle = try FileHandle(forWritingTo: ExternalfileURL)
                    
                    fileHandle.seekToEndOfFile()
                    fileHandle.write(data)
                    fileHandle.closeFile()
                    print ("External LOGLOG 02")
                } catch {
                    print ("External LOGLOG 03")
                    print("External Can't open fileHandle \(error)")
                }
            } else {
                print ("External LOGLOG 04")
                do {
                    print ("External LOGLOG 05")
                    // Write to the file
                    try EXTERNALwriteStringHeaders.write(to: ExternalfileURL, atomically: true, encoding: String.Encoding.utf8)
                } catch let error as NSError {
                    print ("ExternalLOGLOG 06")
                    print("ERROR...Failed writing to URL: \(ExternalfileURL), Error: " + error.localizedDescription)
                }
            }
        } else {
            print ("CT007 logging is disabled")
        }
    }
    
    //Loads all saved settings for External Detector
    func GetCT007SavedSettings(){
        print ("getting CT007 saved settings")
        if defaults.object(forKey: "Saved_IsCT007LoggingEnabled") != nil {
            IsCT007LoggingEnabled = defaults.bool(forKey: "Saved_IsCT007LoggingEnabled")
            print ("Saved_IsCT007LoggingEnabled not nil")
        }
        if defaults.object(forKey: "Saved_Mode") != nil {
            CT007Mode = defaults.string(forKey: "Saved_Mode")!
            print ("Saved_Mode not nil")
        }
        if defaults.object(forKey: "Saved_CT007LogInterval") != nil {
            CT007LogInterval = defaults.integer(forKey: "Saved_CT007LogInterval")
            print ("Saved_CT007LogInterval not nil")
        }
        if defaults.object(forKey: "Saved_CT007_GammaUnits") != nil {
            CT007_GammaUnits = defaults.string(forKey: "Saved_CT007_GammaUnits")!
            print ("Saved_CT007_GammaUnits not nil")
        }
        if defaults.object(forKey: "Saved_CT007_ContamUnits") != nil {
            CT007_ContamUnits = defaults.string(forKey: "Saved_CT007_ContamUnits")!
            print ("Saved_CT007_ContamUnits not nil")
        }
        if defaults.object(forKey: "Saved_MinimumCounts") != nil {
            CT007MinimumCounts = defaults.integer(forKey: "Saved_MinimumCounts")
            //                CT007MinimumCountsLabel.text = "Minimum Counts | %Error: %"+String(CT007MinimumCounts)
            print ("Saved_MinimumCounts not nil")
        }
        if defaults.object(forKey: "Saved_YellowLevelGamma") != nil {
            CT007YellowLevelGamma = defaults.double(forKey: "Saved_YellowLevelGamma")
            print ("Saved_YellowLevelGamma not nil")
        }
        if defaults.object(forKey: "Saved_RedLevelGamma") != nil {
            CT007RedLevelGamma = defaults.double(forKey: "Saved_RedLevelGamma")
            print ("Saved_RedLevelGamma not nil")
        }
        if defaults.object(forKey: "Saved_MaxLevelGamma") != nil {
            CT007MaxLevelGamma = defaults.double(forKey: "Saved_MaxLevelGamma")
            print ("Saved_MaxLevelGamma not nil")
        }
        if defaults.object(forKey: "Saved_YellowLevelContamination") != nil {
            CT007YellowLevelContam = defaults.double(forKey: "Saved_YellowLevelContamination")
            print ("Saved_YellowLevelContamination not nil")
        }
        if defaults.object(forKey: "Saved_RedLevelContamination") != nil {
            CT007RedLevelContam = defaults.double(forKey: "Saved_RedLevelContamination")
            print ("Saved_RedLevelContamination not nil")
        }
        if defaults.object(forKey: "Saved_MaxLevelContamination") != nil {
            CT007MaxLevelContam = defaults.double(forKey: "Saved_MaxLevelContamination")
            print ("Saved_MaxLevelContamination not nil")
        }
        if defaults.object(forKey: "Saved_DeviceBeep") != nil {
            appBeep = defaults.bool(forKey: "Saved_DeviceBeep")
            print ("Saved_DeviceBeep not nil")
        }
        if defaults.object(forKey: "Saved_DeviceAlarm") != nil {
            CT007DeviceAlarm = defaults.bool(forKey: "Saved_DeviceAlarm")
            print ("Saved_DeviceAlarm not nil")
        }
        if defaults.object(forKey: "Saved_LiveGraphEnabled") != nil{
            isLiveGraphEnabled = defaults.bool(forKey: "Saved_LiveGraphEnabled")
            print ("Saved_LiveGraphEnabled not nil")
        }
        if defaults.object(forKey: "Saved_graphPointDuration") != nil{
            graphPointDuration = defaults.integer(forKey: "Saved_graphPointDuration")
            print ("Saved_graphPointDuration not nil")
        }
        if defaults.object(forKey: "Saved_countsToBeep") != nil{
            CT007CountsToBeep = defaults.integer(forKey: "Saved_countsToBeep")
            print ("Saved_countsToBeep not nil")
        }
    }
    //----------------------------
    // Functions for playing beeps
    //----------------------------
    
    //called for every beep
    @objc func playSound() {
        toneCount += 1
        if toneCount >= totalTones{
            if toneTimer != nil {
                toneTimer?.invalidate()
                toneTimer = nil
            }
        }
        AudioSingleton.sharedInstance.playBeep()
    }
    
    // calculates how many tones should be played
    func Tones(counts: Int) {
        let tonesToAdd = Double(counts) * (1/Double(CT007CountsToBeep))
        fractionalTones += tonesToAdd
        let playTones: Int = Int(fractionalTones)
        fractionalTones -= Double(playTones)
        calculateSounds(playTones: playTones)
    }
    
    //restricts maximum tones to one per 30 MS in a 200 MS time period
    //evenly spaces out tones over the 200 MS window
    func calculateSounds(playTones: Int){
        if toneTimer != nil{
            toneTimer?.invalidate()
            toneTimer = nil
        }
        var val = playTones
        val += (totalTones-toneCount)
        let cap: Int = time / 30
        if val > cap{
            val = cap
        }
        if val > 0{
            totalTones = val
            toneCount = 0
            if val == 1{
                playSound()
            }else{
                let timeBetweenTones = Double(time) / Double((val-1)*1000)
                playTonesOverInterval(repeatTime: timeBetweenTones)
            }
        }
    }
    
    //Starts timer for playing beeps
    func playTonesOverInterval(repeatTime: Double){
        playSound()
        toneTimer = Timer.scheduledTimer(timeInterval: repeatTime, target: self,  selector: #selector(playSound), userInfo: nil, repeats: true)
//        let date = Date()
//        let calendar = Calendar.current
//        let nseconds: Double = Double(calendar.component(.nanosecond, from: date))/1000000000
//        let seconds: Double = Double(calendar.component(.second, from: date)) + nseconds
    }
    //-----------------------------------
    // End of Functions for playing beeps
    //-----------------------------------
}


extension PeripheralConnectedViewController: UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        print ("connected view 005")
        return services.count
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "ServiceCell") as! ServiceTableViewCell
        cell.serviceNameLabel.text = "\(services[indexPath.row].uuid)"
        return cell
    }
}

// Helper function inserted by Swift 4.2 migrator.
fileprivate func convertToUNNotificationSoundName(_ input: String) -> UNNotificationSoundName {
	return UNNotificationSoundName(rawValue: input)
}
