//
//  CT007Settings.swift
//  UITableVC
//
//  Created by sepehr khatir on 2017-03-16.
//  Copyright © 2017 EIC. All rights reserved.
//

import Foundation
import UIKit
import MessageUI

var Console_Setting_Text = ""
var Console_Setting_Text_Respond = ""
    
class CT007SettingsViewController: UITableViewController, UITextFieldDelegate, UIPickerViewDelegate, UIPickerViewDataSource, MFMailComposeViewControllerDelegate , UIGestureRecognizerDelegate {
  
        //-----------------------
        // IBOutlets and Variables
        //-----------------------
        var CT007GammaUnitsOptions = ["µSv/h","mSv/h","mRem/h"]
        var CT007ContamUnitsOptions = ["CPM","CPS"]
        var CT007ScreensOptions = ["Rate(1)","Rate(2)","Total Counts/Dose","Detector Info"]
        var CT007DetectorUnitOptions = ["1: CPM | 2: µSv", "1: µSv | 2: CPM", "1: CPM | 2: mSv", "1: mSv | 2: CPM"]
        let CT007GammaUnitsOptionsPicker = UIPickerView()
        let CT007ContamUnitsOptionsPicker = UIPickerView()
        let CT007ScreensOptionsPicker = UIPickerView()
        let CT007DetectorUnitOptionsPicker = UIPickerView()
        let AlarmSoundOptionsPicker = UIPickerView()
        let ToneSoundOptionsPicker = UIPickerView()
        var CT007_Gamma_Units_Tag = 1
        var CT007_Contam_Units_Tag = 1
        var CT007_Detector_Units_Tag = 1
        var ConversionDict: [String: Int] = ["CPM":0,"µSv/h":1,"mSv/h":2,"mRem/h":3,"CPS":4]
        var maxResponseTime = 0
        var selectFromTap = false
        var alarmSounds = [String]()
        var defaultAlarmCount = 0
        var toneSounds = [String]()
        var defaultToneCount = 0
        
        //texts to change settings in detector by sending command to console
        var Beep_ON = "T(2)=1"
        var Beep_OFF = "T(2)=0"
        var Alarm_ON = "T(12)=1"
        var Alarm_OFF = "T(12)=0"
        
        @IBOutlet weak var CT007GammaUnitsTextField: UITextField!
        @IBOutlet weak var CT007ContamUnitsTextField: UITextField!
        @IBOutlet weak var CT007LogIntTextField: UITextField!
        @IBOutlet weak var CT007ResponseTimeTextField: UITextField!
        @IBOutlet weak var CT007MinimumCountsTextField: UITextField!
        @IBOutlet weak var CT007YellowLevelGammaTextField: UITextField!
        @IBOutlet weak var CT007RedLevelGammaTextField: UITextField!
        @IBOutlet weak var CT007MaxLevelGammaTextField: UITextField!
        @IBOutlet weak var CT007YellowLevelContaminationTextField: UITextField!
        @IBOutlet weak var CT007RedLevelContaminationTextField: UITextField!
        @IBOutlet weak var CT007MaxLevelContaminationTextField: UITextField!
        @IBOutlet weak var CT007PointIntervalTextField: UITextField!
        @IBOutlet weak var CT007DeadTimeTextField: UITextField!
        @IBOutlet weak var CT007CountsToBeepTextField: UITextField!
        @IBOutlet weak var CT007ScreensTextField: UITextField!
        @IBOutlet weak var CT007DeviceUnitsTextField: UITextField!
        @IBOutlet weak var CT007ConversionFactorTextField: UITextField!
        @IBOutlet weak var CT007AlarmLevelTextField: UITextField!
        @IBOutlet weak var CountsFilterTextField: UITextField!
        @IBOutlet weak var AlarmSoundTextField: UITextField!
        @IBOutlet weak var ToneSoundTextField: UITextField!
    
        @IBOutlet weak var CT007DeviceAlarmSwitch: UISwitch!
        @IBOutlet weak var CT007DeviceBeepSwitch: UISwitch!
        @IBOutlet weak var CT007LoggingSwitch: UISwitch!
        @IBOutlet weak var CT007_Detector_AlarmSwitch: UISwitch!
        @IBOutlet weak var CT007_Detector_BeepSwitch: UISwitch!
        @IBOutlet weak var LiveGraphSwitch: UISwitch!
        
        @IBOutlet weak var CT007LogLabel: UILabel!
        @IBOutlet weak var CT007DeviceAlarmLabel: UILabel!
        @IBOutlet weak var appBeepLabel: UILabel!
        @IBOutlet weak var CT007LogIntLabel: UILabel!
        @IBOutlet weak var CT007_Detector_AlarmLabel: UILabel!
        @IBOutlet weak var CT007_Detector_BeepLabel: UILabel!
        @IBOutlet weak var CT007MinimumCountsLabel: UILabel!
        @IBOutlet weak var LiveGraphLabel: UILabel!
        @IBOutlet weak var EmailButton: UIButton!
        //-------------------------------
        // End of IBOutlets and Variables
        //-------------------------------
        
        //--------------------------
        // View Controller Functions
        //--------------------------
        override func viewDidLoad() {
            super.viewDidLoad()
            //addSlideMenuButton()
            self.tabBarController?.tabBar.isHidden = true
            self.navigationItem.hidesBackButton = true
            let newBackButton = UIBarButtonItem(title: "< Detector", style: UIBarButtonItem.Style.plain, target: self, action: #selector(CT007SettingsViewController.GoBacktoExternalView))
            self.navigationItem.leftBarButtonItem = newBackButton
            
            /********** Text Field Tags **********/
            CT007LogIntTextField.tag = 1
            CT007ResponseTimeTextField.tag = 2
            CT007MinimumCountsTextField.tag = 3
            CT007YellowLevelGammaTextField.tag = 4
            CT007RedLevelGammaTextField.tag = 5
            CT007MaxLevelGammaTextField.tag = 6
            CT007YellowLevelContaminationTextField.tag = 7
            CT007RedLevelContaminationTextField.tag = 8
            CT007MaxLevelContaminationTextField.tag = 9
            CT007PointIntervalTextField.tag = 10
            CT007DeadTimeTextField.tag = 11
            CT007CountsToBeepTextField.tag = 12
            CT007AlarmLevelTextField.tag = 13
            CT007ConversionFactorTextField.tag = 14
            /********** End of Text Field Tags **********/
            
            //load detector details
            if CT007DetectorDetails.count > 0{
                maxResponseTime = CT007DetectorDetails[6] as! Int
            }
            //End of loading detector details
            print("Settings loaded |","Response Time:", CT007ResponseTime)
            
            /********** Text Field delagates and declarations **********/
            CT007LogIntTextField.delegate = self
            CT007LogIntTextField.text = "\(CT007LogInterval)"
            CT007GammaUnitsTextField.delegate = self
            CT007GammaUnitsTextField.text = CT007_GammaUnits
            CT007ContamUnitsTextField.delegate = self
            CT007ContamUnitsTextField.text = CT007_ContamUnits
            CT007ResponseTimeTextField.delegate = self
            CT007ResponseTimeTextField.text = "\(CT007ResponseTime)"
            CT007MinimumCountsTextField.delegate = self
            CT007MinimumCountsTextField.text = "\(CT007MinimumCounts)"
            CT007YellowLevelGammaTextField.delegate = self
            CT007YellowLevelGammaTextField.text = "\(CT007YellowLevelGamma)"
            CT007RedLevelGammaTextField.delegate = self
            CT007RedLevelGammaTextField.text = "\(CT007RedLevelGamma)"
            CT007MaxLevelGammaTextField.delegate = self
            CT007MaxLevelGammaTextField.text = "\(CT007MaxLevelGamma)"
            CT007YellowLevelContaminationTextField.delegate = self
            CT007YellowLevelContaminationTextField.text = "\(CT007YellowLevelContam)"
            CT007RedLevelContaminationTextField.delegate = self
            CT007RedLevelContaminationTextField.text = "\(CT007RedLevelContam)"
            CT007MaxLevelContaminationTextField.delegate = self
            CT007MaxLevelContaminationTextField.text = "\(CT007MaxLevelContam)"
            CT007PointIntervalTextField.delegate = self
            CT007PointIntervalTextField.text = "\(graphPointDuration)"
            CT007DeadTimeTextField.delegate = self
            CT007DeadTimeTextField.text = "\(CT007DeadTime)"
            CT007CountsToBeepTextField.delegate = self
            CT007CountsToBeepTextField.text = "\(CT007CountsToBeep)"
            CT007ScreensTextField.delegate = self
            CT007ScreensTextField.text = "Select Screens"
            CT007DeviceUnitsTextField.delegate = self
            CT007DeviceUnitsTextField.text = CT007DetectorUnitOptions[CT007Units-1]
            CT007AlarmLevelTextField.text = "\(CT007AlarmLevel)"
            CT007AlarmLevelTextField.delegate = self
            CT007ConversionFactorTextField.text = "\(CT007V)"
            CT007ConversionFactorTextField.delegate = self
            AlarmSoundTextField.text = "CHANGE"
            AlarmSoundTextField.delegate = self
            ToneSoundTextField.text = "CHANGE"
            ToneSoundTextField.delegate = self

//            CT007MinimumCountsLabel.text = "Minimum Counts | %Error: %"+String(Double(CT007MinimumCounts).squareRoot())
            /********** End of Text Field delagates and declarations **********/
            
            /********** Text Field Picker delagates and datasources and declarations **********/
            CT007GammaUnitsOptionsPicker.delegate = self
            CT007GammaUnitsOptionsPicker.dataSource = self
            CT007ContamUnitsOptionsPicker.delegate = self
            CT007ContamUnitsOptionsPicker.dataSource = self
            CT007ScreensOptionsPicker.delegate = self
            CT007ScreensOptionsPicker.dataSource = self
            CT007DetectorUnitOptionsPicker.delegate = self
            CT007DetectorUnitOptionsPicker.dataSource = self
            AlarmSoundOptionsPicker.delegate = self
            AlarmSoundOptionsPicker.dataSource = self
            ToneSoundOptionsPicker.delegate = self
            ToneSoundOptionsPicker.dataSource = self
            let tap = UITapGestureRecognizer(target: self, action: #selector(pickerTapped))
            tap.delegate = self
            
            CT007ScreensOptionsPicker.addGestureRecognizer(tap)
            /********** End of Text Field Picker delagates and datasources and declarations **********/
            
            /********** Binding Textfield to Pickers **********/
            CT007GammaUnitsOptionsPicker.tag = 0
            CT007ContamUnitsOptionsPicker.tag = 1
            CT007ScreensOptionsPicker.tag = 2
            CT007DetectorUnitOptionsPicker.tag = 3
            AlarmSoundOptionsPicker.tag = 4
            ToneSoundOptionsPicker.tag = 5
            CT007GammaUnitsTextField.inputView = CT007GammaUnitsOptionsPicker
            CT007ContamUnitsTextField.inputView = CT007ContamUnitsOptionsPicker
            CT007ScreensTextField.inputView = CT007ScreensOptionsPicker
            CT007DeviceUnitsTextField.inputView = CT007DetectorUnitOptionsPicker
            AlarmSoundTextField.inputView = AlarmSoundOptionsPicker
            ToneSoundTextField.inputView = ToneSoundOptionsPicker
            /********** End of Binding Textfield to Pickers **********/

            /********** Logging Switch notifying **********/
            CT007LoggingSwitch.addTarget(self, action: #selector(CT007SettingsViewController.CT007LogStateChanged(_:)), for: UIControl.Event.valueChanged)
            /********** End of Logging Switch notifying **********/
            
            /********** Logging Switch notifying **********/
            CT007_Detector_BeepSwitch.addTarget(self, action: #selector(CT007SettingsViewController.Detector_Beep_state_changed(_:)), for: UIControl.Event.valueChanged)
            /********** End of Logging Switch notifying **********/
            
            /********** Logging Switch notifying **********/
            CT007_Detector_AlarmSwitch.addTarget(self, action: #selector(CT007SettingsViewController.Detector_Alarm_state_changed(_:)), for: UIControl.Event.valueChanged)
            /********** End of Logging Switch notifying **********/
            
            /********** Live Graph Switch notifying **********/
            LiveGraphSwitch.addTarget(self, action: #selector(CT007SettingsViewController.LiveGraphSwitch(_:)), for: UIControl.Event.valueChanged)
            /********** End of Live Graph Switch notifying **********/

            /********** Logging Switches on view load **********/
            if IsCT007LoggingEnabled {
                CT007LoggingSwitch.isOn = true
                CT007LogLabel.text = "Logging is ON"
                CT007LogIntTextField.isEnabled = true
                CT007LogIntLabel.isEnabled = true
            } else {
                CT007LoggingSwitch.isOn = false
                CT007LogLabel.text = "Logging is OFF"
                CT007LogIntTextField.isEnabled = false
                CT007LogIntLabel.isEnabled = false
                CT007LogIntTextField.textColor = UIColor.gray
            }
            /**********End of Logging Switches on view load **********/
            
            /********** Detector Beeping Switches on view load **********/
            if IsDetectorBeepingEnabled {
                print ("beep is enabled 01")
                CT007_Detector_BeepSwitch.isOn = true
                CT007_Detector_BeepLabel.text = "Beeping is ON"
                
            } else {
                print ("beep is not enabled 01")
                CT007_Detector_BeepSwitch.isOn = false
                CT007_Detector_BeepLabel.text = "Beeping is OFF"
            }
            /**********End of Detector Beeping Switches on view load **********/
            
            
            //Device beep switch
            CT007DeviceBeepSwitch.addTarget(self, action: #selector(CT007SettingsViewController.appBeepSwitchStateChanged(_:)), for: UIControl.Event.valueChanged)
            if appBeep {
                CT007DeviceBeepSwitch.isOn = true
                appBeepLabel.text = "Beeping is ON"
            } else {
                CT007DeviceBeepSwitch.isOn = false
                appBeepLabel.text = "Beeping is OFF"
            }
            //Device alarm switch
            CT007DeviceAlarmSwitch.addTarget(self, action: #selector(CT007SettingsViewController.DeviceAlarmSwitchStateChanged(_:)), for: UIControl.Event.valueChanged)
            if CT007DeviceAlarm {
                CT007DeviceAlarmSwitch.isOn = true
                CT007DeviceAlarmLabel.text = "Alarm is ON"
            } else {
                CT007DeviceAlarmSwitch.isOn = false
                CT007DeviceAlarmLabel.text = "Alarm is OFF"
            }
            
            /********** Detector Alarming Switches on view load **********/
            if IsDetectorAlarmingEnabled {
                CT007_Detector_AlarmSwitch.isOn = true
                CT007_Detector_AlarmLabel.text = "Alarm is ON"
                //CT007LogIntTextField.isEnabled = true
                //CT007LogIntLabel.isEnabled = true
            } else {
                CT007_Detector_AlarmSwitch.isOn = false
                CT007_Detector_AlarmLabel.text = "Alarm is OFF"
                //CT007LogIntTextField.isEnabled = false
                //CT007LogIntLabel.isEnabled = false
                //CT007LogIntTextField.textColor = UIColor.gray
            }
            /**********End of Detector Alarming Switches on view load **********/
            
            /********** Live Graph Switches on view load **********/
            if isLiveGraphEnabled {
                LiveGraphSwitch.isOn = true
                LiveGraphLabel.text = "Live Graph is ON"
            } else {
                LiveGraphSwitch.isOn = false
                LiveGraphLabel.text = "Live Graph is Off"
            }
            /**********End of Live Graph Switches on view load **********/
            
            switch CT007_GammaUnits {
            case "µSv/h":
                CT007_Gamma_Units_Tag = 0
                break;
            case "mSv/h":
                CT007_Gamma_Units_Tag = 1
                break;
            case "mRem/h":
                CT007_Gamma_Units_Tag = 2
                break;
            default:
                CT007_Gamma_Units_Tag = 0
            }
            CT007GammaUnitsOptionsPicker.selectRow(CT007_Gamma_Units_Tag, inComponent: 0, animated: true)
            
            switch CT007_ContamUnits {
            case "CPM":
                CT007_Contam_Units_Tag = 0
                break;
            case "CPS":
                CT007_Contam_Units_Tag = 1
                break;
            default:
                CT007_Contam_Units_Tag = 0
            }
            CT007ContamUnitsOptionsPicker.selectRow(CT007_Contam_Units_Tag, inComponent: 0, animated: true)
            
            switch CT007Units {
            case 1:
                CT007_Detector_Units_Tag = 0
                break;
            case 2:
                CT007_Detector_Units_Tag = 1
                break;
            case 3:
                CT007_Detector_Units_Tag = 2
                break;
            case 4:
                CT007_Detector_Units_Tag = 3
                break;
            default:
                CT007_Detector_Units_Tag = 0
            }
            CT007DetectorUnitOptionsPicker.selectRow(CT007_Detector_Units_Tag, inComponent: 0, animated: true)
            
            CT007ScreensOptionsPicker.selectRow(0, inComponent: 0, animated: true)
            
            
        }
        override func viewDidDisappear(_ animated: Bool) {
            super.viewDidDisappear(true)
            print (".......... Console notification observer removed ...........")
            NotificationCenter.default.removeObserver(self)
        }

        
        override func viewWillAppear(_ animated: Bool) {
            super.viewWillAppear(animated)
            navigationController?.navigationBar.barTintColor = UIColor.orange
            getToneFiles()
            getAlarmFiles()
        }
        //---------------------------------
        // End of View Controller Functions
        //---------------------------------
        
        func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
            setupTextFieldsAccessoryView(textField: textField)
            return true
        }
        
        //-----------------------------------
        // Adding "Done" button to numberpads
        //-----------------------------------
        func setupTextFieldsAccessoryView(textField: UITextField) {
            guard textField.inputAccessoryView == nil else {
                print("textfields accessory view already set up")
                return
            }
            let toolBar: UIToolbar = UIToolbar(frame:CGRect(x: 0, y: 0, width: UIScreen.main.bounds.size.width, height: 44))
            toolBar.barStyle = UIBarStyle.default
            toolBar.isTranslucent = true
            let flexsibleSpace: UIBarButtonItem = UIBarButtonItem(barButtonSystemItem: UIBarButtonItem.SystemItem.flexibleSpace, target: nil, action: nil)
            let doneButton: UIBarButtonItem = UIBarButtonItem(barButtonSystemItem: UIBarButtonItem.SystemItem.done, target: self, action: #selector(didPressDoneButton))
            toolBar.items = [flexsibleSpace, doneButton]
            textField.inputAccessoryView = toolBar
        }
        @objc func didPressDoneButton(button: UIButton, textField: UITextField) {
            // Hide keyboard
            CT007GammaUnitsTextField.resignFirstResponder()
            CT007ContamUnitsTextField.resignFirstResponder()
            CT007LogIntTextField.resignFirstResponder()
            CT007YellowLevelGammaTextField.resignFirstResponder()
            CT007RedLevelGammaTextField.resignFirstResponder()
            CT007MaxLevelGammaTextField.resignFirstResponder()
            CT007YellowLevelContaminationTextField.resignFirstResponder()
            CT007RedLevelContaminationTextField.resignFirstResponder()
            CT007MaxLevelContaminationTextField.resignFirstResponder()
            CT007MinimumCountsTextField.resignFirstResponder()
            CT007ResponseTimeTextField.resignFirstResponder()
            CT007PointIntervalTextField.resignFirstResponder()
            CT007DeadTimeTextField.resignFirstResponder()
            CT007CountsToBeepTextField.resignFirstResponder()
            CT007ScreensTextField.resignFirstResponder()
            CT007DeviceUnitsTextField.resignFirstResponder()
            CT007ConversionFactorTextField.resignFirstResponder()
            CT007AlarmLevelTextField.resignFirstResponder()
            AlarmSoundTextField.resignFirstResponder()
            ToneSoundTextField.resignFirstResponder()
        }
        //------------------------------------------
        // End of Adding "Done" button to numberpads
        //------------------------------------------
        
        
        
        @objc func GoBacktoExternalView(){
            if !fromHome {
                performSegue(withIdentifier: "Dtoctsegue", sender: Any?.self)
//                self.navigationController!.popViewController(animated: true)
            } else {
//                let destViewController : UIViewController = self.storyboard!.instantiateViewController(withIdentifier: "C")
//                self.navigationController!.pushViewController(destViewController, animated: true)
                self.navigationController!.popViewController(animated: true)
            }
            
        }
    
        func getAlarmFiles(){
            alarmSounds.removeAll()
            let allPaths = NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true)
            let documentsDirectory = allPaths.first!
            let pathForExternalFolder = documentsDirectory.appending("/Audio/Alarms")
            let manager = FileManager.default
            //Default sounds
            let defaultSoundsPath = Bundle.main.resourcePath! + "/Audio/Alarms"
            do {
                let allItems = try manager.contentsOfDirectory(atPath: defaultSoundsPath)
                for item in allItems{
                    if item.count < 3{
                        break
                    }
                    let EndIndex = item.endIndex
                    let StartIndex = item.index(EndIndex, offsetBy: -3)
                    let range = Range(uncheckedBounds: (lower: StartIndex, upper: EndIndex))
                    if item[range] == "caf"{
                        alarmSounds.append(item)
                        defaultAlarmCount += 1
                    }
                }
            }catch{
                print("error")
            }
            
            // Custom Sounds
            do {
                let allItems = try manager.contentsOfDirectory(atPath: pathForExternalFolder)
                for item in allItems{
                    if item.count < 3{
                        break
                    }
                    let EndIndex = item.endIndex
                    let StartIndex = item.index(EndIndex, offsetBy: -3)
                    let range = Range(uncheckedBounds: (lower: StartIndex, upper: EndIndex))
                    if item[range] == "caf"{
                        alarmSounds.append(item)
                    }
                }
            }catch{
                print("error")
            }
        }
    
        func getToneFiles(){
            toneSounds.removeAll()
            let allPaths = NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true)
            let documentsDirectory = allPaths.first!
            let pathForExternalFolder = documentsDirectory.appending("/Audio/Tones")
            let manager = FileManager.default
            //Default sounds
            let defaultSoundsPath = Bundle.main.resourcePath! + "/Audio/Tones"
            do {
                let allItems = try manager.contentsOfDirectory(atPath: defaultSoundsPath)
                for item in allItems{
                    if item.count < 3{
                        break
                    }
                    let EndIndex = item.endIndex
                    let StartIndex = item.index(EndIndex, offsetBy: -3)
                    let range = Range(uncheckedBounds: (lower: StartIndex, upper: EndIndex))
                    if item[range] == "mp3"{
                        toneSounds.append(item)
                        defaultToneCount += 1
                    }
                }
            }catch{
                print("error")
            }
            //custom sounds
            do {
                let allItems = try manager.contentsOfDirectory(atPath: pathForExternalFolder)
                for item in allItems{
                    if item.count < 3{
                        break
                    }
                    let EndIndex = item.endIndex
                    let StartIndex = item.index(EndIndex, offsetBy: -3)
                    let range = Range(uncheckedBounds: (lower: StartIndex, upper: EndIndex))
                    if item[range] == "mp3"{
                        toneSounds.append(item)
                    }
                }
            }catch{
                print("error")
            }
        }
        
        // Logging state changed
        @objc func CT007LogStateChanged(_ switchState: UISwitch) {
            loggingEnabledSetting(on: CT007LoggingSwitch.isOn)
//            if CT007LoggingSwitch.isOn {
//                CT007LogLabel.text = "Logging is ON"
//                IsCT007LoggingEnabled = true
//                CT007LogIntTextField.isEnabled = true
//                CT007LogIntLabel.isEnabled = true
//                CT007LogIntTextField.textColor = UIColor.black
//                NotificationCenter.default.post(name: Notification.Name(rawValue: send_msg_to_console_NotificationKey), object: self)
//            }else{
//                CT007LogLabel.text = "Logging is OFF"
//                IsCT007LoggingEnabled = false
//                CT007LogIntTextField.isEnabled = false
//                CT007LogIntLabel.isEnabled = false
//                CT007LogIntTextField.textColor = UIColor.gray
//                NotificationCenter.default.post(name: Notification.Name(rawValue: send_msg_to_console_NotificationKey), object: self)
//            }
//            defaults.set(IsCT007LoggingEnabled, forKey: "Saved_IsCT007LoggingEnabled")
        }
        
        
        //------------------------------
        // Device Sound Change Functions
        //------------------------------
        @objc func appBeepSwitchStateChanged(_ switchState: UISwitch) {
            if appBeep {
                appBeep = false
                appBeepLabel.text = "Beeping is OFF"
            }else{
                appBeep = true
                appBeepLabel.text = "Beeping is ON"
            }
            defaults.set(appBeep, forKey: "Saved_DeviceBeep")
        }
        @objc func DeviceAlarmSwitchStateChanged(_ switchState: UISwitch) {
            if CT007DeviceAlarm {
                CT007DeviceAlarm = false
                CT007DeviceAlarmLabel.text = "Alarm is OFF"
            }else{
                CT007DeviceAlarm = true
                CT007DeviceAlarmLabel.text = "Alarm is ON"
            }
            defaults.set(CT007DeviceAlarm, forKey: "Saved_DeviceAlarm")
        }
        //-------------------------------------
        // End of Device Sound Change Functions
        //-------------------------------------

        //------------------------------
        // Device Sound Change Functions
        //------------------------------
        @objc func LiveGraphSwitch(_ switchState: UISwitch) {
            if !isLiveGraphEnabled {
                isLiveGraphEnabled = true
                LiveGraphLabel.text = "Live Graph is ON"
            } else {
                isLiveGraphEnabled = false
                LiveGraphLabel.text = "Live Graph is Off"
            }
            defaults.set(isLiveGraphEnabled, forKey: "Saved_LiveGraphEnabled")
        }
        //-------------------------------------
        // End of Device Sound Change Functions
        //-------------------------------------
        
        //--------------------------------
        // Detector Sound Change Functions
        //--------------------------------
        @objc func Detector_Beep_state_changed(_ switchState: UISwitch) {
            if CT007_Detector_BeepSwitch.isOn {
                Console_Setting_Text = "BUZZER OFF"
                NotificationCenter.default.post(name: Notification.Name(rawValue: send_msg_to_console_NotificationKey), object: self)
                //alarm off if beeping on
                if IsDetectorAlarmingEnabled {
                    // to run something in 0.1 seconds
                    
                    DispatchQueue.main.asyncAfter(deadline: .now() + 0.1) {
                        Console_Setting_Text = self.Alarm_OFF
                        self.CT007_Detector_AlarmLabel.text = "Alarming is OFF"
                        NotificationCenter.default.post(name: Notification.Name(rawValue: send_msg_to_console_NotificationKey), object: self)
                        self.CT007_Detector_AlarmSwitch.isOn = false
                        IsDetectorAlarmingEnabled = false
                    }
                }
                DispatchQueue.main.asyncAfter(deadline: .now() + 0.1) {
                Console_Setting_Text = self.Beep_ON
                self.CT007_Detector_BeepLabel.text = "Beeping is ON"
                IsDetectorBeepingEnabled = true
                NotificationCenter.default.post(name: Notification.Name(rawValue: send_msg_to_console_NotificationKey), object: self)
                }
            }else{
                Console_Setting_Text = Beep_OFF
                CT007_Detector_BeepLabel.text = "Beeping is OFF"
                IsDetectorBeepingEnabled = false
                NotificationCenter.default.post(name: Notification.Name(rawValue: send_msg_to_console_NotificationKey), object: self)
                DispatchQueue.main.asyncAfter(deadline: .now() + 0.1) {
                    Console_Setting_Text = "BUZZER OFF"
                    NotificationCenter.default.post(name: Notification.Name(rawValue: send_msg_to_console_NotificationKey), object: self)
                    NotificationCenter.default.post(name: Notification.Name(rawValue: send_msg_to_console_NotificationKey), object: self)
                    NotificationCenter.default.post(name: Notification.Name(rawValue: send_msg_to_console_NotificationKey), object: self)
                }
            }
            //defaults.set(IsCT007LoggingEnabled, forKey: "Saved_IsCT007LoggingEnabled")
        }
        @objc func Detector_Alarm_state_changed(_ switchState: UISwitch) {
            if CT007_Detector_AlarmSwitch.isOn {
                //beeping off if alarm on
                if IsDetectorBeepingEnabled {
                    DispatchQueue.main.asyncAfter(deadline: .now() + 0.1) {
                        Console_Setting_Text = self.Beep_OFF
                        self.CT007_Detector_BeepLabel.text = "Beeping is OFF"
                        NotificationCenter.default.post(name: Notification.Name(rawValue: send_msg_to_console_NotificationKey), object: self)
                        self.CT007_Detector_BeepSwitch.isOn = false
                        IsDetectorBeepingEnabled = false
                    }
                    DispatchQueue.main.asyncAfter(deadline: .now() + 0.1) {
                        Console_Setting_Text = "BUZZER OFF"
                        NotificationCenter.default.post(name: Notification.Name(rawValue: send_msg_to_console_NotificationKey), object: self)
                        NotificationCenter.default.post(name: Notification.Name(rawValue: send_msg_to_console_NotificationKey), object: self)
                        NotificationCenter.default.post(name: Notification.Name(rawValue: send_msg_to_console_NotificationKey), object: self)
                    }
                    DispatchQueue.main.asyncAfter(deadline: .now() + 0.1) {
                        Console_Setting_Text = "BUZZER OFF"
                        NotificationCenter.default.post(name: Notification.Name(rawValue: send_msg_to_console_NotificationKey), object: self)
                    }
                }
                DispatchQueue.main.asyncAfter(deadline: .now() + 0.1) {
                Console_Setting_Text = self.Alarm_ON
                self.CT007_Detector_AlarmLabel.text = "Alarming is ON"
                IsDetectorAlarmingEnabled = true
                NotificationCenter.default.post(name: Notification.Name(rawValue: send_msg_to_console_NotificationKey), object: self)
                }
            }else{
                Console_Setting_Text = Alarm_OFF
                CT007_Detector_AlarmLabel.text = "Alarming is OFF"
                IsDetectorAlarmingEnabled = false
                NotificationCenter.default.post(name: Notification.Name(rawValue: send_msg_to_console_NotificationKey), object: self)
                DispatchQueue.main.asyncAfter(deadline: .now() + 0.1) {
                    Console_Setting_Text = "BUZZER OFF"
                    NotificationCenter.default.post(name: Notification.Name(rawValue: send_msg_to_console_NotificationKey), object: self)
                    NotificationCenter.default.post(name: Notification.Name(rawValue: send_msg_to_console_NotificationKey), object: self)
                }
            }
            //defaults.set(IsCT007LoggingEnabled, forKey: "Saved_IsCT007LoggingEnabled")
        }
        //---------------------------------------
        // End of Detector Sound Change Functions
        //---------------------------------------
        
        
        //----------------------
        // Picker View Functions
        //----------------------
        // returns the number of 'columns' to display.
        public func numberOfComponents(in pickerView: UIPickerView) -> Int{
            return 1
        }
        // returns the # of rows in each component..
        public func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
            if pickerView.tag == 0{return CT007GammaUnitsOptions.count}
            else if pickerView.tag == 1 {return CT007ContamUnitsOptions.count}
            else if pickerView.tag == 2 {return CT007ScreensOptions.count}
            else if pickerView.tag == 3 {return CT007DetectorUnitOptions.count}
            else if pickerView.tag == 4 {return alarmSounds.count}
            else if pickerView.tag == 5 {return toneSounds.count}
            return 0
        }
        
        func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
            if pickerView.tag == 0{return CT007GammaUnitsOptions[row]}
            else if pickerView.tag == 1 {return CT007ContamUnitsOptions[row]}
            else if pickerView.tag == 2 {
                if CT007Screens[row] == 1{
                    return "\u{2713} \(CT007ScreensOptions[row])"
                }
                else{
                    return "\u{2715} \(CT007ScreensOptions[row])"
                }
            }
            else if pickerView.tag == 3 {
                return CT007DetectorUnitOptions[row]
            }
            else if pickerView.tag == 4 {
                let fileName = alarmSounds[row]
                let EndIndex = fileName.index(fileName.endIndex, offsetBy: -4)
                let range = Range(uncheckedBounds: (lower: fileName.startIndex, upper: EndIndex))
                let title = fileName[range]
                return String(title)
            }
            else if pickerView.tag == 5 {
                let fileName = toneSounds[row]
                let EndIndex = fileName.index(fileName.endIndex, offsetBy: -4)
                let range = Range(uncheckedBounds: (lower: fileName.startIndex, upper: EndIndex))
                let title = fileName[range]
                return String(title)
            }
            return ""
        }
        func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
            if pickerView.tag == 0{
                CT007GammaUnitsTextField.text = CT007GammaUnitsOptions[row]
                CT007_GammaUnits = CT007GammaUnitsOptions[row]
                //saving the selected units
                defaults.set(CT007_GammaUnits, forKey: "Saved_CT007_GammaUnits")
            }
            else if pickerView.tag == 1{
                CT007ContamUnitsTextField.text = CT007ContamUnitsOptions[row]
                CT007_ContamUnits = CT007ContamUnitsOptions[row]
                //saving the selected units
                defaults.set(CT007_ContamUnits, forKey: "Saved_CT007_ContamUnits")
            }
            else if pickerView.tag == 2{
                if selectFromTap{
                    if CT007Screens[row] == 1{
                        //turn off screen
                        CT007Screens[row] = 0
                        Console_Setting_Text = "X("+String(row+1)+")="+String(0)
                        NotificationCenter.default.post(name: Notification.Name(rawValue: send_msg_to_console_NotificationKey), object: self)
                        pickerView.reloadComponent(0)
                    }
                    else{
                        // turn on screen
                        CT007Screens[row] = 1
                        Console_Setting_Text = "X("+String(row+1)+")="+String(1)
                        NotificationCenter.default.post(name: Notification.Name(rawValue: send_msg_to_console_NotificationKey), object: self)
                        pickerView.reloadComponent(0)
                    }
                    var screenSet = false
                    var firstEnabledRow = 1
                    for screenEnabled in CT007Screens{
                        if screenEnabled == 1 && !screenSet{
                            Console_Setting_Text = "T(20)="+String(firstEnabledRow)
                            NotificationCenter.default.post(name: Notification.Name(rawValue: send_msg_to_console_NotificationKey), object: self)
                            Console_Setting_Text = "T(1)="+String(firstEnabledRow)
                            NotificationCenter.default.post(name: Notification.Name(rawValue: send_msg_to_console_NotificationKey), object: self)
                            screenSet = true
                        }
                        firstEnabledRow += 1
                    }
                    if !screenSet{
                        Console_Setting_Text = "T(20)=10"
                        NotificationCenter.default.post(name: Notification.Name(rawValue: send_msg_to_console_NotificationKey), object: self)
                        Console_Setting_Text = "T(1)=10"
                        NotificationCenter.default.post(name: Notification.Name(rawValue: send_msg_to_console_NotificationKey), object: self)
                    }
                    selectFromTap = false
                }
            }
            else if pickerView.tag == 3{
                CT007DeviceUnitsTextField.text = CT007DetectorUnitOptions[row]
                CT007Units = row + 1
                Console_Setting_Text = "T(36)="+String(CT007Units)
                NotificationCenter.default.post(name: Notification.Name(rawValue: send_msg_to_console_NotificationKey), object: self)
            }
            else if pickerView.tag == 4{
                // alarm sounds
                let fileName = alarmSounds[row]
                if (row+1) > defaultAlarmCount{
                    let allPaths = NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true)
                    let documentsDirectory = allPaths.first!
                    let pathForExternalFolder = documentsDirectory.appending("/Audio/Alarms/")
                    let path = pathForExternalFolder.appending(fileName)
                    print("Selected path: \(path)")
                    AudioSingleton.sharedInstance.alarmPath = path
                }else{
                    let folderPath = Bundle.main.resourcePath! + "/Audio/Alarms/"
                    let path = folderPath.appending(fileName)
                    print("Selected path: \(path)")
                    AudioSingleton.sharedInstance.alarmPath = path
                }
            }
            else if pickerView.tag == 5{
                // tone sounds
                let fileName = toneSounds[row]
                if (row+1) > defaultToneCount{
                    let allPaths = NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true)
                    let documentsDirectory = allPaths.first!
                    let pathForExternalFolder = documentsDirectory.appending("/Audio/Tones/")
                    let path = pathForExternalFolder.appending(fileName)
                    AudioSingleton.sharedInstance.tonePath = path
                }else{
                    let folderPath = Bundle.main.resourcePath! + "/Audio/Tones/"
                    let path = folderPath.appending(fileName)
                    AudioSingleton.sharedInstance.tonePath = path
                }
            }
        }
        
        func gestureRecognizer(_ gestureRecognizer: UIGestureRecognizer, shouldRecognizeSimultaneouslyWith otherGestureRecognizer: UIGestureRecognizer) -> Bool {
            return true
        }
        
        @objc func pickerTapped(tapRecognizer: UITapGestureRecognizer) {
            if tapRecognizer.state == .ended {
                let rowHeight = self.CT007ScreensOptionsPicker.rowSize(forComponent: 0).height
                let selectedRowFrame = self.CT007ScreensOptionsPicker.bounds.insetBy(dx: 0, dy: (self.CT007ScreensOptionsPicker.frame.height - rowHeight) / 2)
                let userTappedOnSelectedRow = selectedRowFrame.contains(tapRecognizer.location(in: self.CT007ScreensOptionsPicker))
//                var testrow = 0
                
//                let selectedRow = self.CT007ScreensOptionsPicker.selectedRow(inComponent: 0)
                
//                CT007ScreensOptionsPicker.selectRow(selectedRow, inComponent: 0, animated: true)
//                pickerView(self.CT007ScreensOptionsPicker, didSelectRow: selectedRow, inComponent: 0)
                
                selectFromTap = true
                if userTappedOnSelectedRow {
                    let selectedRow = self.CT007ScreensOptionsPicker.selectedRow(inComponent: 0)
                    pickerView(self.CT007ScreensOptionsPicker, didSelectRow: selectedRow, inComponent: 0)
                }
                
//                if CT007Screens[selectedRow] == 1{
//                    //turn off screen
//                    CT007Screens[selectedRow] = 0
//                    Console_Setting_Text = "X("+String(selectedRow+1)+")="+String(0)
//                    NotificationCenter.default.post(name: Notification.Name(rawValue: send_msg_to_console_NotificationKey), object: self)
//                    CT007ScreensOptionsPicker.reloadComponent(0)
//                }
//                else{
//                    // turn on screen
//                    CT007Screens[selectedRow] = 1
//                    Console_Setting_Text = "X("+String(selectedRow+1)+")="+String(1)
//                    NotificationCenter.default.post(name: Notification.Name(rawValue: send_msg_to_console_NotificationKey), object: self)
//                    CT007ScreensOptionsPicker.reloadComponent(0)
//                }
//                var screenSet = false
//                var firstEnabledRow = 1
//                for screenEnabled in CT007Screens{
//                    if screenEnabled == 1{
//                        Console_Setting_Text = "T(20)="+String(firstEnabledRow)
//                        NotificationCenter.default.post(name: Notification.Name(rawValue: send_msg_to_console_NotificationKey), object: self)
//                        Console_Setting_Text = "T(1)="+String(firstEnabledRow)
//                        NotificationCenter.default.post(name: Notification.Name(rawValue: send_msg_to_console_NotificationKey), object: self)
//                        screenSet = true
//                    }
//                    firstEnabledRow += 1
//                }
//                if !screenSet{
//                    Console_Setting_Text = "T(20)=10"
//                    NotificationCenter.default.post(name: Notification.Name(rawValue: send_msg_to_console_NotificationKey), object: self)
//                    Console_Setting_Text = "T(1)=10"
//                    NotificationCenter.default.post(name: Notification.Name(rawValue: send_msg_to_console_NotificationKey), object: self)
//                }
            }
        }
        //-----------------------------
        // End of Picker View Functions
        //-----------------------------
        
        
        //MARK: UITextFieldDelegate
        func textFieldShouldReturn(_ textField: UITextField) -> Bool {
            // Hide the keyboard.
            textField.resignFirstResponder()
            print ("when comming here 01")
            return true
        }
        
        
        //--------------------------------------
        // Function handling all textfield edits
        //--------------------------------------
        func textFieldDidEndEditing(_ textField: UITextField) {
            switch (textField.tag) {
            case 0:
                // this is for th text field which is handled in pickerview
                break;
            case 1:
                print ("CT007 log interval text filed")
                // do something with this text field
                let Entered_Interval = Int(textField.text!)
                if Entered_Interval != nil {
                    CT007LogInterval = Int(textField.text!)!
                    defaults.set(CT007LogInterval, forKey: "Saved_CT007LogInterval")
                    print ("CT007LogInterval is : " , CT007LogInterval)
                } else {
                    print ("enetered value is not an integer")
                }
                
                break;
            case 2:
                print ("CT007 Response Time text filed")
                // do something with this text field
                let Entered_Interval = Int(textField.text!)
                if Entered_Interval != nil{
                    let time = Entered_Interval!
                    if time >= 0 && time <= maxResponseTime{
                        CT007ResponseTime = Int(textField.text!)!
                        if CT007DeviceResponseCapable{
                            if time == 0{
                                Console_Setting_Text = "Y=30"
                                NotificationCenter.default.post(name: Notification.Name(rawValue: send_msg_to_console_NotificationKey), object: self)
                            }
                            Console_Setting_Text = "T(37)="+String(time)
                            NotificationCenter.default.post(name: Notification.Name(rawValue: send_msg_to_console_NotificationKey), object: self)
                        }else{
                            print("Device has no response variable")
                        }
                    }else{
                        textField.text = String(CT007ResponseTime)
                    }
                } else {
                    print ("enetered value is not an integer")
                }
            case 3:
                print ("CT007 Minimum Counts text filed")
                // do something with this text field
                let Entered_Interval = Int(textField.text!)
                if Entered_Interval != nil{
                    let counts = Entered_Interval!
                    if counts > 0 && counts <= 1000000{
//                        CT007MinimumCountsLabel.text = "Minimum Counts | %Error: %"+String(Double(counts).squareRoot())
                        CT007MinimumCounts = Int(textField.text!)!
                        defaults.set(CT007MinimumCounts, forKey: "Saved_MinimumCounts")
                    }else{
                        textField.text = String(CT007MinimumCounts)
                    }
                } else {
                    print ("enetered value is not an integer")
                }
            case 4:
                print ("CT007 Yellow Level Gamma text filed")
                // do something with this text field
                let Entered_Interval = Double(textField.text!)
                if Entered_Interval != nil{
                    if Entered_Interval! < CT007RedLevelGamma{
                        CT007YellowLevelGamma = Double(textField.text!)!
                        defaults.set(CT007YellowLevelGamma, forKey: "Saved_YellowLevelGamma")
                    }else{
                        textField.text = String(CT007YellowLevelGamma)
                    }
                } else {
                    print ("enetered value is not an Double")
                }
            case 5:
                print ("CT007 Red Level Gamma text filed")
                // do something with this text field
                let Entered_Interval = Double(textField.text!)
                if Entered_Interval != nil{
                    if Entered_Interval! > CT007YellowLevelGamma && Entered_Interval! < CT007MaxLevelGamma{
                        CT007RedLevelGamma = Double(textField.text!)!
                        defaults.set(CT007RedLevelGamma, forKey: "Saved_RedLevelGamma")
                    }else{
                        textField.text = String(CT007RedLevelGamma)
                    }
                } else {
                    print ("enetered value is not an double")
                }
            case 6:
                print ("CT007 Max Level Gamma text filed")
                // do something with this text field
                let Entered_Interval = Double(textField.text!)
                if Entered_Interval != nil{
                    if Entered_Interval! > CT007RedLevelGamma{
                        CT007MaxLevelGamma = Double(textField.text!)!
                        defaults.set(CT007MaxLevelGamma, forKey: "Saved_MaxLevelGamma")
                    }else{
                        textField.text = String(CT007MaxLevelGamma)
                    }
                } else {
                    print ("enetered value is not an double")
                }
            case 7:
                    print ("CT007 Yellow Level Contamination text filed")
                    // do something with this text field
                    let Entered_Interval = Double(textField.text!)
                    if Entered_Interval != nil{
                        if Entered_Interval! < CT007RedLevelContam{
                            CT007YellowLevelContam = Double(textField.text!)!
                            defaults.set(CT007YellowLevelContam, forKey: "Saved_YellowLevelContamination")
                        }else{
                            textField.text = String(CT007YellowLevelContam)
                        }
                    } else {
                        print ("enetered value is not an Double")
                }
            case 8:
                print ("CT007 Red Level Contamination text filed")
                // do something with this text field
                let Entered_Interval = Double(textField.text!)
                if Entered_Interval != nil{
                    if Entered_Interval! > CT007YellowLevelContam && Entered_Interval! < CT007MaxLevelContam{
                        CT007RedLevelContam = Double(textField.text!)!
                        defaults.set(CT007RedLevelContam, forKey: "Saved_RedLevelContamination")
                    }else{
                        textField.text = String(CT007RedLevelContam)
                    }
                } else {
                    print ("enetered value is not an double")
                }
            case 9:
                print ("CT007 Max Level Contamination text filed")
                // do something with this text field
                let Entered_Interval = Double(textField.text!)
                if Entered_Interval != nil{
                    if Entered_Interval! > CT007RedLevelContam{
                        CT007MaxLevelContam = Double(textField.text!)!
                        defaults.set(CT007MaxLevelContam, forKey: "Saved_MaxLevelContamination")
                    }else{
                        textField.text = String(CT007MaxLevelContam)
                    }
                } else {
                    print ("enetered value is not an double")
                }
            case 10:
                print ("CT007 Point Interval text filed")
                // do something with this text field
                let Entered_Interval = Int(textField.text!)
                if Entered_Interval != nil{
                    if Entered_Interval! > 0{
                        graphPointDuration = Int(textField.text!)!
                        defaults.set(graphPointDuration, forKey: "Saved_graphPointDuration")
                        // Save and clear data set for new dataset
                        if Connected && isLiveGraphEnabled{
                            CTData.saveGraph()
                        }
                        CTData.currentSet = nil
                        CTData.totalCT007Counts = 0
                        CTData.totalTime = 0
                        CTData.recordedPoints = 0
                        CTData.over30Points = false
                    }else{
                        textField.text = String(graphPointDuration)
                    }
                } else {
                    print ("enetered value is not an double")
                }
            case 11:
                print ("CT007 Dead Time text filed")
                // do something with this text field
                let Entered_Interval = Int(textField.text!)
                if Entered_Interval != nil{
                    let time = Entered_Interval!
                    if time >= 0 && time <= 1000000000{
                        CT007DeadTime = Int(textField.text!)!
                        if CT007DeviceDeadTimeCapable{
                            Console_Setting_Text = "W="+String(time)
                            NotificationCenter.default.post(name: Notification.Name(rawValue: send_msg_to_console_NotificationKey), object: self)
                        }else{
                            print("Device has no response variable")
                        }
                    }else{
                        textField.text = String(CT007DeadTime)
                    }
                } else {
                    print ("enetered value is not an integer")
                }
            case 12:
                print ("CT007 Counts to Beep text filed")
                // do something with this text field
                let Entered_Interval = Int(textField.text!)
                if Entered_Interval != nil{
                    let time = Entered_Interval!
                    if time >= 1 && time <= 10000{
                        CT007CountsToBeep = Int(textField.text!)!
                        defaults.set(CT007CountsToBeep, forKey: "Saved_countsToBeep")
                    }else{
                        textField.text = String(CT007CountsToBeep)
                    }
                } else {
                    print ("enetered value is not an integer")
                }
            case 13:
                print ("CT007 Alarm Level Filed")
                // do something with this text field
                let Entered_Interval = Int(textField.text!)
                if Entered_Interval != nil{
                    let level = Entered_Interval!
                    if level >= 1 && level <= 10000{
                        CT007AlarmLevel = Int(textField.text!)!
                        //Send command
                        Console_Setting_Text = "F="+String(CT007AlarmLevel)
                        NotificationCenter.default.post(name: Notification.Name(rawValue: send_msg_to_console_NotificationKey), object: self)
                    }else{
                        textField.text = String(CT007AlarmLevel)
                    }
                } else {
                    print ("enetered value is not a Double")
                }
            case 14:
                print ("CT007 Conversion Factor Filed")
                // do something with this text field
                let Entered_Interval = Int(textField.text!)
                if Entered_Interval != nil{
                    let factor = Entered_Interval!
                    if factor >= 1 && factor <= 1000000{
                        let vValue: Int = Int(textField.text!)!
                        textField.text = String(vValue)
                        let dValue: Int = 166667/vValue
                        CTData.C_to_uSv_Conversion = Double(dValue) * Double(10 ^^ (-7))
                        CTData.CPS_to_uSvPerHour_Conversion = CTData.C_to_uSv_Conversion * 3600
                        currentConversionFactor = CTData.CPS_to_uSvPerHour_Conversion
                        //Send command
                        Console_Setting_Text = "V="+String(vValue)
                        NotificationCenter.default.post(name: Notification.Name(rawValue: send_msg_to_console_NotificationKey), object: self)
                        Console_Setting_Text = "D="+String(dValue)
                        NotificationCenter.default.post(name: Notification.Name(rawValue: send_msg_to_console_NotificationKey), object: self)
                    }else{
                        textField.text = String(Int(166667/(currentConversionFactor/(3.6 * 10 ^^ -4))))
                    }
                } else {
                    print ("enetered value is not a Double")
                }
            default:
                print ("default text filed")
                break;
                // remainder of switch statement....
            }
        }
        //---------------------------------------------
        // End of Function handling all textfield edits
        //---------------------------------------------
    
        @IBAction func EmailButtonPressed(_ sender: Any) {
            if( MFMailComposeViewController.canSendMail() ) {
                print("Can send email.")
                
                let mailComposer = MFMailComposeViewController()
                mailComposer.mailComposeDelegate = self
                
                //Set the subject and message of the email
                mailComposer.setSubject("GammaGuard Log File")
//                mailComposer.setMessageBody("", isHTML: false)
                
                
                let allPaths = NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true)
                let documentsDirectory = allPaths.first!
                let pathForExternalFolder = documentsDirectory.appending("/External")
                let pathForLog = pathForExternalFolder.appending("/DataLog-ExtDetector.csv")
                if let fileData = NSData(contentsOfFile: pathForLog) {
                    print("File data loaded.")
                    mailComposer.addAttachmentData(fileData as Data, mimeType: "text/csv", fileName: "DataLog-ExtDetector.csv")
                    self.present(mailComposer, animated: true, completion: nil)
                }else{
                    let alertController = UIAlertController(title: "Alert", message: "There was no saved log file.", preferredStyle: .alert)
                    alertController.addAction(UIAlertAction(title: NSLocalizedString("OK", comment: "Default action"), style: .`default`, handler: { _ in
                        NSLog("The \"OK\" alert occured.")
                    }))
                    self.present(alertController, animated: true, completion: nil)
                }
            }else{
                let alertController = UIAlertController(title: "Alert", message: "In order to send an email you must sign into an account on the Mail app.", preferredStyle: .alert)
                alertController.addAction(UIAlertAction(title: NSLocalizedString("OK", comment: "Default action"), style: .`default`, handler: { _ in
                    NSLog("The \"OK\" alert occured.")
                }))
                self.present(alertController, animated: true, completion: nil)
            }
        }
        func mailComposeController(_ controller: MFMailComposeViewController, didFinishWith result: MFMailComposeResult, error: Error?) {
            // Dismiss the mail compose view controller.
            controller.dismiss(animated: true, completion: nil)
        }
    
    @IBAction func AlarmSoundPreview(_ sender: Any) {
    }
    
    @IBAction func ToneSoundPreview(_ sender: Any) {
    }
    
    @IBAction func resetToDefault(_ sender: Any) {
        /*
         ----------------
        Client Only Settings
         ----------------
         */
        // Gamma Units Def: uSv/h
        
        // Contam Units Def: CPM
        
        // Live Graph Def: Disabled
        
        // Point Interval Def: 5 Sec
        
        // Data Logging Def: Disabled
        
        // Data Logging Interval Def: 300 Sec
        
        // Gamma Yellow Warning Level Def: 5.0
        
        // Gamma Red Warning Level Def: 250000.0
        
        // Gamma Full Warning Level Def: 1000000.0
        
        // Contam Yellow Warning Level Def: 300.0
        
        // Contam Red Warning Level Def: 100000.0
        
        // Contam Full Warning Level Def: 5000000.0
        
        // GG Alarm Def: Enabled
        
        // GG Counts Beeping Def: Enabled
        
        // GG Counts to Beep Ratio Def: 1
        
        /*
         ----------------
         Detector Settings
         ----------------
         */
        
        // Detector Alarm Def: Enabled
        
        // Detector Beeping Def: Disabled
        
        // Detector Alarm Level Def: 50.0
        
        // Detector Response Time Def: 0
        
        // Detector Minimum Counts Def: 100
        
        // Detector Dead Time Def: 66667
        
        // Detector Conversion Factor Def: 1111
        
        // Detector Units Def: 1->uSv 2->CPM
        
        // Detector Screens Def: All Enabled
        
    }
    
    
}



