//
//  PeripheralConnectedViewController+BT.swift
//  GammaGuard
//
//  Created by EIC on 2018-10-26.
//  Copyright © 2018 EIC. All rights reserved.
//

import Foundation
import CoreBluetooth
import UIKit


extension PeripheralConnectedViewController: CBPeripheralDelegate {
    func centralManager(_ central: CBCentralManager, didFailToConnectPeripheral peripheral: CBPeripheral, error: NSError?) {
        print ("connected view 006")
        print("Error connecting peripheral: \(String(describing: error?.localizedDescription))")
        print("213 Failed to connect call")
    }
    func centralManager(_ central: CBCentralManager, didConnect peripheral: CBPeripheral) {
        print("213 DID RECONNECT",peripheral)
        Singleton.sharedInstance.centralManager?.stopScan()
        // Wait 4 seconds incase device is still booting up
//        MillisecondsTimer = 0
        showConnectingAlert()
        if workItem != nil{
            workItem?.cancel()
            workItem = nil
        }
        workItem = DispatchWorkItem { self.connectAfterStable(peripheral: peripheral) }
        DispatchQueue.main.asyncAfter(deadline: .now() + 5, execute: workItem!)
//        if !connectionQueueAdded{
//            print("999 1")
//            consistentConnection = true
//            DispatchQueue.main.asyncAfter(deadline: .now() + 5) {
//                if self.consistentConnection{
//                    print("999 3")
//                    peripheral.discoverServices(nil)
//                    self.readAfterConnect()
//                    // Restart the display refresh timer
//                    if !(rssiReloadTimer?.isValid)!{
//                        rssiReloadTimer = nil
//                        rssiReloadTimer = Timer.scheduledTimer(timeInterval: 1.0, target: self, selector: #selector(PeripheralConnectedViewController.refreshRSSI), userInfo: nil, repeats: true)
//                        RunLoop.current.add(rssiReloadTimer!, forMode: RunLoop.Mode.common)
//                    }
//                }
//                //bad fix preferably remove chunk from queue
//                else{
//                    print("999 4")
//                    Singleton.sharedInstance.centralManager?.cancelPeripheralConnection(peripheral)
//                }
//                self.connectionQueueAdded = false
//            }
//        }else{
//            print("999 2")
//        }
//        connectionQueueAdded = true
    }
    
    func connectAfterStable(peripheral: CBPeripheral){
        print("999 3")
        peripheral.discoverServices(nil)
        self.readAfterConnect()
        // Restart the display refresh timer
        if !(rssiReloadTimer?.isValid)!{
            rssiReloadTimer = nil
            rssiReloadTimer = Timer.scheduledTimer(timeInterval: 1.0, target: self, selector: #selector(PeripheralConnectedViewController.refreshRSSI), userInfo: nil, repeats: true)
            RunLoop.current.add(rssiReloadTimer!, forMode: RunLoop.Mode.common)
        }
    }
    
//    @objc func checkServices(){
//        MillisecondsTimer = 0
//        print("213 attempt to discover services")
//        self.peripheral?.discoverServices(nil)
//        if self.peripheral?.services?.count == nil{
//            if serviceTimer != nil {
//                serviceTimer?.invalidate()
//                serviceTimer = nil
//                print("Successfully reconnected and discovered services")
//                if disconnectAlertVisible{
//                    print("987 disconnected dismissed")
//                    disconnectAlertController?.dismiss(animated: true, completion: nil)
//                    disconnectAlertVisible = false
//                }
//                connectingAlertVisible = true
//                connectingAlertController = UIAlertController(title: "Connected", message: "The connection was found, reconnecting to device...", preferredStyle: .alert)
//                if var topController = UIApplication.shared.keyWindow?.rootViewController {
//                    while let presentedViewController = topController.presentedViewController {
//                        topController = presentedViewController
//                    }
//                    topController = topController.presentingViewController!
//                    print("987 connecting presented")
//                    topController.present(connectingAlertController!, animated: true, completion: nil)
//                    print("test9123 presenting Conncting alert")
//                }
//                _ = Timer.scheduledTimer(timeInterval: 1, target: self, selector: #selector(readAfterConnect), userInfo: nil, repeats: false)
//            }
//        }
//    }
    
    @objc func readAfterConnect(){
        //Rescan for settings incase they changed
        CTData.graphCounts = 0
        CTData.graphDurationCounter = 0
        MillisecondsTimer = 0
        if SentMsg != ""{
            print("Possibly would have crashed")
        }
        SentMsg = ""
        if consoleTimer == nil{
            consoleTimer = Timer.scheduledTimer(timeInterval: 0.5,
                                                target: self,
                                                selector: #selector(PeripheralConnectedViewController.GetConsoleData),
                                                userInfo: nil,
                                                repeats: true)
        }
    }
    
    func peripheral(_ peripheral: CBPeripheral, didDiscoverServices error: Error?){
        print ("connected view 006-001")
        if error != nil{
            print("213 didDiscoverServices",error!)
        }else{
            print("213 didDiscoverServices")
        }
//        if !(rssiReloadTimer?.isValid)!{
//            rssiReloadTimer = nil
//            rssiReloadTimer = Timer.scheduledTimer(timeInterval: 1.0, target: self, selector: #selector(PeripheralConnectedViewController.refreshRSSI), userInfo: nil, repeats: true)
//            RunLoop.current.add(rssiReloadTimer!, forMode: RunLoop.Mode.common)
//        }
        for service in peripheral.services! {
            let thisService = service as CBService
            if CT007.isoadService(thisService) {
                // Discover characteristics of all valid services
                //  self.statusLabel.text = "Discovered battery service"
                print ("Discovered oad Service")
                peripheral.discoverCharacteristics(nil, for: thisService)
            }
            if CT007.isCommService(thisService) {
                // Discover characteristics of all valid services
                //  self.statusLabel.text = "Discovered battery service"
                //                let MyComService = thisService
                print ("Discovered Comm service")
                peripheral.discoverCharacteristics(nil, for: thisService)
            }
            if CT007.isBatteryService(thisService) {
                // Discover characteristics of all valid services
                //  self.statusLabel.text = "Discovered battery service"
                print ("Discovered battery service")
                peripheral.discoverCharacteristics(nil, for: thisService)
            }
            if CT007.isRadiationService(thisService) {
                // Discover characteristics of all valid services
                //    self.statusLabel.text = "Discovered radiation service"
                print ("Discovered radiation service")
                peripheral.discoverCharacteristics(nil, for: thisService)
            }
            if CT007.isKeyFobService(thisService){
                print ("Discovered KeyFob service")
                peripheral.discoverCharacteristics(nil, for: thisService)
                keyFobConnected = true
                CT007Mode = "Contamination"
            }
            if CT007.isKeyFobService1(thisService){
                print ("Discovered KeyFob1 service")
                peripheral.discoverCharacteristics(nil, for: thisService)
                keyFobConnected = true
                CT007Mode = "Contamination"
            }
            if CT007.isKeyFobService2(thisService){
                print ("Discovered KeyFob2 service")
                peripheral.discoverCharacteristics(nil, for: thisService)
                keyFobConnected = true
                CT007Mode = "Contamination"
            }
        }
        if error != nil {
            print("Error discovering services: \(String(describing: error?.localizedDescription))")
        }
        services.removeAll()
        ids.removeAll()
        peripheral.services?.forEach({ (service) in
            services.append(service)
            ids.append(service.uuid)
            //        tableView.reloadData()
            peripheral.discoverCharacteristics(nil, for: service)
            print("213 services",services)
        })
    }
    func peripheral(_ peripheral: CBPeripheral, didDiscoverCharacteristicsFor service: CBService, error: Error?) {
        for charateristic in service.characteristics! {
            let thisCharacteristic = charateristic as CBCharacteristic
            if CT007.isInputCharacteristic(thisCharacteristic) {
                print ("Input charac notify 014")
                self.peripheral?.setNotifyValue(true, for: thisCharacteristic)
            }
            if CT007.isOutputCharacteristic(thisCharacteristic) {
                MyOutPutChar = thisCharacteristic
                print ("Output charac write 014")
                //                if let buf = command.data(using: String.Encoding.ascii, allowLossyConversion: false) {
                //                    write(buf, characteristic: thisCharacteristic, type: .withResponse)
                //                    print ("---------- 014 ---------thisCharacteristic:  " ,thisCharacteristic)
                //                }
            }
            if CT007.isImgIdentityCharacteristic(thisCharacteristic) {
                print ("Img Identity charac read ,  \(thisCharacteristic)")
                self.peripheral?.readValue(for: thisCharacteristic)
            }
            if CT007.isimgBlockCharacteristic(thisCharacteristic) {
                print ("img block charac read  ,  \(thisCharacteristic)")
                self.peripheral?.readValue(for: thisCharacteristic)
            }
            if CT007.isRadiationCharacteristic(thisCharacteristic) {
                print ("radiation charac notify ,  \(thisCharacteristic)")
                self.peripheral?.setNotifyValue(true, for: thisCharacteristic)
            }
            if CT007.isConversionCharacteristic(thisCharacteristic) {
                print ("conversion charac read ,  \(thisCharacteristic)")
                self.peripheral?.readValue(for: thisCharacteristic)
            }
            if CT007.isBatteryCharacteristic(thisCharacteristic) {
                print ("battery charac read ,  \(thisCharacteristic)")
                self.peripheral?.readValue(for: thisCharacteristic)
//                self.peripheral?.setNotifyValue(true, for: thisCharacteristic)
            }
            if CT007.isKF1Characteristic(thisCharacteristic) {
                print ("kf1 charac ,  \(thisCharacteristic)")
                self.peripheral?.setNotifyValue(true, for: thisCharacteristic)
//                self.peripheral?.readValue(for: thisCharacteristic)
            }
            if CT007.isKF2Characteristic(thisCharacteristic) {
                print ("kf2char ,  \(thisCharacteristic)")
//                self.peripheral?.setNotifyValue(true, for: thisCharacteristic)
                self.peripheral?.readValue(for: thisCharacteristic)
            }
        }
        if error != nil {
            print("Error discovering service characteristics: \(String(describing: error?.localizedDescription))")
        }
        service.characteristics?.forEach({ (characteristic) in
            print("charact: -->  \(String(describing: characteristic.descriptors))---\(characteristic.properties)")
        })
    }
    
    
    // MARK: Get data values when they are updated
    func peripheral(_ peripheral: CBPeripheral, didUpdateValueFor characteristic: CBCharacteristic, error: Error?) {
        //        self.statusLabel.text = "Connected"
        if characteristic.uuid == batteryLevel {
            if let BatteryCharVal = characteristic.value {
                var bytes = Array(repeating: 0 as UInt8, count:BatteryCharVal.count/MemoryLayout<UInt8>.size)
                //print (bytes)
                BatteryCharVal.copyBytes(to: &bytes, count:BatteryCharVal.count)
                let data16 = bytes.map { UInt8($0) }
                let data = Data(bytes: data16)
                BatteryLevel_Characteristics_Value = UInt32(littleEndian: data.withUnsafeBytes { $0.pointee })
                print ("BatteryLevel_Characteristics_Value:  " ,BatteryLevel_Characteristics_Value)
            }
            BatteryLevel_Characteristics_Value_Int = Int(BatteryLevel_Characteristics_Value)
        }
        
        if characteristic.uuid == Conversion_Factor {
            if let ConversionFactorCharVal = characteristic.value {
                var bytes = Array(repeating: 0 as UInt8, count:ConversionFactorCharVal.count/MemoryLayout<UInt8>.size)
                //print (bytes)
                ConversionFactorCharVal.copyBytes(to: &bytes, count:ConversionFactorCharVal.count)
                let data16 = bytes.map { UInt8($0) }
                let data = Data(bytes: data16)
                ConversionFactor_Characteristics_Value = UInt32(littleEndian: data.withUnsafeBytes { $0.pointee })
                print ("writepending - ConversionFactor_Characteristics_Value:  " ,ConversionFactor_Characteristics_Value)
                ConversionFactor_Characteristics_Value_Int = Int(ConversionFactor_Characteristics_Value)
                CTData.C_to_uSv_Conversion = Double(ConversionFactor_Characteristics_Value_Int) * Double(10 ^^ (-7))
                CTData.CPS_to_uSvPerHour_Conversion = CTData.C_to_uSv_Conversion * 3600
                print ("writepending - C_to_uSv_Conversion:  ", CTData.C_to_uSv_Conversion)
                print ("writepending - CPS_to_uSvPerHour_Conversion:  ", CTData.CPS_to_uSvPerHour_Conversion)
            }
        }
        if characteristic.uuid == radiationCount {
            if let radiationCount = characteristic.value {
                var bytes = Array(repeating: 0 as UInt8, count:radiationCount.count/MemoryLayout<UInt8>.size)
                //print (bytes)
                radiationCount.copyBytes(to: &bytes, count:radiationCount.count)
                let data16 = bytes.map { UInt8($0) }
                let data = Data(bytes: data16)
                Radiation_Characteristics_Value = UInt32(littleEndian: data.withUnsafeBytes { $0.pointee })
            }
            if appBeep{
                Tones(counts: Int(Radiation_Characteristics_Value))
            }
            CTData.totalCT007Counts += Double(Radiation_Characteristics_Value)
            Radiation_Characteristics_Value_Int += Int(Radiation_Characteristics_Value)
            if timerRun{
                timerTotal += Int(Radiation_Characteristics_Value)
            }
            print ("External_Radiation_Characteristics_Value :  "  , Radiation_Characteristics_Value_Int)
            
            packetCount += 1
        }
        if characteristic.uuid == keyFobChar1 {
            if let keyFobChar1 = characteristic.value {
                var bytes = Array(repeating: 0 as UInt8, count:keyFobChar1.count/MemoryLayout<UInt8>.size)
                keyFobChar1.copyBytes(to: &bytes, count:keyFobChar1.count)
                let data16 = bytes.map { UInt8($0) }
                let data = Data(bytes: data16)
                Radiation_Characteristics_Value = UInt32(littleEndian: data.withUnsafeBytes { $0.pointee })
            }
            if appBeep{
                Tones(counts: Int(Radiation_Characteristics_Value))
            }
            CTData.totalCT007Counts += Double(Radiation_Characteristics_Value)
            Radiation_Characteristics_Value_Int += Int(Radiation_Characteristics_Value)
            if timerRun{
                timerTotal += Int(Radiation_Characteristics_Value)
            }
            print ("KeyFob Counts :  "  , Radiation_Characteristics_Value_Int)
            
            packetCount += 1
        }
        if characteristic.uuid == keyFobChar2 {
            if let BatteryCharVal = characteristic.value {
                var bytes = Array(repeating: 0 as UInt8, count:BatteryCharVal.count/MemoryLayout<UInt8>.size)
                //print (bytes)
                BatteryCharVal.copyBytes(to: &bytes, count:BatteryCharVal.count)
                let data16 = bytes.map { UInt8($0) }
                let data = Data(bytes: data16)
                BatteryLevel_Characteristics_Value = UInt32(littleEndian: data.withUnsafeBytes { $0.pointee })
                print ("BatteryLevel_Characteristics_Value:  " ,BatteryLevel_Characteristics_Value)
            }
            BatteryLevel_Characteristics_Value_Int = Int(BatteryLevel_Characteristics_Value)
        }
        
        // recieves and saves data from detector
        if characteristic.uuid == inputCharacteristicUUID {
            if let str1 = characteristic.value {
                let str2 = NSString(data: str1, encoding: String.Encoding.ascii.rawValue)!
                let str3 = str2 as String
                var textArray: [String]  = []
                for line in str3.components(separatedBy: "\n") {
                    if !line.hasPrefix("//") {
                        textArray.append(line)
                    }
                    print ("line : ", textArray)
                }
                
                let str = textArray.joined(separator: "\n")
                for ch in str {
                    writepending.append(ch)
                    if ch == "\n" {
                        //print ("writepending init =  ", writepending)
                        DeviceRespond = writepending
                        Console_Setting_Text_Respond = DeviceRespond
                        writepending = ""
                        if DeviceRespond == "OK\n" {
                            //print ("writepending is OK: --> ", DeviceRespond)
                            print ("Respond AAA 001 is OK: --> ", Console_Setting_Text_Respond)
                        } else {
                            print("989 \(MillisecondsTimer)")
                            if SentMsg == command_RESPOND_TEST{
                                print ("Device Response was: ", DeviceRespond)
                                if DeviceRespond != "TEST\n"{
                                    MillisecondsTimer = 0
                                    print("ERROR RESTARTING DEVICE SETTINGS")
                                }else{
                                    print("device check passed")
                                }
                                SentMsg = ""
                            }
                            else if SentMsg == command_PRINT_V {
                                print ("writepending = conversion factor is : ", DeviceRespond)
                                GOT_CONVERSION = DeviceRespond
                                let GOT_CONVERSION_truncated = GOT_CONVERSION[..<GOT_CONVERSION.index(before: GOT_CONVERSION.endIndex)]
                                if (Int(GOT_CONVERSION_truncated) != 0) {
                                    print("989 \(GOT_CONVERSION_truncated)")
                                    //                                    print ("writepending-" , Int(GOT_CONVERSION_truncated)!)
                                    if Int(GOT_CONVERSION_truncated) != nil {
                                        print ("writepending-" , Int(GOT_CONVERSION_truncated)!)
                                        ConversionFactor_Characteristics_Value_Int = Int(GOT_CONVERSION_truncated)!
                                        CT007V = ConversionFactor_Characteristics_Value_Int
                                        let dValue:Int = 166667/CT007V
                                        ConversionFactorLabel.text = "getting Conversion Factor--->  \(dValue)"
                                        CTData.C_to_uSv_Conversion = Double(dValue) * Double(10 ^^ (-7))
                                        CTData.CPS_to_uSvPerHour_Conversion = CTData.C_to_uSv_Conversion * 3600
                                        currentConversionFactor = CTData.CPS_to_uSvPerHour_Conversion
                                        print("TESTING: ", currentConversionFactor)
                                        print ("writepending = C_to_uSv_Conversion is : ", CTData.C_to_uSv_Conversion)
                                        print ("writepending = CPS_to_uSvPerHour_Conversion is : ", CTData.CPS_to_uSvPerHour_Conversion)
                                    }
                                }
                                SentMsg = ""
                            }
                            else if SentMsg == command_PRINT_VERSION {
                                print ("writepending = Device Basic Version is  : ", DeviceRespond)
                                GOT_BASIC_VERSION = DeviceRespond
                                BasicFWversionLabel.text = "Getting Basic Version--->  \(GOT_BASIC_VERSION)"
                                
                                SentMsg = ""
                            }
                            else if SentMsg == command_PRINT_DEVICE_ID {
                                print ("writepending = Device ID is  : ", DeviceRespond)
                                GOT_DEVICE_ID = DeviceRespond
                                print("978 "+GOT_DEVICE_ID)
                                if let details = detailsDict[Int(String(DeviceRespond.filter { !" \n".contains($0) }))!]{
                                    CT007DetectorDetails = details
                                }
                                UnitIDLabel.text = "getting Device Unit ID--->  \(GOT_DEVICE_ID)"
                                SentMsg = ""
                            }
                            else if SentMsg == command_PRINT_Beep {
                                print ("writepending = Device beep state is  : ", DeviceRespond)
                                GOT_BEEP_STATE = DeviceRespond
                                let GOT_BEEP_STATE_truncated = GOT_BEEP_STATE[..<GOT_BEEP_STATE.index(before: GOT_BEEP_STATE.endIndex)]
                                
                                if (Int(GOT_BEEP_STATE_truncated) == 1) {
                                    IsDetectorBeepingEnabled = true
                                }else{
                                    IsDetectorBeepingEnabled = false
                                }
                                //UnitIDLabel.text = "getting Device Unit ID--->  \(GOT_DEVICE_ID)"
                                SentMsg = ""
                            }
                            else if SentMsg == command_PRINT_RTime {
                                print ("writepending = Device Response Time is  : ", DeviceRespond)
                                GOT_RTime = DeviceRespond
                                //Save value
                                let GOT_RTime_truncated = GOT_RTime[..<GOT_RTime.index(before: GOT_RTime.endIndex)]
                                //                                CT007ResponseTime = Int(GOT_RTime_truncated)!
                                if let responseInt = Int(GOT_RTime_truncated){
                                    CT007ResponseTime = responseInt
                                    CT007DeviceResponseCapable = true
                                }else{
                                    CT007DeviceResponseCapable = false
                                    print("Could not read response time")
                                }
                                //UnitIDLabel.text = "getting Device Unit ID--->  \(GOT_DEVICE_ID)"
                                SentMsg = ""
                            }
                            else if SentMsg == command_PRINT_DTime {
                                print ("writepending = Device Dead Time is  : ", DeviceRespond)
                                GOT_DTime = DeviceRespond
                                //Save value
                                let GOT_DTime_truncated = GOT_DTime[..<GOT_DTime.index(before: GOT_DTime.endIndex)]
                                if let deadTimeInt = Int(GOT_DTime_truncated){
                                    CT007DeadTime = deadTimeInt
                                    CT007DeviceDeadTimeCapable = true
                                }else{
                                    CT007DeviceDeadTimeCapable = false
                                    print("Could not read dead time")
                                }
                                //UnitIDLabel.text = "getting Device Unit ID--->  \(GOT_DEVICE_ID)"
                                SentMsg = ""
                            }
                            else if SentMsg == command_PRINT_Alarm {
                                print ("writepending = Device alarm state is  : ", DeviceRespond)
                                GOT_ALARM_STATE = DeviceRespond
                                let GOT_ALARM_STATE_truncated = GOT_ALARM_STATE[..<GOT_ALARM_STATE.index(before: GOT_ALARM_STATE.endIndex)]
                                if (Int(GOT_ALARM_STATE_truncated) == 1) {
                                    IsDetectorAlarmingEnabled = true
                                }else{
                                    IsDetectorAlarmingEnabled = false
                                }
                                //UnitIDLabel.text = "getting Device Unit ID--->  \(GOT_DEVICE_ID)"
                                SentMsg = ""
                            }
                            else if SentMsg == command_PRINT_Alarm_Level {
                                print ("writepending = Device alarm level is  : ", DeviceRespond)
                                GOT_ALARM_LEVEL = DeviceRespond
                                let GOT_ALARM_LEVEL_truncated = GOT_ALARM_LEVEL[..<GOT_ALARM_LEVEL.index(before: GOT_ALARM_LEVEL.endIndex)]
                                CT007AlarmLevel = Int(GOT_ALARM_LEVEL_truncated)!
                                //UnitIDLabel.text = "getting Device Unit ID--->  \(GOT_DEVICE_ID)"
                                SentMsg = ""
                            }
                            else if SentMsg == command_PRINT_SCREENS {
                                print ("writepending = Device screens state is  : ", DeviceRespond)
                                GOT_Screens = DeviceRespond
                                let GOT_Screens_truncated = GOT_Screens[..<GOT_Screens.index(before: GOT_Screens.endIndex)]
                                var startIndex = 0
                                for i in GOT_Screens_truncated{
                                    CT007Screens[startIndex] = Int(String(i))!
                                    startIndex += 1
                                }
                                print("Screens:",CT007Screens)
                                //UnitIDLabel.text = "getting Device Unit ID--->  \(GOT_DEVICE_ID)"
                                SentMsg = ""
                            }
                            else if SentMsg == command_PRINT_UNITS {
                                print ("writepending = Device units state is  : ", DeviceRespond)
                                GOT_Units = DeviceRespond
                                let GOT_Units_truncated = GOT_Units[..<GOT_Units.index(before: GOT_Units.endIndex)]
                                CT007Units = Int(GOT_Units_truncated)!
                                //UnitIDLabel.text = "getting Device Unit ID--->  \(GOT_DEVICE_ID)"
                                SentMsg = ""
                            }
                            else if SentMsg == command_LIST {
                                if DeviceRespond.hasPrefix("3630"){
                                    if let match = DeviceRespond.range(of:"(?<=\")[^\"]+", options: .regularExpression) {
                                        DeviceRespond = String(DeviceRespond[match])
                                        //print(DeviceRespond.substring(with: match))
                                        GOT_DEVICE_NAME = DeviceRespond
                                        DetectorNameLabel.text = "getting Detector Name--->  \(GOT_DEVICE_NAME)"
                                        Detector_NAME = GOT_DEVICE_NAME
                                        print ("writepending = Device NAME is  : ", DeviceRespond)
                                    }
                                }
                                SentMsg = ""
                            }
                        }
                    }
                }
                print ("DeviceRespond: " , DeviceRespond)
            } else {
                print ("---------- writepending --------- CANT DO IT")
            }
        }else{
            // Maybe bad
            SentMsg = ""
        }
        print ("DeviceRespond: " , DeviceRespond)
    }
    
    
    //    func peripheral(_ peripheral: CBPeripheral, didWriteValueFor characteristic: CBCharacteristic, error: Error?) {
    //        print ("..........Device.didWriteValueFor......... (014)", characteristic)
    //        //delegate?.onWriteComplete(error == nil, uuid: characteristic.uuid)
    //        //onWriteComplete(error == nil, uuid: characteristic.uuid)
    //
    //    }
    //
    
    
    func write(_ data: Data, characteristic: CBCharacteristic, type: CBCharacteristicWriteType) {
        print ("..........Device.writing....014..... (001): ", characteristic)
        peripheral?.writeValue(data, for: characteristic, type: type)
    }
    
    
    func peripheral(_ peripheral: CBPeripheral, didReadRSSI RSSI: NSNumber, error: Error?) {
        //print ("connected view 007")
        IntRSSI = Int(truncating: RSSI)
        print ("RSSI:  \(RSSI.intValue)")
        switch RSSI.intValue {
        case -90 ... -60:
            rssiLabel.textColor = UIColor.bluetoothOrangeColor()
            break
        case -200 ... -90:
            rssiLabel.textColor = UIColor.bluetoothRedColor()
            break
        default:
            rssiLabel.textColor = UIColor.bluetoothGreenColor()
        }
        RSSIGLOBAL = "\(RSSI)dB"
        rssiLabel.text = "\(RSSI)dB"
        BatteryLabel.text = "Battery \(BatteryLevel_Characteristics_Value_Int) %"
    }
    
}



extension PeripheralConnectedViewController: CBCentralManagerDelegate {
    func centralManagerDidUpdateState(_ central: CBCentralManager) {
    }
    func centralManager(_ central: CBCentralManager, didDiscover peripheral: CBPeripheral, advertisementData: [String : Any], rssi RSSI: NSNumber) {
        if self.peripheral == peripheral{
            print("Found previous peripheral")
            Singleton.sharedInstance.centralManager?.connect(self.peripheral!, options: nil)
        }
    }
    func centralManager(_ central: CBCentralManager, didFailToConnect peripheral: CBPeripheral, error: Error?) {
        print("213 Failed")
    }
    func centralManager(_ central: CBCentralManager, didDisconnectPeripheral peripheral: CBPeripheral, error: Error?) {
        print("213 DISCONNECTED")
        if workItem != nil{
            workItem!.cancel()
        }
//        print("213 Trying to reconnect")
//
//        DispatchQueue.main.asyncAfter(deadline: .now() + 2) {
//            //Start here
//            if !disconnectAlertVisible{
//                self.timeToReconnect = self.reconnectionTime
//            }
//            self.disconnectFunc()
//            //        print("213 State:",self.peripheral!)
//            if self.reconTimer == nil{
//                self.reconTimer = Timer.scheduledTimer(timeInterval: TimeInterval(self.reconnectionTime),
//                                                       target: self,
//                                                       selector: #selector(self.checkConnection),
//                                                       userInfo: nil, repeats: true)
//                self.reconTimer?.fire()
//            }
//        }
        MillisecondsTimer = 0
        showDisconnectAlert()
        consistentConnection = false
        DispatchQueue.main.asyncAfter(deadline: .now() + 3){
            Singleton.sharedInstance.centralManager?.scanForPeripherals(withServices: self.ids, options: nil)
        }
//        Singleton.sharedInstance.centralManager?.scanForPeripherals(withServices: self.ids, options: nil)
        if consoleTimer != nil {
            consoleTimer?.invalidate()
            consoleTimer = nil
        }
    }
    
    @objc func checkConnection(){
        print("213 Check connection")
        print("333 1")
        timeToReconnect = reconnectionTime
        Singleton.sharedInstance.centralManager?.connect(peripheral!, options: nil)
        _ = Timer.scheduledTimer(timeInterval: TimeInterval(reconnectAttemptTime), target: self,  selector: #selector(cancelConnectionAttempt), userInfo: nil, repeats: false)
    }
    
    @objc func cancelConnectionAttempt(){
        print("333 2")
        if peripheral?.state != CBPeripheralState.connected{
            print("213 Cancel Attempt")
            Singleton.sharedInstance.centralManager?.cancelPeripheralConnection(peripheral!)
        }else{
            if reconTimer != nil {
                reconTimer?.invalidate()
                reconTimer = nil
            }
        }
    }
    
}

