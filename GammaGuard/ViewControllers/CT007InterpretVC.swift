//
//  CT007InterpretVC.swift
//  UITableVC
//
//  Created by sepehr khatir on 2017-03-30.
//  Copyright © 2017 EIC. All rights reserved.
//

import UIKit

class CT007InterpretVC: BaseViewController {
    
    //------------------------
    // IBOutlets and Variables
    //------------------------
    @IBOutlet weak var rssiLabel: UILabel!
    @IBOutlet weak var Status: UILabel!
    @IBOutlet weak var BatteryLabel: UILabel!
    @IBOutlet weak var DisplayDoseRateShortLabel: UILabel!
    @IBOutlet weak var DoseRateShortUnitsLabel: UILabel!
    @IBOutlet weak var DisplayTotalDoseLabel: UILabel!
    @IBOutlet weak var TotalDoseUnitsLabel: UILabel!
    @IBOutlet weak var CT007LogButtonLabel: UIButton!
    @IBOutlet weak var CT007RateLabel: UILabel!
    @IBOutlet weak var CT007TotalLabel: UILabel!
    @IBOutlet weak var MessageLabel: UILabel!
    @IBOutlet weak var DisconnectButton: UIButton!
    @IBOutlet weak var DeviceNameLabel: UILabel!
    @IBOutlet weak var DateLabel: UILabel!
    @IBOutlet weak var UnitsLabel: UILabel!
    
    var contaminationCapable = false
    var contaminationWarning = 0
    var stopColorChange = false
    //-------------------------------
    // End of IBOutlets and Variables
    //-------------------------------
    
    
    //--------------------------
    // View Controller Functions
    //--------------------------
    override func viewDidLoad() {
        super.viewDidLoad()
        addSlideMenuButton()
        if !keyFobConnected{
            addModeButton()
        }
        DeviceNameLabel.text = Detector_NAME
        if IsCT007LoggingEnabled {
            CT007LogButtonLabel.isEnabled = true
        } else {
            CT007LogButtonLabel.isEnabled = false
            CT007LogButtonLabel.setTitleColor(UIColor.gray, for: .disabled)
        }
        //load detector details
        if CT007DetectorDetails.count > 0{
            contaminationCapable = CT007DetectorDetails[8] as! Bool
            contaminationWarning = CT007DetectorDetails[7] as! Int
        }
    }
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(true)
        print (".......... CT interpret notification observer removed ...........")
        NotificationCenter.default.removeObserver(self)
        if view.subviews.last!.tag == 23{
            sideMenuOpen = false
            view.subviews.last!.removeFromSuperview()
        }
    }
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        NotificationCenter.default.addObserver(self,
                                               selector: #selector(doSomethingAfterNotifiedCT007),
                                               name: NSNotification.Name(rawValue: myCT007NotificationKey),
                                               object: nil)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        stopColorChange = false
        doSomethingAfterNotifiedCT007()
        //load mode settings
        if CT007Mode == "Gamma"{
            self.navigationItem.titleView = doseModeLabel()
            loadGammaSettings()
        }else if CT007Mode == "Contamination"{
            self.navigationItem.titleView = countModeLabel()
            loadContaminationSettings()
        }else{
            self.navigationItem.titleView = invalidModeLabel()
        }
        colorChange()
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(true)
        stopColorChange = true
    }
    //---------------------------------
    // End of View Controller Functions
    //---------------------------------
    
    
    //-----------------------
    // Mode Related functions
    //-----------------------
    func loadGammaSettings(){
        self.navigationItem.titleView = doseModeLabel()
        CT007Mode = "Gamma"
        CT007RateLabel.text = "Dose Rate"
        CT007TotalLabel.text = "Total Dose"
        defaults.set(CT007Mode, forKey: "Saved_Mode")
    }
    func loadContaminationSettings(){
        self.navigationItem.titleView = countModeLabel()
        CT007Mode = "Contamination"
        CT007RateLabel.text = "Count Rate"
        CT007TotalLabel.text = "Total Counts"
        defaults.set(CT007Mode, forKey: "Saved_Mode")
    }
    func addModeButton(){
        let btnMode = UIButton(type: UIButton.ButtonType.system)
        btnMode.setImage(self.defaultModeImage(), for: UIControl.State())
        btnMode.frame = CGRect(x: 0, y: 0, width: 60, height: 30)
        btnMode.addTarget(self, action: #selector(ModeButtonPressed), for: UIControl.Event.touchUpInside)
        let customBarItem = UIBarButtonItem(customView: btnMode)
        self.navigationItem.rightBarButtonItem = customBarItem;
    }
    @objc func ModeButtonPressed(){
        doSomethingAfterNotifiedCT007()
        if CT007Mode == "Gamma"{
            print("loading contamination settings")
            loadContaminationSettings()
        }else if CT007Mode == "Contamination"{
            print("loading gamma settings")
            loadGammaSettings()
        }else{
            print("Invalid Mode")
        }
    }
    //------------------------------
    // End of Mode Related functions
    //------------------------------
    

    // Nav Bar warning color
    func colorChange(){
        if !stopColorChange{
            if CT007Mode == "Gamma"{
                switch Display_DoseRate_Short_Units_Label {
                case "µSv/h":
                    unitFactor = 1
                case "mSv/h":
                    unitFactor = 1/1000
                case "mRem/h":
                    unitFactor = 1/10
                default:
                    print("invalid units")
                }
            }else{
                unitFactor = 1
            }
            let navBar = navigationController?.navigationBar
            let tabBar = tabBarController?.tabBar
            if (Double(Display_DoseRate_Short_Label)! < CT007CurrentYellowLevel*unitFactor){
                navBar?.barTintColor = UIColor.warningGreenColor()
                tabBar?.barTintColor = UIColor.warningGreenColor()
            }
            else if (Double(Display_DoseRate_Short_Label)! >= CT007CurrentYellowLevel*unitFactor) && (Double(Display_DoseRate_Short_Label)! < CT007CurrentRedLevel*unitFactor) {
                navBar?.barTintColor = UIColor.warningYellowColor()
                tabBar?.barTintColor = UIColor.warningYellowColor()
            }else {
                if navBar?.barTintColor != UIColor.warningRedColor() && CT007DeviceAlarm{
                    playAlarm = true
                }
                navBar?.barTintColor = UIColor.warningRedColor()
                tabBar?.barTintColor = UIColor.warningRedColor()
            }
        }
    }
    
    
    // Creates message to display on interpret view
    func createMessage() -> String{
        var message: String = ""
        if CT007Mode == "Gamma"{
            message = "At \(Double(Display_DoseRate_Short_Label)!/unitFactor) µSv/h you can be here for \(String(format: "%.2f",500/(Double(Display_DoseRate_Short_Label)!/unitFactor))) hours before exceeding the routine dose limit of 500 µSv and \(String(format: "%.2f",250000/(Double(Display_DoseRate_Short_Label)!/unitFactor))) hours before exceeding the emergency dose limit of 250,000 µSv. You have reached \(Int((Double(Display_TotalDose_Label)!)/(5*unitFactor)))% of the routine dose limit and \(Int(Double(Display_TotalDose_Label)!/(2500*unitFactor)))% of the emergency dose limit"
        }else{
            if contaminationCapable{
                var warning: String = "is less than"
                if Double(Display_DoseRate_Short_Label)! >= Double(contaminationWarning){
                    warning = "exceeds"
                }
                message = "\(Display_DoseRate_Short_Label) CPM \(warning) the hot zone level of \(contaminationWarning) CPM"
            }else{
                message = "The connected detector is not capable of measuring contamination"
            }
        }
        return message
    }
    
    
    // Called every second
    @objc func doSomethingAfterNotifiedCT007 (){
        //MARK: connection status .....
        Status.text = ConnectionState
        switch IntRSSI {
        case -90 ... -60:
            rssiLabel.textColor = UIColor.bluetoothOrangeColor()
            break
        case -200 ... -90:
            rssiLabel.textColor = UIColor.bluetoothRedColor()
            break
        default:
            rssiLabel.textColor = UIColor.bluetoothGreenColor()
        }
        rssiLabel.text = RSSIGLOBAL
        BatteryLabel.text = "Battery \(BatteryLevel_Characteristics_Value_Int) %"
        DisplayDoseRateShortLabel.text = Display_DoseRate_Short_Label
        DoseRateShortUnitsLabel.text = Display_DoseRate_Short_Units_Label
        DisplayTotalDoseLabel.text = Display_TotalDose_Label
        UnitsLabel.text = Display_DoseRate_Short_Units_Label
        TotalDoseUnitsLabel.text = Display_TotalDose_Units_Label
        MessageLabel.text = createMessage()
        colorChange()
        let date = Date()
        let dayTimePeriodFormatter = DateFormatter()
        dayTimePeriodFormatter.dateFormat = "MMM d, yyyy"
        DateLabel.text = dayTimePeriodFormatter.string(from: date)
        print ("noti interpret: " , Display_TotalDose_Label)
    }
    

    @IBAction func disconnectButtonPressed(_ sender: Any) {
        PeripheralConnectedViewController().disconnectButtonPressed()
    }
    
    @IBAction func LogCT007DataPressed(_ sender: Any) {
        PeripheralConnectedViewController().LogExternalData()
    }
    
}
