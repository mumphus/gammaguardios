//
//  CT007detailVC.swift
//  UITableVC
//
//  Created by sepehr khatir on 2017-03-30.
//  Copyright © 2017 EIC. All rights reserved.
//

import UIKit

var timerTotal: Int = 0
var timerRun = false

class CT007detailVC: BaseViewController, UITextFieldDelegate {

    //------------------------
    // IBOutlets and Variables
    //------------------------
    @IBOutlet weak var rssiLabel: UILabel!
    @IBOutlet weak var Status: UILabel!
    @IBOutlet weak var BatteryLabel: UILabel!
    @IBOutlet weak var DisplayDoseRateShortLabel: UILabel!
    @IBOutlet weak var DoseRateShortUnitsLabel: UILabel!
    @IBOutlet weak var DisplayCountsLabel: UILabel!
    @IBOutlet weak var DisplayTotalDoseLabel: UILabel!
    @IBOutlet weak var DisplayTotalTimeLabel: UILabel!
    @IBOutlet weak var TotalDoseUnitsLabel: UILabel!
    @IBOutlet weak var CT007LogButtonLabel: UIButton!
    @IBOutlet weak var OverallDoseRateLabel: UILabel!
    @IBOutlet weak var OverallDoseRateUnitsLabel: UILabel!
    @IBOutlet weak var DoseRateTitle: UILabel!
    @IBOutlet weak var TotalDoseTitle: UILabel!
    @IBOutlet weak var OverallDoseRateTitlel: UILabel!
    @IBOutlet weak var RunTimeTextField: UITextField!
    @IBOutlet weak var StartButton: UIButton!
    @IBOutlet weak var TimerDoseRateLabel: UILabel!
    @IBOutlet weak var TimerDoseRateUnitsLabel: UILabel!
    @IBOutlet weak var TimerTotalDoseLabel: UILabel!
    @IBOutlet weak var TimerTotalRunDoseUnitsLabel: UILabel!
    @IBOutlet weak var TimerDoseRateTitle: UILabel!
    @IBOutlet weak var TimerTotalDoseTitle: UILabel!
    @IBOutlet weak var DisconnectButton: UIButton!
    @IBOutlet weak var DeviceNameLabel: UILabel!
    @IBOutlet weak var DateLabel: UILabel!
    @IBOutlet weak var UnitsLabel: UILabel!
    
    var stopColorChange = false
    var hour = 0
    var min = 0
    var sec = 0
//    var timerTotal: Int = 0
    var timerSeconds = 0
    var resetTime = ""
    //-------------------------------
    // End of IBOutlets and Variables
    //-------------------------------

    
    //--------------------------
    // View Controller Functions
    //--------------------------
    override func viewDidLoad() {
        super.viewDidLoad()
        RunTimeTextField.delegate = self
        addSlideMenuButton()
        if !keyFobConnected{
            addModeButton()
        }
        DeviceNameLabel.text = Detector_NAME
        if IsCT007LoggingEnabled {
            CT007LogButtonLabel.isEnabled = true
        } else {
            CT007LogButtonLabel.isEnabled = false
            CT007LogButtonLabel.setTitleColor(UIColor.gray, for: .disabled)
        }
        if countdownTimer != nil{
            countdownTimer?.invalidate()
        }
    }
    
    
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(true)
        print (".......... CT detail notification observer removed ...........")
        NotificationCenter.default.removeObserver(self)
        if view.subviews.last!.tag == 23{
            sideMenuOpen = false
            view.subviews.last!.removeFromSuperview()
        }
    }
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        stopColorChange = false
        doSomethingAfterNotifiedCT007()
        //load mode settings
        if CT007Mode == "Gamma"{
            self.navigationItem.titleView = doseModeLabel()
            loadGammaSettings()
        }else if CT007Mode == "Contamination"{
            self.navigationItem.titleView = countModeLabel()
            loadContaminationSettings()
        }else{
            self.navigationItem.titleView = invalidModeLabel()
        }
        colorChange()
        NotificationCenter.default.addObserver(self,
                                               selector: #selector(doSomethingAfterNotifiedCT007),
                                               name: NSNotification.Name(rawValue: myCT007NotificationKey),
                                               object: nil)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(true)
        stopColorChange = true
    }
    //---------------------------------
    // End of View Controller Functions
    //---------------------------------
    
    
    
    //---------------------------------
    // Text Field Functions
    //---------------------------------
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        setupTextFieldsAccessoryView(textField: textField)
        return true
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool{
        let newString = NSString(string: textField.text!).replacingCharacters(in: range, with: string)
        if newString.count > 9{
            return false
        }
        if string.count == 0{
            let removedChars = NSString(string: textField.text!).substring(with: range)
            if removedChars.contains(":"){
                return false
            }
            print("TextField Edit |","Removed:","[\(removedChars)]")
        }else{
            if Int(string) == nil{
                return false
            }
            print("TextField Edit |","Added:","[\(string)]")
        }
        print("TextField Edit |","Original:",textField.text!,"Modified:",newString)
        return true
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        readTimes()
    }
    
    func readTimes(){
        let text = RunTimeTextField.text
        var splitter = text?.split(separator: ":")
        if splitter?.count != 3{
            RunTimeTextField.text = "00:00:00"
            readTimes()
            return
        }
        hour = Int(splitter![0])!
        min = Int(splitter![1])!
        sec = Int(splitter![2])!
        if sec >= 60{
            let extraMin = sec/60
            let remain = sec%60
            min += extraMin
            sec = remain
        }
        if min >= 60{
            let extraHour = min/60
            let remain = min%60
            hour += extraHour
            min = remain
        }
    }
    
    //-----------------------------------
    // Adding "Done" button to numberpads
    //-----------------------------------
    func setupTextFieldsAccessoryView(textField: UITextField) {
        guard textField.inputAccessoryView == nil else {
            print("textfields accessory view already set up")
            return
        }
        let toolBar: UIToolbar = UIToolbar(frame:CGRect(x: 0, y: 0, width: UIScreen.main.bounds.size.width, height: 44))
        toolBar.barStyle = UIBarStyle.default
        toolBar.isTranslucent = true
        let flexsibleSpace: UIBarButtonItem = UIBarButtonItem(barButtonSystemItem: UIBarButtonItem.SystemItem.flexibleSpace, target: nil, action: nil)
        let doneButton: UIBarButtonItem = UIBarButtonItem(barButtonSystemItem: UIBarButtonItem.SystemItem.done, target: self, action: #selector(didPressDoneButton))
        toolBar.items = [flexsibleSpace, doneButton]
        textField.inputAccessoryView = toolBar
    }
    @objc func didPressDoneButton(button: UIButton, textField: UITextField) {
        // Hide keyboard
        RunTimeTextField.resignFirstResponder()
    }
    //---------------------------------
    // End of Text Field Functions
    //---------------------------------
    
    //-----------------------
    // Mode Related functions
    //-----------------------
    func loadGammaSettings(){
        self.navigationItem.titleView = doseModeLabel()
        CT007Mode = "Gamma"
        DoseRateTitle.text = "Dose Rate"
        TotalDoseTitle.text = "Total Dose"
        TimerDoseRateTitle.text = "Run Dose Rate"
        TimerTotalDoseTitle.text = "Run Total Dose"
        OverallDoseRateTitlel.text = "Overall Dose Rate"
        defaults.set(CT007Mode, forKey: "Saved_Mode")
    }
    func loadContaminationSettings(){
        self.navigationItem.titleView = countModeLabel()
        CT007Mode = "Contamination"
        DoseRateTitle.text = "Count Rate"
        TotalDoseTitle.text = "Total Counts"
        TimerDoseRateTitle.text = "Run Count Rate"
        TimerTotalDoseTitle.text = "Run Total Counts"
        OverallDoseRateTitlel.text = "Overall Count Rate"
        defaults.set(CT007Mode, forKey: "Saved_Mode")
    }
    func addModeButton(){
        let btnMode = UIButton(type: UIButton.ButtonType.system)
        btnMode.setImage(self.defaultModeImage(), for: UIControl.State())
        btnMode.frame = CGRect(x: 0, y: 0, width: 60, height: 30)
        btnMode.addTarget(self, action: #selector(ModeButtonPressed), for: UIControl.Event.touchUpInside)
        let customBarItem = UIBarButtonItem(customView: btnMode)
        self.navigationItem.rightBarButtonItem = customBarItem;
    }
    @objc func ModeButtonPressed(){
        doSomethingAfterNotifiedCT007()
        if CT007Mode == "Gamma"{
            print("loading contamination settings")
            loadContaminationSettings()
        }else if CT007Mode == "Contamination"{
            print("loading gamma settings")
            loadGammaSettings()
        }else{
            print("Invalid Mode")
        }
    }
    //------------------------------
    // End of Mode Related functions
    //------------------------------
    
    
    //Nav bar Warning color
    func colorChange(){
        if !stopColorChange{
            if CT007Mode == "Gamma"{
                switch Display_DoseRate_Short_Units_Label {
                case "µSv/h":
                    unitFactor = 1
                case "mSv/h":
                    unitFactor = 1/1000
                case "mRem/h":
                    unitFactor = 1/10
                default:
                    print("invalid units")
                }
            }else{
                unitFactor = 1
            }
            let navBar = navigationController?.navigationBar
            let tabBar = tabBarController?.tabBar
            if (Double(Display_DoseRate_Short_Label)! < CT007CurrentYellowLevel*unitFactor){
                navBar?.barTintColor = UIColor.warningGreenColor()
                tabBar?.barTintColor = UIColor.warningGreenColor()
            }
            else if (Double(Display_DoseRate_Short_Label)! >= CT007CurrentYellowLevel*unitFactor) && (Double(Display_DoseRate_Short_Label)! < CT007CurrentRedLevel*unitFactor) {
                navBar?.barTintColor = UIColor.warningYellowColor()
                tabBar?.barTintColor = UIColor.warningYellowColor()
            }else {
                if navBar?.barTintColor != UIColor.warningYellowColor() && CT007DeviceAlarm{
                    playAlarm = true
                }
                navBar?.barTintColor = UIColor.warningRedColor()
                tabBar?.barTintColor = UIColor.warningRedColor()
            }
        }
    }
    
    @objc func timerLogic(){
        print("Timer Tick:",timerSeconds)
        sec -= 1
        if sec < 0{
            min -= 1
            if min < 0{
                hour -= 1
                if hour < 0{
                    //TIMER ENDED
                    RunTimeTextField.text = resetTime
                    readTimes()
                    if countdownTimer != nil{
                        countdownTimer?.invalidate()
                    }
                    StartButton.setTitle("Start", for: .normal)
                    StartButton.backgroundColor = UIColor.groupTableViewBackground
                    timerRun = false
                    RunTimeTextField.isEnabled = true
                    return
                }
                min = 59
            }
            sec = 59
        }
        // Tick
//        let counts = CTData.totalAcumulatedCT007Counts
        timerSeconds += 1
//        timerTotal += Radiation_Characteristics_Value_Int
        timerTextUpdate()
    }
    
    func timerTextUpdate(){
        let minZero = min < 10 ? "0" : ""
        let secZero = sec < 10 ? "0" : ""
        RunTimeTextField.text = "\(hour):\(minZero)\(min):\(secZero)\(sec)"
    }
    
    
    // Called every second
    @objc func doSomethingAfterNotifiedCT007 (){
        //MARK: connection status .....
        Status.text = ConnectionState
        switch IntRSSI {
        case -90 ... -60:
            rssiLabel.textColor = UIColor.bluetoothOrangeColor()
            break
        case -200 ... -90:
            rssiLabel.textColor = UIColor.bluetoothRedColor()
            break
        default:
            rssiLabel.textColor = UIColor.bluetoothGreenColor()
        }
        
        rssiLabel.text = RSSIGLOBAL
        BatteryLabel.text = "Battery \(BatteryLevel_Characteristics_Value_Int) %"
        UnitsLabel.text = Display_DoseRate_Short_Units_Label
        DisplayCountsLabel.text = String(Radiation_Characteristics_Value_Int)
        DisplayDoseRateShortLabel.text = Display_DoseRate_Short_Label
        DoseRateShortUnitsLabel.text = Display_DoseRate_Short_Units_Label
        DisplayTotalTimeLabel.text = Display_TotalTime_Label
        DisplayTotalDoseLabel.text = Display_TotalDose_Label
        TotalDoseUnitsLabel.text = Display_TotalDose_Units_Label
        OverallDoseRateLabel.text = Display_OverallDoseRate_Label
        OverallDoseRateUnitsLabel.text = Display_DoseRate_Short_Units_Label
        TimerDoseRateUnitsLabel.text = Display_DoseRate_Short_Units_Label
        TimerTotalRunDoseUnitsLabel.text = Display_TotalDose_Units_Label
        
        var rate: Double = 0
        var totalDose: Double = 0
        print("test:",timerTotal,"/",timerSeconds)
        if CT007Mode == "Gamma"{
            var unitMultiplier: Double = 1
            switch CT007_GammaUnits{
            case "µSv/h":
                unitMultiplier = 1
            case "mSv/h":
                unitMultiplier = 1/1000
            case "mRem/h":
                unitMultiplier = 1/10
            default:
                unitMultiplier = 1
            }
            
            rate = ((Double(timerTotal) / Double(timerSeconds))*CTData.CPS_to_uSvPerHour_Conversion) * unitMultiplier
            totalDose = ((Double(timerTotal)*CTData.CPS_to_uSvPerHour_Conversion)/3600) * unitMultiplier
            TimerTotalDoseLabel.text = String(format: "%.3f",totalDose)
        }else{
            var unitMultiplier: Double = 1
            if CT007_ContamUnits == "CPM"{
                unitMultiplier = 60
            }
            rate = (Double(timerTotal) / Double(timerSeconds))*unitMultiplier
            TimerTotalDoseLabel.text = String(format: "%.0f",Double(timerTotal))
        }
        if rate.isNaN{
            rate = 0
        }
        TimerDoseRateLabel.text = String(format: "%.3f",rate)
        
        colorChange()
        let date = Date()
        let dayTimePeriodFormatter = DateFormatter()
        dayTimePeriodFormatter.dateFormat = "MMM d, yyyy"
        DateLabel.text = dayTimePeriodFormatter.string(from: date)
        print ("noti detail: " , Display_TotalDose_Label)
    }
    
    @IBAction func startTimerPressed(_ sender: Any) {
        if timerRun{
            countdownTimer?.invalidate()
            StartButton.setTitle("Start", for: .normal)
            StartButton.backgroundColor = UIColor.groupTableViewBackground
            timerRun = false
            RunTimeTextField.text = resetTime
            readTimes()
            RunTimeTextField.isEnabled = true
            return
        }
        timerTotal = 0
        StartButton.setTitle("Stop", for: .normal)
        StartButton.backgroundColor = UIColor.red
        resetTime = RunTimeTextField.text!
        RunTimeTextField.isEnabled = false
        timerRun = true
        timerSeconds = 0
        timerTotal = 0
        TimerDoseRateLabel.text = String(0)
        TimerTotalDoseLabel.text = String(0)
        if countdownTimer == nil{
            countdownTimer = Timer.scheduledTimer(timeInterval: 1, target: self,  selector: #selector(timerLogic), userInfo: nil, repeats: true)
        }
        countdownTimer?.fire()
    }
    
    @IBAction func disconnectButtonPressed(_ sender: Any) {
    PeripheralConnectedViewController().disconnectButtonPressed()
    }
    
    @IBAction func LogCT007DataPressed(_ sender: Any) {
        PeripheralConnectedViewController().LogExternalData()
        
    }
}
