//
//  PeripheralTableViewController.swift
//  BLEScanner
//
//  Created by Sepehr Khatir on 15/03/2017.
//  Copyright © 2017 EIC. All rights reserved.
//

import CoreBluetooth
import UIKit
import Foundation
import SystemConfiguration
import UserNotifications

var Externalfilename = ""
var EXTERNALwriteStringHeaders = ""
var ExternalfileURL: URL = URL(string: "gammawatch.com")!
var FirstHomeLoad = true
var IsFileCreated = false

struct DisplayPeripheral{
	var peripheral: CBPeripheral?
	var lastRSSI: NSNumber?
	var isConnectable: Bool?
}

class PeripheralViewController: BaseViewController {
	
	@IBOutlet weak var statusLabel: UILabel!
	@IBOutlet weak var bluetoothIcon: UIImageView!
	@IBOutlet weak var scanningButton: ScanButton!
    @IBOutlet weak var ExtDetInfoLabel: UILabel!
    @IBOutlet weak var Banner: UIImageView!
    @IBOutlet weak var CameraRemoved: UILabel!
    
    //stuff from home
    let documentsPath1 = NSURL(fileURLWithPath: NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true)[0])
    var ExternallogsPath = URL(string: "")
    
    //URL that we get the image of the ads banner from
    let URL_BANNER = URL(string: "http://gammawatch.com/images/AppBanner.png")! // We can force unwrap because we are 100% certain the constructor will not return nil in this case.
    
    //end of stuff from home
    
    var RefreshScanCounter = 0
    //var centralManager: CBCentralManager?
    var peripherals: [DisplayPeripheral] = []
	var viewReloadTimer: Timer?
	
	var selectedPeripheral: CBPeripheral?
	
	@IBOutlet weak var tableView: UITableView!
	
	required init?(coder aDecoder: NSCoder) {
		super.init(coder: aDecoder)
		
		//Initialise CoreBluetooth Central Manager
		Singleton.sharedInstance.centralManager = CBCentralManager(delegate: self, queue: DispatchQueue.main)
	}
	
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(true)
        if view.subviews.last!.tag == 23{
            sideMenuOpen = false
            view.subviews.last!.removeFromSuperview()
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        addSlideMenuButton()
        self.tabBarController?.tabBar.isHidden = true
        self.extendedLayoutIncludesOpaqueBars = true
        super.viewDidLoad()
        print ("banner 1 is : ", Banner.image!)
        if isInternetAvailable() && isBannerEnabled(){
            let session = URLSession(configuration: .default)
            //creating a dataTask
            let getImageFromUrl = session.dataTask(with: URL_BANNER) { (data, response, error) in
                //if there is any error
                if let e = error {
                    //displaying the message
                    print("Error Occurred: \(e)")
                } else {
                    //in case of now error, checking wheather the response is nil or not
                    if (response as? HTTPURLResponse) != nil {
                        //checking if the response contains an image
                        if let imageData = data {
                            //getting the image
                            let image = UIImage(data: imageData)
                            //displaying the image
                            DispatchQueue.main.async {
                                self.Banner.image = image
                                print ("banner 2 is : ", self.Banner.image!)
                            }
                        } else {
                            self.Banner.image = #imageLiteral(resourceName: "GenericBanner")
                            print("Image file is currupted")
                            print ("banner 3 is : ", self.Banner.image!)
                        }
                    } else {
                        self.Banner.image = #imageLiteral(resourceName: "GenericBanner")
                        print("No response from server")
                        print ("banner 4 is : ", self.Banner.image!)
                    }
                }
            }
            getImageFromUrl.resume()
        } else {
            self.Banner.isHidden = true
            //now hides instead of placing generic banner
            //self.Banner.image = #imageLiteral(resourceName: "GenericBanner")
        }
        //starting the download task
        print ("banner 111 is : ", Banner.image!)
        print ("PeripheralView did load 01")
        addSlideMenuButton()
        /******************* gammawatch hyperlink to the website ********************/
        let tap = UITapGestureRecognizer(target: self, action: #selector(PeripheralViewController.tapFunction))
        let tap2 = UITapGestureRecognizer(target: self, action: #selector(PeripheralViewController.tapFunction2))
        ExtDetInfoLabel.isUserInteractionEnabled = true
        ExtDetInfoLabel.addGestureRecognizer(tap)
        let tapBanner = UITapGestureRecognizer(target: self, action: #selector(PeripheralViewController.tapBannerFunction))
        CameraRemoved.isUserInteractionEnabled = true
        CameraRemoved.addGestureRecognizer(tap2)
        // add it to the image view;
        Banner.addGestureRecognizer(tapBanner)
        // make sure imageView can be interacted with by user
        Banner.isUserInteractionEnabled = true
        if FirstHomeLoad {
            print ("FirstHomeLoad")
            UIApplication.shared.isIdleTimerDisabled = true
            FirstHomeLoad = false
            /************** get saved settings on first load ******************/
            myCT007.GetCT007SavedSettings()
        } else {
            print ("NOT FirstHomeLoad")
        }
        if !IsFileCreated {
            /************** creat files for in the folder ******************/
            CreateFolders()
            //CreateFileswithHeaders()
            CreateFiles()
            defaults.set(ExternalfileURL, forKey: "Saved_ExternalfileURL")
            IsFileCreated = true
            defaults.set(IsFileCreated, forKey: "Saved_IsFileCreated")
        } else {
            print ("Files already Created")
        }
        if #available(iOS 10.0, *) {
            let center = UNUserNotificationCenter.current()
            // Request permission to display alerts and play sounds.
            center.requestAuthorization(options: [.alert, .sound])
            { (granted, error) in
                // Enable or disable features based on authorization.
            }
        } else {
            UIApplication.shared.registerUserNotificationSettings(UIUserNotificationSettings(types: [.sound, .alert], categories: nil))
            UIApplication.shared.registerForRemoteNotifications()
        }
    }
    
    func isBannerEnabled() -> Bool{
        var myHTMLString: String
        do {
            myHTMLString = try String(contentsOf:(URL(string: "http://gammawatch.com/files/AppAdURL.txt"))!)
            let array = myHTMLString.components(separatedBy: CharacterSet.newlines)
            if array[1] == "true"{
                return true
            }
            print("returned false")
            return false
            //let data = try Data(contentsOf: url)
            // do something with data
            // if the call fails, the catch block is executed
        } catch {
            print(error.localizedDescription)
            return false
        }
    }
    
    func CreateFolders() {
        /******************* file External folder ********************/
        ExternallogsPath = documentsPath1.appendingPathComponent("External")
        //ExternallogsPath = ExternallogsPath?.appendingPathComponent(fileName)
        print("Ext LOG Path : " , ExternallogsPath!)
        
        do {
            try FileManager.default.createDirectory(atPath: ExternallogsPath!.path, withIntermediateDirectories: true, attributes: nil)
        } catch let error as NSError {
            NSLog("ERROR...Unable to create External directory \(error.debugDescription)")
        }
        
    }
    
    
    func CreateFileswithHeaders() {
        
        let date = Date()
        let formatter = DateFormatter()
        formatter.dateFormat = "yyyy:MM:dd'  'HH-mm-ss"
        let result = formatter.string(from: date)
        let fileName = result
        print ("result=  \(result)")
        
        Externalfilename = "External " + fileName
        
        
        /******************* file Extensions ********************/
        ExternalfileURL = (ExternallogsPath?.appendingPathComponent(Externalfilename).appendingPathExtension("csv"))!
        
        /******************* file Headers ********************/
        
        let EXTERNALwhatTowrite = "Time , Battery [%] , Selected Unit , DisplayCounts , DoseRate (short) , DoseRate (long) , Total Dose"
        
        EXTERNALwriteStringHeaders.append(EXTERNALwhatTowrite)
        
        /******************* writing Headers to the files ********************/
        do {
            try EXTERNALwriteStringHeaders.write(to: ExternalfileURL, atomically: true, encoding: String.Encoding.utf8)
        } catch let error as NSError {
            print("ERROR...Failed writing to External URL: \(ExternalfileURL), Error: " + error.localizedDescription)
        }
        
    }
    
    
    
    func CreateFiles() {
        Externalfilename = "DataLog-ExtDetector"
        /******************* file Extensions ********************/
        ExternalfileURL = (ExternallogsPath?.appendingPathComponent(Externalfilename).appendingPathExtension("csv"))!
    }
    
    
    func isInternetAvailable() -> Bool
    {
        var zeroAddress = sockaddr_in()
        zeroAddress.sin_len = UInt8(MemoryLayout.size(ofValue: zeroAddress))
        zeroAddress.sin_family = sa_family_t(AF_INET)
        
        let defaultRouteReachability = withUnsafePointer(to: &zeroAddress) {
            $0.withMemoryRebound(to: sockaddr.self, capacity: 1) {zeroSockAddress in
                SCNetworkReachabilityCreateWithAddress(nil, zeroSockAddress)
            }
        }
        
        var flags = SCNetworkReachabilityFlags()
        if !SCNetworkReachabilityGetFlags(defaultRouteReachability!, &flags) {
            return false
        }
        let isReachable = flags.contains(.reachable)
        let needsConnection = flags.contains(.connectionRequired)
        return (isReachable && !needsConnection)
    }

    
    
    
	override func viewWillAppear(_ animated: Bool) {
		super.viewWillAppear(animated)
		
        navigationController?.navigationBar.barTintColor = UIColor.orange
		viewReloadTimer = Timer.scheduledTimer(timeInterval: 1.0, target: self, selector: #selector(PeripheralViewController.refreshScanView), userInfo: nil, repeats: true)
        keyFobConnected = false
	}
	
	override func viewWillDisappear(_ animated: Bool) {
		super.viewWillDisappear(animated)
		
		viewReloadTimer?.invalidate()
	}
    
    @objc func tapFunction(sender:UITapGestureRecognizer) {
        print("tap working")
        
        let url = URL(string: "http://gammawatch.com/ExternalDetectors.html")!
        if #available(iOS 10.0, *) {
            UIApplication.shared.open(url, options: convertToUIApplicationOpenExternalURLOptionsKeyDictionary([:]), completionHandler: nil)
        } else {
            UIApplication.shared.openURL(url)
        }
        
    }
    
    @objc func tapFunction2(sender:UITapGestureRecognizer) {
        print("tap working")
        
        let url = URL(string: "http://gammawatch.com/gammaguard-camera-detector-removal")!
        if #available(iOS 10.0, *) {
            UIApplication.shared.open(url, options: convertToUIApplicationOpenExternalURLOptionsKeyDictionary([:]), completionHandler: nil)
        } else {
            UIApplication.shared.openURL(url)
        }
        
    }

    
    @objc func tapBannerFunction(sender:UITapGestureRecognizer) {
        print("tap working")
        
        var myHTMLString = ""
        
//        let myURLString = "http://gammawatch.com/files/AppAdURL.txt"
        
        do {
            myHTMLString = try String(contentsOf:(URL(string: "http://gammawatch.com/files/AppAdURL.txt"))!)
            let array = myHTMLString.components(separatedBy: CharacterSet.newlines)
            if array[1] == "true"{
                myHTMLString = array[0]
            }else{
                myHTMLString = "http://gammawatch.com/ExternalDetectors.html"
            }
            //let data = try Data(contentsOf: url)
            // do something with data
            // if the call fails, the catch block is executed
        } catch {
            print(error.localizedDescription)
            myHTMLString = "http://gammawatch.com/ExternalDetectors.html"
            
        }
        
        print(myHTMLString)
        let url = URL(string: myHTMLString)!
        if #available(iOS 10.0, *) {
            UIApplication.shared.open(url, options: convertToUIApplicationOpenExternalURLOptionsKeyDictionary([:]), completionHandler: nil)
        } else {
            UIApplication.shared.openURL(url)
        }
        
    }

	
    
	func updateViewForScanning(){
        if self.statusLabel != nil {
		statusLabel.text = " Scanning BLE Devices..."
		bluetoothIcon.pulseAnimation()
		bluetoothIcon.isHidden = false
		scanningButton.buttonColorScheme(true)
        } else {
            print ("ERROR ON updateViewForScanning --> statuslabel is nil ")
        }
	}
	
	func updateViewForStopScanning(){
		let plural = peripherals.count > 1 ? "s" : ""
		statusLabel.text = " \(peripherals.count) Device\(plural) Found"
		bluetoothIcon.layer.removeAllAnimations()
		bluetoothIcon.isHidden = true
		scanningButton.buttonColorScheme(false)
	}
	
	@IBAction func scanningButtonPressed(_ sender: AnyObject){
		if Singleton.sharedInstance.centralManager!.isScanning{
			Singleton.sharedInstance.centralManager?.stopScan()
			updateViewForStopScanning()
		}else{
			startScanning()
		}
	}
	
	func startScanning(){
        RefreshScanCounter = 1
		peripherals = []
		Singleton.sharedInstance.centralManager?.scanForPeripherals(withServices: [radiationService, keyFobService], options: [CBCentralManagerScanOptionAllowDuplicatesKey: true])
		updateViewForScanning()
		let triggerTime = (Int64(NSEC_PER_SEC) * 10)
		DispatchQueue.main.asyncAfter(deadline: DispatchTime.now() + Double(triggerTime) / Double(NSEC_PER_SEC), execute: { () -> Void in
			if (Singleton.sharedInstance.centralManager!.isScanning) && (self.RefreshScanCounter >= 9) {
				Singleton.sharedInstance.centralManager?.stopScan()
				self.updateViewForStopScanning()
			}
		})
	}
	
	@objc func refreshScanView()
	{
		if peripherals.count >= 0 && Singleton.sharedInstance.centralManager!.isScanning{
			tableView.reloadData()
            if RefreshScanCounter > 12 {
                RefreshScanCounter = 0
            }
            RefreshScanCounter += 1
		}
	}
	
	override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
		if let destinationViewController = segue.destination as? PeripheralConnectedViewController{
			destinationViewController.peripheral = selectedPeripheral
		}
	}
}

extension PeripheralViewController: CBCentralManagerDelegate{
	func centralManagerDidUpdateState(_ central: CBCentralManager){
		if (central.state == .poweredOn){
			startScanning()
		}else{
			// do something like alert the user that ble is not on
		}
	}
	
	func centralManager(_ central: CBCentralManager, didDiscover peripheral: CBPeripheral, advertisementData: [String : Any], rssi RSSI: NSNumber){
		
		for (index, foundPeripheral) in peripherals.enumerated(){
			if foundPeripheral.peripheral?.identifier == peripheral.identifier{
				peripherals[index].lastRSSI = RSSI
				return
			}
		}
		
		let isConnectable = advertisementData["kCBAdvDataIsConnectable"] as! Bool
		let displayPeripheral = DisplayPeripheral(peripheral: peripheral, lastRSSI: RSSI, isConnectable: isConnectable)
		peripherals.append(displayPeripheral)
		tableView.reloadData()
	}
}

extension PeripheralViewController: CBPeripheralDelegate {
	func centralManager(_ central: CBCentralManager, didFailToConnect peripheral: CBPeripheral, error: Error?) {
        print("Error connecting peripheral: \(String(describing: error?.localizedDescription))")
	}
	
	func centralManager(_ central: CBCentralManager, didConnect peripheral: CBPeripheral) {
        print ("perform segu PeripheralConnectedSegue 0000")
        Singleton.sharedInstance.centralManager?.stopScan()
		performSegue(withIdentifier: "PeripheralConnectedSegue", sender: self)
 		peripheral.discoverServices(nil)

	}
}

extension PeripheralViewController: UITableViewDataSource {
	func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell{
		
		let cell = self.tableView.dequeueReusableCell(withIdentifier: "cell")! as! DeviceTableViewCell
//        print ("the new cell is : ", cell)
//        print ("\n")
		cell.displayPeripheral = peripherals[indexPath.row]
		cell.delegate = self
		return cell
	}
	
	func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int{
		return peripherals.count
	}
}

extension PeripheralViewController: DeviceCellDelegate{
	func connectPressed(_ peripheral: CBPeripheral) {
		if peripheral.state != .connected {
			selectedPeripheral = peripheral
			peripheral.delegate = self
            print ("selectedPeripheral...... 002 \(String(describing: selectedPeripheral))")
			Singleton.sharedInstance.centralManager?.connect(peripheral, options: nil)
		}
	}
}


// Helper function inserted by Swift 4.2 migrator.
fileprivate func convertToUIApplicationOpenExternalURLOptionsKeyDictionary(_ input: [String: Any]) -> [UIApplication.OpenExternalURLOptionsKey: Any] {
	return Dictionary(uniqueKeysWithValues: input.map { key, value in (UIApplication.OpenExternalURLOptionsKey(rawValue: key), value)})
}
