//
//  CT007SettingsViewController+SettingHandler.swift
//  GammaGuard
//
//  Created by EIC on 2018-11-02.
//  Copyright © 2018 EIC. All rights reserved.
//

import Foundation
import UIKit

extension CT007SettingsViewController{
    func loggingEnabledSetting(on: Bool){
        if on{
            CT007LogLabel.text = "Logging is ON"
            IsCT007LoggingEnabled = true
            CT007LogIntTextField.isEnabled = true
            CT007LogIntLabel.isEnabled = true
            CT007LogIntTextField.textColor = UIColor.black
            NotificationCenter.default.post(name: Notification.Name(rawValue: send_msg_to_console_NotificationKey), object: self)
        }else{
            CT007LogLabel.text = "Logging is OFF"
            IsCT007LoggingEnabled = false
            CT007LogIntTextField.isEnabled = false
            CT007LogIntLabel.isEnabled = false
            CT007LogIntTextField.textColor = UIColor.gray
            NotificationCenter.default.post(name: Notification.Name(rawValue: send_msg_to_console_NotificationKey), object: self)
        }
        defaults.set(IsCT007LoggingEnabled, forKey: "Saved_IsCT007LoggingEnabled")
    }
}
