//
//  singleton.swift
//  UITableVC
//
//  Created by Sepehr Khatir on 15/03/2017.
//  Copyright © 2017 EIC. All rights reserved.
//

import Foundation
import CoreBluetooth

final class Singleton {
    
    // Can't init is singleton
    private init() { }
    
    //MARK: Shared Instance
    //var peripheral: CBPeripheral?
    var centralManager: CBCentralManager?
    static let sharedInstance: Singleton = Singleton()
    
    //MARK: Local Variable
    
    var emptyStringArray : [String] = []
    
}
