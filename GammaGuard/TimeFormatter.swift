//
//  TimeFormatter.swift
//  GammaGuard
//
//  Created by EIC on 2018-05-14.
//  Copyright © 2018 EIC. All rights reserved.
//

import UIKit
import Charts

class TimeFormatter: NSObject, IAxisValueFormatter {
    private let dateFormatter = DateFormatter()
    var updateDict = [Double: [Int]]()
    
    override init() {
        super.init()
        dateFormatter.dateFormat = "HH:mm:ss"
    }
    
    public func stringForValue(_ value: Double, axis: AxisBase?) -> String {
        var basetime = Double(liveGraphBaseTime)
        if archiveGraphBaseTime > 0{
            basetime = Double(archiveGraphBaseTime)
        }
        return dateFormatter.string(from: Date(timeIntervalSince1970: value + basetime))
    }
}
