//
//  BaseViewController.swift
//  AKSwiftSlideMenu
//
//  Created by Sepehr Khatir on 15/03/2017.
//  Copyright © 2017 EIC. All rights reserved.
//

import UIKit

class BaseViewController: UIViewController, SlideMenuDelegate {
    
    var sideMenuOpen = false
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
    }
    
    func slideMenuItemSelectedAtIndex(_ index: Int32) {
        let topViewController : UIViewController = self.navigationController!.topViewController!
        print("View Controller is : \(topViewController) \n", terminator: "")
        switch(index){
        case 0:
            print("External Settings\n", terminator: "")
            
            if !fromHome {
                self.openViewControllerBasedOnIdentifier2("D")
            } else {
                self.openViewControllerBasedOnIdentifier("D")
            }
            break
        case 2:
            print("OPEN Graph Archive\n", terminator: "")
            if !fromHome {
                self.openViewControllerBasedOnIdentifier2("Graph")
            } else {
                self.openViewControllerBasedOnIdentifier("Graph")
            }
            break
        case 4:
            print("OPEN About page\n", terminator: "")
            
            if !fromHome {
                self.openViewControllerBasedOnIdentifier2("About")
            } else {
                self.openViewControllerBasedOnIdentifier("About")
            }
            break
        default:
            print ("same page view")
        }
    }
    
    func openViewControllerBasedOnIdentifier(_ strIdentifier:String){
        let destViewController : UIViewController = self.storyboard!.instantiateViewController(withIdentifier: strIdentifier)
        
        let topViewController : UIViewController = self.navigationController!.topViewController!
        
        if (topViewController.restorationIdentifier! == destViewController.restorationIdentifier!){
            print("Same VC")
        } else {
            print("ID 1")
            self.navigationController!.pushViewController(destViewController, animated: true)
        }
    }
    
    func openViewControllerBasedOnIdentifier2(_ strIdentifier:String){
        let destViewController : UIViewController = self.storyboard!.instantiateViewController(withIdentifier: strIdentifier)

        //let topViewController : UIViewController = self.navigationController!.topViewController!
        print("ID 2")
        self.navigationController!.pushViewController(destViewController, animated: true)
    }
    
    func openViewControllersegue(){
        let topViewController : UIViewController = self.navigationController!.topViewController!
        print ("segue: ")
        
        if (topViewController.restorationIdentifier! == "CTSimple") ||
            (topViewController.restorationIdentifier! == "CTdetail") ||
            (topViewController.restorationIdentifier! == "CTinterpret") {
            
        print ("Dont segue: ")
            
        }else if (topViewController.restorationIdentifier! == "A") {
            performSegue(withIdentifier: "Atoctsegue", sender: Any?.self)
        }else if (topViewController.restorationIdentifier! == "B")  {
            performSegue(withIdentifier: "Btoctsegue", sender: Any?.self)
        }else if (topViewController.restorationIdentifier! == "C")  {
            performSegue(withIdentifier: "Ctoctsegue", sender: Any?.self)
        }else if (topViewController.restorationIdentifier! == "D")  {
            performSegue(withIdentifier: "Dtoctsegue", sender: Any?.self)
        }else if (topViewController.restorationIdentifier! == "Home")  {
            performSegue(withIdentifier: "hometoctsegue", sender: Any?.self)
        }else{
            
            print ("ignore")
        }
    }
    
    
    
    func addSlideMenuButton(){
        let btnShowMenu = UIButton(type: UIButton.ButtonType.system)
        btnShowMenu.setImage(self.defaultMenuImage(), for: UIControl.State())
        btnShowMenu.frame = CGRect(x: 0, y: 0, width: 30, height: 30)
        btnShowMenu.addTarget(self, action: #selector(BaseViewController.onSlideMenuButtonPressed(_:)), for: UIControl.Event.touchUpInside)
        let customBarItem = UIBarButtonItem(customView: btnShowMenu)
        self.navigationItem.leftBarButtonItem = customBarItem;
    }
    

    // Creates slide menu image
    func defaultMenuImage() -> UIImage {
        var defaultMenuImage = UIImage()
        
        UIGraphicsBeginImageContextWithOptions(CGSize(width: 30, height: 22), false, 0.0)
        
        UIColor.black.setFill()
        UIBezierPath(rect: CGRect(x: 0, y: 3, width: 30, height: 1)).fill()
        UIBezierPath(rect: CGRect(x: 0, y: 10, width: 30, height: 1)).fill()
        UIBezierPath(rect: CGRect(x: 0, y: 17, width: 30, height: 1)).fill()
        
        UIColor.white.setFill()
        UIBezierPath(rect: CGRect(x: 0, y: 4, width: 30, height: 1)).fill()
        UIBezierPath(rect: CGRect(x: 0, y: 11,  width: 30, height: 1)).fill()
        UIBezierPath(rect: CGRect(x: 0, y: 18, width: 30, height: 1)).fill()
        
        defaultMenuImage = UIGraphicsGetImageFromCurrentImageContext()!
        
        UIGraphicsEndImageContext()
       
        return defaultMenuImage;
    }
    
    //Creating mode switch button
    func defaultModeImage() -> UIImage {
        var defaultMenuImage = UIImage()
        UIGraphicsBeginImageContextWithOptions(CGSize(width: 60, height: 30), false, 0.0)
        UIColor.black.setFill()
        UIBezierPath(rect: CGRect(x: 0, y: 0, width: 60, height: 2)).fill()
        UIBezierPath(rect: CGRect(x: 0, y: 28, width: 60, height: 2)).fill()
        UIBezierPath(rect: CGRect(x: 0, y: 0, width: 2, height: 30)).fill()
        UIBezierPath(rect: CGRect(x: 58, y: 0, width: 2, height: 30)).fill()
        let textFontAttributes = [
            NSAttributedString.Key.font: UIFont(name: "HelveticaNeue-Bold", size: 15),
            NSAttributedString.Key.foregroundColor: UIColor.black,
            ]
        let text: NSString = "Mode"
        let textSize = text.size(withAttributes: [NSAttributedString.Key.font:UIFont(name: "HelveticaNeue-Bold", size: 15)!])
        let textRect = CGRect(x: 60/2 - textSize.width/2, y: 30/2 - textSize.height/2, width: textSize.width, height: textSize.height)
        text.draw(in: textRect, withAttributes: (textFontAttributes as Any as! [NSAttributedString.Key : Any]))
        defaultMenuImage = UIGraphicsGetImageFromCurrentImageContext()!
        UIGraphicsEndImageContext()
        return defaultMenuImage;
    }
    
    @objc func onSlideMenuButtonPressed(_ sender : UIButton){
        if sideMenuOpen
        {
            // To Hide Menu If it already there
            self.slideMenuItemSelectedAtIndex(-1);
            sideMenuOpen = false
            
            sender.tag = 0;
            
            let viewMenuBack : UIView = view.subviews.last!
            if viewMenuBack.tag == 23{
                
                UIView.animate(withDuration: 0.3, animations: { () -> Void in
                    var frameMenu : CGRect = viewMenuBack.frame
                    frameMenu.origin.x = -1 * UIScreen.main.bounds.size.width
                    viewMenuBack.frame = frameMenu
                    viewMenuBack.layoutIfNeeded()
                    viewMenuBack.backgroundColor = UIColor.clear
                    }, completion: { (finished) -> Void in
                        viewMenuBack.removeFromSuperview()
                })
            }
            
            return
        }
        sender.isEnabled = false
        sideMenuOpen = true
        
        let menuVC : MenuViewController = self.storyboard!.instantiateViewController(withIdentifier: "MenuViewController") as! MenuViewController
        menuVC.btnMenu = sender
        menuVC.delegate = self
        menuVC.view.tag = 23
        self.view.addSubview(menuVC.view)
        self.addChild(menuVC)
        menuVC.view.layoutIfNeeded()
        menuVC.view.frame=CGRect(x: 0 - UIScreen.main.bounds.size.width, y: 0, width: UIScreen.main.bounds.size.width, height: UIScreen.main.bounds.size.height);
        UIView.animate(withDuration: 0.3, animations: { () -> Void in
            menuVC.view.frame=CGRect(x: 0, y: 0, width: UIScreen.main.bounds.size.width, height: UIScreen.main.bounds.size.height);
            sender.isEnabled = true
            }, completion:nil)
    }
    

    
    
}
