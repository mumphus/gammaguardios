//
//  BluetoothColorsExtension.swift
//  BLEScanner
//
//  Created by Sepehr Khatir on 15/03/2017.
//  Copyright © 2017 EIC. All rights reserved.
//

import UIKit

extension UIColor{
	class func bluetoothBlueColor() -> UIColor{
		return UIColor(red: 26.0/255.0, green: 118.0/255.0, blue: 245.0/255.0, alpha: 1.0)
	}
	
	class func bluetoothGreenColor() -> UIColor{
		return UIColor(red: 0.0/255.0, green: 212.0/255.0, blue: 142.0/255.0, alpha: 1.0)
	}
	
	class func bluetoothRedColor() -> UIColor{
		return UIColor(red: 246.0/255.0, green: 65.0/255.0, blue: 56.0/255.0, alpha: 1.0)
	}
	
	class func bluetoothOrangeColor() -> UIColor{
		return UIColor(red: 255.0/255.0, green: 128.0/255.0, blue: 0/255.0, alpha: 1.0)
	}
    
    class func MenuDarkGrayColor() -> UIColor{
        return UIColor(red: 100.0/255.0, green: 100.0/255.0, blue: 100/255.0, alpha: 1.0)
    }
    
    class func warningGreenColor() -> UIColor{
        return UIColor(red: 0.0/255.0, green: 220.0/255.0, blue: 0/255.0, alpha: 1.0)
    }
    class func warningYellowColor() -> UIColor{
        return UIColor(red: 220.0/255.0, green: 220.0/255.0, blue: 0/255.0, alpha: 1.0)
    }
    class func warningRedColor() -> UIColor{
        return UIColor(red: 220.0/255.0, green: 0.0/255.0, blue: 0/255.0, alpha: 1.0)
    }
}
