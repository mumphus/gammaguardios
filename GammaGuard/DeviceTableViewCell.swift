//
//  DeviceTableViewCell.swift
//  BLEScanner
//
//  Created by Sepehr Khatir on 15/03/2017.
//  Copyright © 2017 EIC. All rights reserved.
//

import UIKit
import CoreBluetooth

protocol DeviceCellDelegate: class {
    
	func connectPressed(_ peripheral: CBPeripheral)
    
}

class DeviceTableViewCell: UITableViewCell {

	@IBOutlet weak var deviceNameLabel: UILabel!
	@IBOutlet weak var deviceRssiLabel: UILabel!
	@IBOutlet weak var connectButton: UIButton!
	
	var delegate: DeviceCellDelegate?
	
	var displayPeripheral: DisplayPeripheral? {
		didSet {
            if let deviceName = displayPeripheral!.peripheral?.name{
				deviceNameLabel.text = deviceName.isEmpty ? "No Device Name" : deviceName
			}else{
				deviceNameLabel.text = "No Device Name"
			}
			if let rssi = displayPeripheral!.lastRSSI {
				deviceRssiLabel.text = "\(rssi)dB"
			}
			connectButton.isHidden = !(displayPeripheral?.isConnectable!)!
		}
	}
	
	@IBAction func connectButtonPressed(_ sender: AnyObject) {
        print ("DeviceTableViewCell connect button pressed 006")
        //CT007ViewController.selectedPeripheral = displayPeripheral?.peripheral
		delegate?.connectPressed((displayPeripheral?.peripheral)!)
        doneLoadingDevice = false
        CTData.currentSet = nil
        CTData.totalCT007Counts = 0
        CTData.totalTime = 0
        CTData.recordedPoints = 0
        CTData.over30Points = false
        fromHome = false
	}
}
