//
//  MenuViewController.swift
//  AKSwiftSlideMenu
//
//  Created by Sepehr Khatir on 15/03/2017.
//  Copyright © 2017 EIC. All rights reserved.
//

import UIKit

protocol SlideMenuDelegate {
    func slideMenuItemSelectedAtIndex(_ index : Int32)
}

var menuDidload2 = false

class MenuViewController: UIViewController, UITableViewDataSource, UITableViewDelegate {
    
    var LabelVersion = ""
    var LabelBuild = ""
    /**
    *  Array to display menu options
    */
    @IBOutlet var tblMenuOptions : UITableView!
    
    /**
    *  Transparent button to hide menu
    */
    @IBOutlet var btnCloseMenuOverlay : UIButton!
    
    /**
    *  Array containing menu options
    */
    var arrayMenuOptions = [Dictionary<String,String>]()
    
    /**
    *  Menu button which was tapped to display the menu
    */
    var btnMenu : UIButton!
    var btnMenutab : UITabBarItem!
    
    /**
    *  Delegate of the MenuVC
    */
    var delegate : SlideMenuDelegate?
    var MenuDidLoad = false
    
    override func viewDidLoad() {
        super.viewDidLoad()
        tblMenuOptions.tableFooterView = UIView()
        // Do any additional setup after loading the view.
        MenuDidLoad = true
        /**
         *  Get build number
         */
        
        if let BuildNumber = Bundle.main.infoDictionary?["CFBundleVersion"] as? String {
            LabelBuild = BuildNumber
        }
        
        /**
         *  Get Version number
         */
        if let version = Bundle.main.infoDictionary?["CFBundleShortVersionString"] as? String {
            LabelVersion = version
        }
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        updateArrayMenuOptions()
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(true)
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(true)
        MenuDidLoad = false
            }
    
    func updateArrayMenuOptions(){
        if MenuDidLoad {
        arrayMenuOptions.append(["title":"Settings", "icon":"ExternalSettingsIcon"])
            arrayMenuOptions.append([:])
        arrayMenuOptions.append(["title":"Graph Archive", "icon":"GraphArchiveIcon"])
            arrayMenuOptions.append([:])
        arrayMenuOptions.append(["title":"About", "icon":"Versions"])
            arrayMenuOptions.append([:])
            arrayMenuOptions.append(["title":"Version:"])
        tblMenuOptions.reloadData()
            
            
        }
    }
    
    @IBAction func onCloseMenuClick(_ button:UIButton!){
        btnMenu.tag = 0
        
        if (self.delegate != nil) {
            var index = Int32(button.tag)
            if(button == self.btnCloseMenuOverlay){
                index = -1
            }
            delegate?.slideMenuItemSelectedAtIndex(index)
        }
        
        
        
        UIView.animate(withDuration: 0.3, animations: { () -> Void in
            self.view.frame = CGRect(x: -UIScreen.main.bounds.size.width, y: 0, width: UIScreen.main.bounds.size.width,height: UIScreen.main.bounds.size.height)
            self.view.layoutIfNeeded()
            self.view.backgroundColor = UIColor.clear
            }, completion: { (finished) -> Void in
                self.view.removeFromSuperview()
                self.removeFromParent()
        })
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell : UITableViewCell = tableView.dequeueReusableCell(withIdentifier: "cellMenu")!
        let celldivider = UITableViewCell(style: UITableViewCell.CellStyle.default,reuseIdentifier: "celldivider")
        let cellversion = UITableViewCell(style: UITableViewCell.CellStyle.value1, reuseIdentifier: "cellversion")
        
        cell.selectionStyle = UITableViewCell.SelectionStyle.none
        cell.layoutMargins = UIEdgeInsets.zero
        cell.preservesSuperviewLayoutMargins = false
        
        let lblTitle : UILabel = cell.contentView.viewWithTag(101) as! UILabel
        let imgIcon : UIImageView = cell.contentView.viewWithTag(100) as! UIImageView
        
        if (indexPath.row == 1 || indexPath.row == 3 || indexPath.row == 5) {
            celldivider.backgroundColor = UIColor.MenuDarkGrayColor()
            return celldivider
        } else if (indexPath.row == 6) {
            lblTitle.text = nil
            imgIcon.image = nil
            cellversion.textLabel?.text = "Version"
            cellversion.detailTextLabel?.text = "\(LabelVersion)(\(LabelBuild))"
            cellversion.backgroundColor = UIColor.clear
         return cellversion
        } else {
            //cell.alpha = 0
            cell.backgroundColor = UIColor.clear
            cell.accessoryType = UITableViewCell.AccessoryType.disclosureIndicator
            //cell.accessoryType = UITableViewCellAccessoryType.disclosureIndicator

            imgIcon.image = UIImage(named: arrayMenuOptions[indexPath.row]["icon"]!)
            lblTitle.text = arrayMenuOptions[indexPath.row]["title"]!

        }
        
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let btn = UIButton(type: UIButton.ButtonType.custom)
        btn.tag = indexPath.row
        self.onCloseMenuClick(btn)
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arrayMenuOptions.count
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat
    {
        if (indexPath.row == 1 || indexPath.row == 3 || indexPath.row == 5) {
            return 20.0;//Choose your custom row height
        } else if (indexPath.row == 6) {
            return 40.0
        }
        return 60.0
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1;
    }
        
}
