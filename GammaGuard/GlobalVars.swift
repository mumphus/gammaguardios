//
//  GlobalVars.swift
//  GammaGuard
//
//  Created by EIC on 2018-05-17.
//  Copyright © 2018 EIC. All rights reserved.
//

import Foundation
import UIKit

// Added this as there were global variables in random places all over
// and cleaner code compared to loading saved variables for multiple VCs
let myCT007 = PeripheralConnectedViewController()
let defaults = UserDefaults.standard
var CT007ResponseTime = 0
var CT007DeadTime = 0
var CT007Mode = "Gamma"
var keyFobConnected = false
var CT007Screens = [1,1,1,1]
var CT007Units = 2
var CT007MinimumCounts: Int = 100
var CT007YellowLevelGamma: Double = 5
var CT007RedLevelGamma: Double = 250000
var CT007MaxLevelGamma: Double = 1000000
var CT007YellowLevelContam: Double = 300
var CT007RedLevelContam: Double = 100000
var CT007MaxLevelContam: Double = 500000
var CT007CurrentYellowLevel = CT007YellowLevelGamma
var CT007CurrentRedLevel = CT007RedLevelGamma
var CT007CurrentMaxLevel = CT007MaxLevelGamma
var CT007GaugeMax = CT007YellowLevelGamma
var CT007_GammaUnits = "µSv/h"
var CT007_ContamUnits = "CPM"
var appBeep: Bool = true
var CT007DeviceAlarm: Bool = true
var CT007DeviceResponseCapable: Bool = true
var CT007DeviceDeadTimeCapable: Bool = true
var CT007DetectorDetails = [Any]()
var CT007LogInterval = 300
var CT007AlarmLevel: Int = 1
var IsCT007LoggingEnabled = false
var IsDetectorBeepingEnabled = false
var IsDetectorAlarmingEnabled = false
var CT007CountsToBeep: Int = 1
var doneLoadingDevice = false
var currentConversionFactor: Double = 0.0
var CT007V: Int = 0
var isLiveGraphEnabled: Bool = false
var liveGraphBaseTime: Int = 0
var archiveGraphBaseTime: Int = 0
var playAlarm: Bool = false
var fromHome: Bool = true
var rssiReloadTimer: Timer?
weak var countdownTimer: Timer?
var graphPointDuration: Int = 5 //Time in seconds
var disconnectAlertVisible = false
var connectingAlertVisible = false

func doseModeLabel() -> UILabel{
    let label = UILabel(frame: CGRect(x:0, y:0, width:400, height:50))
    label.backgroundColor = .clear
    label.numberOfLines = 2
    label.font = UIFont.boldSystemFont(ofSize: 16.0)
    label.textAlignment = .center
    label.textColor = .black
    label.text = "Dose Mode\nDetector: \(Detector_NAME!)"
    return label
}

func countModeLabel() -> UILabel{
    let label = UILabel(frame: CGRect(x:0, y:0, width:400, height:50))
    label.backgroundColor = .clear
    label.numberOfLines = 2
    label.font = UIFont.boldSystemFont(ofSize: 16.0)
    label.textAlignment = .center
    label.textColor = .black
    label.text = "Counts Mode\nDetector: \(Detector_NAME!)"
    return label
}

func invalidModeLabel() -> UILabel{
    let label = UILabel(frame: CGRect(x:0, y:0, width:400, height:50))
    label.backgroundColor = .clear
    label.numberOfLines = 2
    label.font = UIFont.boldSystemFont(ofSize: 16.0)
    label.textAlignment = .center
    label.textColor = .black
    label.text = "Invalid Mode"
    return label
}
