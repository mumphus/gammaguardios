//
//  CT007Data.swift
//  UITableVC
//
//  Created by Sepehr Khatir on 15/03/2017.
//  Copyright © 2017 EIC. All rights reserved.
//

import UIKit
import AVFoundation
import CoreLocation
import CoreData

precedencegroup PowerPrecedence { higherThan: MultiplicationPrecedence }
infix operator ^^ : PowerPrecedence
func ^^ (radix: Int, power: Int) -> Double {
    return pow(Double(radix), Double(power))
}


class CT007Data: UIViewController, CLLocationManagerDelegate{
    /********** Values **********/
    var CT007dose = 0.0
    var CT007doseArray: [Double] = Array(repeating: 0, count: 30)
    var CT007avgIndex = 0
    var totalTime = 0
    var totalCT007Counts: Double = 0
    var totalCT007Dose = 0.0
    var totalAcumulatedCT007Counts: Double = 0
    var CT007_Dose_Conversion_Factor = 1.0
    var CT007_DoseRate_Conversion_Factor = 1.0
    var C_to_uSv_Conversion: Double = 1.0
    var CPS_to_uSvPerHour_Conversion: Double = 1.0
    var currentCoordinates = [Double]()
    var currentSet: NSManagedObject?
    var lastTime: Double = 0
    var autoSaveCounter: Int = 0
    var autoSaveTime: Int = 60 //Time in seconds
    var graphCounts: Double = 0
    var graphDurationCounter: Int = 0
    /***************************/
    var testCounter = 0
    var hasInitContext = false
    var privateContext = NSManagedObjectContext.init(concurrencyType: .privateQueueConcurrencyType)
    var appDelegate: AppDelegate?
    var managedContext: NSManagedObjectContext?
    var recordedPoints: Int = 0
    var over30Points: Bool = false
    
    
    func addCT007Counts (counts: Double){
        if !over30Points{
            recordedPoints += 1
            if recordedPoints >= 30{
                over30Points = true
            }
        }
        totalAcumulatedCT007Counts = counts
        totalTime += 1
//        totalCT007Counts += totalAcumulatedCT007Counts
        totalCT007Dose = Double(totalCT007Counts) * C_to_uSv_Conversion
        CT007doseArray[CT007avgIndex] = counts
        CT007avgIndex += 1
        if ((CT007avgIndex )  == CT007doseArray.count){
            CT007avgIndex = 0
        }
        //DATA FOR GRAPH
        if isLiveGraphEnabled{
            getLocation()
            let currentTime = getTime()
            if currentCoordinates.count > 0 && doneLoadingDevice{
                graphDurationCounter += 1
                graphCounts += counts
                print(graphDurationCounter, "/", graphPointDuration, " | ",graphCounts, " - ", graphCounts/Double(graphDurationCounter))
                if graphDurationCounter >= graphPointDuration{
                    if currentSet == nil{
                        createNewGraphEntry(device: Detector_NAME!, lastSave: Int64(currentTime), frequency: graphPointDuration)
                        liveGraphBaseTime = currentTime
                    }
    //                if testCounter < 100{
    //                    for _ in 0..<1000{
                    addNewGraphPoint(counts: graphCounts, time: Int64(currentTime - liveGraphBaseTime), lat: currentCoordinates[0], long: currentCoordinates[1])
    //                    }
    //                    testCounter += 1
    //                }
                    graphCounts = 0
                    graphDurationCounter = 0
                }
            }else{
                print("No location found")
            }
        }
        //END OF DATA FOR GRAPH
    }

    func createNewGraphEntry(device: String, lastSave: Int64, frequency: Int){
        if !hasInitContext{
            initContext()
            hasInitContext = true
        }
//        guard let appDelegate = UIApplication.shared.delegate as? AppDelegate else {return}
//        privateContext = appDelegate.generatePrivateContext()
        
        privateContext.perform {
            let entity = NSEntityDescription.entity(forEntityName: "GraphDataSet", in: self.privateContext)!
            let set = NSManagedObject(entity: entity, insertInto: self.privateContext)
            set.setValue(lastSave, forKeyPath: "dateSaved")
            set.setValue(device, forKeyPath: "deviceUsed")
            set.setValue(frequency, forKeyPath: "pointFrequency")
            set.setValue(self.CPS_to_uSvPerHour_Conversion, forKey: "cpsTouSvh")
            self.currentSet = set
        }
    }
    
    func addNewGraphPoint(counts: Double, time: Int64, lat: Double, long: Double) {
//        guard let appDelegate = UIApplication.shared.delegate as? AppDelegate else {
//            return
//        }
//        let managedContext = appDelegate.managedObjectContext
        
        privateContext.perform {
            let entity = NSEntityDescription.entity(forEntityName: "GraphDataPoint", in: self.privateContext)!
            let point = NSManagedObject(entity: entity, insertInto: self.privateContext)
            let set = self.currentSet as! GraphDataSet
            let values = set.values
            let points = set.mutableOrderedSetValue(forKey: "dataPoints")
            set.setValue(values+1, forKey: "values")
            point.setValue(counts, forKeyPath: "count")
            point.setValue(time, forKeyPath: "time")
            point.setValue(lat, forKeyPath: "latitude")
            point.setValue(long, forKey: "longitude")
            points.add(point)
            self.autoSaveCounter += 1
            if self.autoSaveCounter == self.autoSaveTime{
                self.autoSaveCounter = 0
                self.saveGraph()
            }
        }
    }
    
    func saveGraph(){
        print("SAVING DATA")
        do {
            try self.privateContext.save()
            self.managedContext!.performAndWait {
                do {try self.managedContext!.save()}
                catch {fatalError("Failure to save context: \(error)")}
            }
        } catch {fatalError("Failure to save context: \(error)")}
    }
    
    func initContext(){
        guard let ad = UIApplication.shared.delegate as? AppDelegate else {
            return
        }
        appDelegate = ad
        managedContext = appDelegate!.managedObjectContext
        privateContext = appDelegate!.generatePrivateContext()
    }
    
    
    //return the CPS value
        func getCT007DoseRate() -> Double{
            var CT007doserate: Double = 0.0
            var CT007avgTotal: Double = 0
            if CT007ResponseTime == 0{
                var values: Int = 0
                while CT007avgTotal <= Double(CT007MinimumCounts) && values < recordedPoints{
                    var index = CT007avgIndex - 1 - values
                    if index < 0{
                        index += CT007doseArray.count
                    }
                    CT007avgTotal += CT007doseArray[index]
                    values += 1
                }
                CT007doserate = Double(CT007avgTotal) / Double(values)
                if (CT007doserate < 0) {CT007doserate = 0}
            }
            else{
                for i in 0..<(recordedPoints >= 30 ? CT007ResponseTime : recordedPoints) {
                    var counter = CT007avgIndex - i - 1
                    if counter < 0{
                        counter += CT007doseArray.count
                    }
                    CT007avgTotal += CT007doseArray[counter]
                }
                CT007doserate = Double(CT007avgTotal) / Double(recordedPoints >= 30 ? CT007ResponseTime : recordedPoints)
                if (CT007doserate < 0) {CT007doserate = 0}
            }
            //CPS
            return CT007doserate
        }
    
    //return the CPS value
    func getLongCT007DoseRate() -> Double{
        //load detector details
        var maxResponseTime = 30
        if CT007DetectorDetails.count > 0{
            maxResponseTime = CT007DetectorDetails[6] as! Int
        }
        
        var CT007doserate: Double = 0.0
        var CT007avgTotal: Double = 0
        for i in 0..<(recordedPoints >= 30 ? maxResponseTime : recordedPoints) {
            var counter = CT007avgIndex - i - 1
            if counter < 0{
                counter += CT007doseArray.count
            }
            CT007avgTotal += CT007doseArray[counter]
        }
        CT007doserate = Double(CT007avgTotal) / Double(recordedPoints >= 30 ? maxResponseTime : recordedPoints)
        if (CT007doserate < 0) {CT007doserate = 0}
        //CPS
        return CT007doserate
    }
    
    // return the cpm or dose rate in uSv
    func getCT007_uSv_h_DoseRate(array: String) -> Double{
        var CT007_uSv_h_doserate: Double = 0.0
        switch array {
        case "CPM":
            CT007_uSv_h_doserate = getCT007DoseRate() * 60
            
        case "µSv_h":
            CT007_uSv_h_doserate = getCT007DoseRate() * CPS_to_uSvPerHour_Conversion
            
        default:
            CT007_uSv_h_doserate = 0
            
        }
        if (CT007_uSv_h_doserate < 0) {CT007_uSv_h_doserate = 0}
        
        return CT007_uSv_h_doserate
    }
    
    // return the long cpm or dose rate in uSv
    func getLongCT007_uSv_h_DoseRate(array: String) -> Double{
        var CT007_uSv_h_doserate: Double = 0.0
        switch array {
        case "CPM":
            CT007_uSv_h_doserate = getLongCT007DoseRate() * 60
            
        case "µSv_h":
            CT007_uSv_h_doserate = getLongCT007DoseRate() * CPS_to_uSvPerHour_Conversion
            
        default:
            CT007_uSv_h_doserate = 0
            
        }
        if (CT007_uSv_h_doserate < 0) {CT007_uSv_h_doserate = 0}
        
        return CT007_uSv_h_doserate
    }
    
    func graphArrayCompress (countRateArray: [Double]) -> [Double]{
        var newArray = [Double]()
        for i in 0..<50{
            var newValue: Double = 0
            newValue += (countRateArray[i*2] + countRateArray[1+(i*2)])/2
            newArray.append(newValue)
        }
        return newArray
    }
    
    func getTime() -> Int{
        return Int(NSDate().timeIntervalSince1970-0.5)
    }
    
    let locationMgr = CLLocationManager()
    func getLocation(){
        // 1
        let status  = CLLocationManager.authorizationStatus()
        
        // 2
        if status == .notDetermined {
            locationMgr.requestWhenInUseAuthorization()
            return
        }
        
        // 3
        if status == .denied || status == .restricted {
            let alert = UIAlertController(title: "Location Services Disabled", message: "Please enable Location Services in Settings", preferredStyle: .alert)
            
            let okAction = UIAlertAction(title: "OK", style: .default, handler: nil)
            alert.addAction(okAction)
            
            present(alert, animated: true, completion: nil)
            return
        }
        
        
        // 4
        locationMgr.delegate = self
        locationMgr.startUpdatingLocation()
    }
    
    // 1
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        print("111 \(locations.count)")
        let currentLocation  = locations.last!
        currentCoordinates.removeAll()
        currentCoordinates.append(currentLocation.coordinate.latitude)
        currentCoordinates.append(currentLocation.coordinate.longitude)
    }
    
    // 2
    func locationManager(_ manager: CLLocationManager, didFailWithError error: Error) {
        print("Error \(error)")
    }

}
