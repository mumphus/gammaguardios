//
//  GraphDataSet+CoreDataProperties.swift
//  
//
//  Created by EIC on 2018-07-17.
//
//

import Foundation
import CoreData


extension GraphDataSet {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<GraphDataSet> {
        return NSFetchRequest<GraphDataSet>(entityName: "GraphDataSet")
    }

    @NSManaged public var cpsTouSvh: Double
    @NSManaged public var dateSaved: Int64
    @NSManaged public var deviceUsed: String?
    @NSManaged public var name: String?
    @NSManaged public var values: Int64
    @NSManaged public var pointFrequency: Int64
    @NSManaged public var dataPoints: NSOrderedSet?

}

// MARK: Generated accessors for dataPoints
extension GraphDataSet {

    @objc(insertObject:inDataPointsAtIndex:)
    @NSManaged public func insertIntoDataPoints(_ value: GraphDataPoint, at idx: Int)

    @objc(removeObjectFromDataPointsAtIndex:)
    @NSManaged public func removeFromDataPoints(at idx: Int)

    @objc(insertDataPoints:atIndexes:)
    @NSManaged public func insertIntoDataPoints(_ values: [GraphDataPoint], at indexes: NSIndexSet)

    @objc(removeDataPointsAtIndexes:)
    @NSManaged public func removeFromDataPoints(at indexes: NSIndexSet)

    @objc(replaceObjectInDataPointsAtIndex:withObject:)
    @NSManaged public func replaceDataPoints(at idx: Int, with value: GraphDataPoint)

    @objc(replaceDataPointsAtIndexes:withDataPoints:)
    @NSManaged public func replaceDataPoints(at indexes: NSIndexSet, with values: [GraphDataPoint])

    @objc(addDataPointsObject:)
    @NSManaged public func addToDataPoints(_ value: GraphDataPoint)

    @objc(removeDataPointsObject:)
    @NSManaged public func removeFromDataPoints(_ value: GraphDataPoint)

    @objc(addDataPoints:)
    @NSManaged public func addToDataPoints(_ values: NSOrderedSet)

    @objc(removeDataPoints:)
    @NSManaged public func removeFromDataPoints(_ values: NSOrderedSet)

}
