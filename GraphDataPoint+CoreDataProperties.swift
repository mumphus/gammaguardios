//
//  GraphDataPoint+CoreDataProperties.swift
//  
//
//  Created by EIC on 2018-07-17.
//
//

import Foundation
import CoreData


extension GraphDataPoint {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<GraphDataPoint> {
        return NSFetchRequest<GraphDataPoint>(entityName: "GraphDataPoint")
    }

    @NSManaged public var count: Double
    @NSManaged public var latitude: Double
    @NSManaged public var longitude: Double
    @NSManaged public var time: Int64
    @NSManaged public var set: GraphDataSet?

}
